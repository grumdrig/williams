var fs = require('fs');

function undef(x) { return typeof x === 'undefined' }
function defined(x) { return typeof x !== 'undefined' }

function x8(n) {
  if (undef(n)) return '__';
  return ('0' + (0xFF&n).toString(16).toUpperCase()).slice(-2);
}

function x16(n) {
  if (undef(n)) return '____';
  return ('000' + (0xFFFF&n).toString(16).toUpperCase()).slice(-4);
}

function hex(n, bits) { return bits == 16 ? x16(n) : x8(n) }

function fmt() {
  var r = arguments[0];
  for (var a = 1; ; ++a) {
    var m = r.match(/%(2x|4x|d|s)/);
    if (!m) break;
    switch (m[1]) {
    case 's': r = r.replace(m[0], arguments[a]); break;
    case 'd': r = r.replace(m[0], arguments[a]+''); break;
    case '2x': r = r.replace(m[0], x8(arguments[a])); break;
    case '4x': r = r.replace(m[0], x16(arguments[a])); break;
    }
  }
  return r;
}

var INHERENT = 'inherent'
var IMMEDIATE = 'immediate'
var LIMMEDIATE = 'limmediate'
var DIRECT = 'direct'
var INDEXED = 'indexed'
var EXTENDED = 'extended'
var RELATIVE = 'relative'
var LRELATIVE = 'lrelative'
var REGISTER = 'register'


function Cpu(partNumber, clockRate) {
  var self = this;
  clockRate = clockRate || 1; // MHz

  var E, F, H, I, N, Z, V, C;
  var A;
  var B;
  var DP;
  var X;
  var Y;
  var S;
  var U;
  var PC;

  var CLOCK = 0, MCLOCK = 0;

  var verbose = false;

  this.RESET = true;
  this.NMI = this.IRQ = this.FIRQ = false;
  this.WAIT = false;

  function CC(v) {
    if (undef(v)) {
      return E<<7 | F <<6 | H<<5 | I<<4 | N<<3 | Z<<2 | V<<1 | C;
    } else {
      E = 1 & v >> 7;
      F = 1 & v >> 6;
      H = 1 & v >> 5;
      I = 1 & v >> 4;
      N = 1 & v >> 3;
      Z = 1 & v >> 2;
      V = 1 & v >> 1;
      C = 1 & v;
    }
  }

  function D(v) {
    if (undef(v)) {
      return (A << 8) + (B & 0xFF);
    } else {
      A = 0xFF & (v >> 8);
      B = 0xFF & v;
    }
  }

  this.verbose = function (v) { verbose = v }

  function traceBytes(start, bytes, width, sep, norom) {
    width = width || bytes;
    var bs = '';
    for (var i = 0; i < width; ++i)
      bs += (i < bytes ? x8(fetch8(start + i, true, norom)) : '  ') +
        (sep || ' ');
    return bs;
  }
  this.traceBytes = traceBytes;


  function disassemble6800(pc) {
    var opcode = fetch8(pc, true);
    var nbytes = 1;
    var record = INSTRUCTIONS_6800[opcode];

    if (!record) return '!BADOP_' + x8(opcode);

    var register = record[1];
    var mnemonic = record[0] + register;
    var mode = record[3];

    if (mode === INHERENT) {
      operand = '';
    } else if (mode === RELATIVE) {
      nbytes = 2;
      addr = pc + 2 + s8(fetch8(pc + 1, true));
      operand = 'L_' + x16(addr);
    } else if (mode === EXTENDED) {
      nbytes = 3;
      addr = fetch16(pc + 1, true);
      operand = '$' + x16(addr)
    } else if (mode === DIRECT) {
      nbytes = 2;
      addr = fetch8(pc + 1, true);
      operand = '<$' + x16(addr)
    } else if (mode === INDEXED) {
      nbytes = 2;
      addr = fetch8(pc + 1, true);
      operand = 'X + $' + x8(addr);
    } else if (mode === LIMMEDIATE) {
      addr = pc + 1;
      nbytes = 3;
      operand = '#$' + x16(fetch16(addr, true));
    } else {
      console.assert(mode === IMMEDIATE);
      addr = pc + 1;
      nbytes = 2;
      operand = '#$' + x8(fetch8(addr, true));
    }

    var bytes = traceBytes(pc, nbytes, 3);
    var stack = traceBytes(S+1, 4, 4, ',');
    var state = (x8(A) + ':' + x8(B) + ' <' +
                 x16(X) + ' ' + x16(S) + '[' + stack + '] ' +
                 (H?'H':'-') + (I?'I':'-') + (N?'N':'-') +
                 (Z?'Z':'-') + (V?'V':'-') + (C?'C':'-') + ' ');
    return {
      nbytes: nbytes,
      long: x16(pc) + ': ' + state + ' ' + bytes + ' ' + mnemonic + ' ' + operand,
      short: x16(pc) + ':  ' + bytes + mnemonic + ' ' + operand,
    }
  }

  function disassemble6809(pc, bytes, asm) {
    var opcode = fetch8(pc, true);
    var ppc = pc + 1;
    if (opcode === 0x10 || opcode === 0x11)
      opcode = opcode * 256 + fetch8(ppc++, true);

    var record = INSTRUCTIONS_6809[opcode];
    if (!record) return '!BADOP_' + x8(opcode);

    var register = record[1];
    var mnemonic = record[0] + register;
    var mode = record[2];
    var nbytes = record[4];

    var width = BITS[register];

    var param;
    if (mode === EXTENDED) {
      param = fmt('$%4x', fetch16(ppc, true));

    } else if (mode === INHERENT) {
      param = '';

    } else if (mode === RELATIVE) {
      param = fmt('L_%4x', ppc+1 + s8(fetch8(ppc, true)));

    } else if (mode === LRELATIVE) {
      param = fmt('L_%4x', ppc+2 + s16(fetch16(ppc, true)));

    } else if (mode === DIRECT) {
      var offset = fetch8(ppc, true);
      var addr = DP * 256 + offset;
      param = fmt('DP:$%2x ; %4x', offset, addr);

    } else if (mode === REGISTER) {
      TFR_REGS = ['D', 'X', 'Y',  'U',  'S', 'PC',  null, null,
                  'A', 'B', 'CC', 'DP'];
      var postbyte = fetch8(ppc, true);
      param = fmt('%s, %s', TFR_REGS[postbyte & 0xF], TFR_REGS[postbyte >> 4]);

    } else if (mode === IMMEDIATE) {
      param = fmt('#$%s', (width === 16) ? x16(fetch16(ppc, true)) : x8(fetch8(ppc, true)));

    } else if (mode === INDEXED) {
      // For this mode we'll update ppc in order to correct the value of nbytes
      var postbyte = fetch8(ppc++, true);
      var subregister = 'XYUS'[(postbyte>>5) & 0x3];
      var indirect = ((postbyte & 0x10) !== 0);
      var submode = postbyte & 0xF;
      var addr;
      if (postbyte === 0x9F) {
        // Extended indirect
        var ptr = fetch16(ppc, true);
        ppc += 2;
        addr = fetch16(ptr, true);
        param = fmt('[%4x] ; %4x', ptr, addr);
      } else if ((postbyte & 0x80) === 0) {
        // 5-bit offset from register contained in postbyte
        if (indirect) submode -= 0x10;  // account for sign bit
        addr = reg(subregister) + submode;
        param = fmt('%s,%d ; %4x', subregister, submode, addr);
      } else {
        var addr = reg(subregister);
        var offset;
        param = subregister;
        if (submode === 0x4) {
          offset = 0;
        } else if (submode === 0x8) {
          offset = s8(fetch8(ppc++, true));
        } else if (submode === 0x9) {
          offset = s16(fetch16(ppc, true));
          ppc += 2;
        } else if (submode === 0x6) {
          param = subregister + ',A';
          offset = A;
        } else if (submode === 0x5) {
          param = subregister + ',B';
          offset = B;
        } else if (submode === 0xB) {
          param = subregister + ',D';
          offset = D();
        } else if (submode === 0) {
          autoincrement = 1;
          param += '+';
          offset = 0;
        } else if (submode === 1) {
          autoincrement = 2;
          param += '++';
          offset = 0;
        } else if (submode === 2) {
          autoincrement = -1;
          param = '-' + param;
          offset = -1;  // it's a pre-decrement
        } else if (submode === 3) {
          autoincrement = -2;
          param = '--' + param;
          offset = -2;  // it's a pre-decrement
        } else if (submode === 0xC) {
          addr = 0;
          offset = s8(fetch8(ppc++, true));
          param = offset + '';
        } else if (submode === 0xD) {
          addr = 0;
          offset = s16(fetch16(ppc, true));
          ppc += 2;
          param = offset + '';
        } else {
          param = '??';
        }

        if (param === subregister && offset)
          param += ',' + offset;

        addr += offset;
        param = fmt('%s ; %4x', param, addr);

        if (indirect) {
          addr = fetch16(addr, true);
          param = fmt('[%s] ; %4x', param, addr);
        }

        nbytes = ppc - pc;
      }
    }

    var state = fmt("<%2x %2x:%2x x%4x y%4x s%4x[%s] u%4x ",
      DP, A, B, X, Y, S, traceBytes(S,2,2,','), U) +
      (E?'E':'-') + (F?'F':'-') + (H?'H':'-') + (I?'I':'-') + (N?'N':'-') +
      (Z?'Z':'-') + (V?'V':'-') + (C?'C':'-');

    var bytes = traceBytes(pc, nbytes, 4);

    return {
      nbytes: nbytes,
      long: x16(pc) + ': ' + state + ' ' + bytes + mnemonic + ' ' + param,
      short: x16(pc) + ':  ' + bytes + mnemonic + ' ' + param,
    }

  }

  var ROM = {};

  this.loadRom = function (addr, file) {
    if (!file) {
      delete ROM[addr];
      return;
    }
    if (typeof file === 'string')
      file = fs.readFileSync('roms/' + file);
    ROM[addr] = file;
  }

  var RAM = [];
  var IO = {};
  var PIAS = [];

  this.defineIO = function (start, length) {
    IO.start = start;
    IO.length = length;
  }

  this.io = function () { return IO }

  this.loadSrec = function (file) {
    // http://en.wikipedia.org/wiki/SREC_(file_format)
    if (typeof file === 'string')
      file = fs.readFileSync(file);
    file = String.fromCharCode.apply(null, file).split('\n');
    for (var i = 0; i < file.length; ++i) {
      var line = file[i];
      var command = line.substr(0,2);
      if (command === 'S9')
        break;
      if (command !== 'S1')
        error('Unrecognized SREC record type: ' + command);
      var bytecount = parseInt(line.substr(2,2), 16);
      var address = parseInt(line.substr(4,4), 16);
      var bytes = [];
      for (var j = 0; j < bytecount - 3; ++j) {
        var v = parseInt(line.substr(8 + j * 2, 2), 16);
        store8(address, v);
        ++address;
      }
    }
  }

  this.loadRam = function (addr, data) {
    if (addr !== 0)
      error('Unimplected: loading RAM elsewhere than at 0');
    RAM = data;
  }

  this.createRam = function (addr, length) {
    if (addr !== 0)
      error('Unimplected: creating RAM elsewhere than at 0');
    RAM = new Uint8Array(length);
  }

  this.randRam = function (addr, length) {
    if (undef(addr)) addr = 0;
    if (undef(length)) length = RAM.length - addr;
    for (var i = 0; i < length; ++i)
      RAM[addr + i] = Math.random() * 256 >> 0;
  }

  this.ram = function () { return RAM }

  var WRITE_HOOKS = {};
  var READ_HOOKS = {};

  var ILLEGAL = 'illegal'

  var BITS = {
    A: 8,
    B: 8,
    D: 16,
    X: 16,
    Y: 16,
    S: 16,
    U: 16,
    PC: 16,
    DP: 8,
    CC: 8
    }

  var INSTRUCTIONS_6800 = {
    0x1B: ['ABA', '',  2, INHERENT],
    0xB9: ['ADC', 'A', 4, EXTENDED],
    0xA9: ['ADC', 'A', 5, INDEXED],
    0x99: ['ADC', 'A', 3, DIRECT],
    0x89: ['ADC', 'A', 2, IMMEDIATE],
    0xF9: ['ADC', 'B', 4, EXTENDED],
    0xE9: ['ADC', 'B', 5, INDEXED],
    0xD9: ['ADC', 'B', 3, DIRECT],
    0xC9: ['ADC', 'B', 2, IMMEDIATE],
    0xBB: ['ADD', 'A', 4, EXTENDED],
    0xAB: ['ADD', 'A', 5, INDEXED],
    0x9B: ['ADD', 'A', 3, DIRECT],
    0x8B: ['ADD', 'A', 2, IMMEDIATE],
    0xFB: ['ADD', 'B', 4, EXTENDED],
    0xEB: ['ADD', 'B', 5, INDEXED],
    0xDB: ['ADD', 'B', 3, DIRECT],
    0xCB: ['ADD', 'B', 2, IMMEDIATE],
    0xB4: ['AND', 'A', 4, EXTENDED],
    0xA4: ['AND', 'A', 5, INDEXED],
    0x94: ['AND', 'A', 3, DIRECT],
    0x84: ['AND', 'A', 2, IMMEDIATE],
    0xF4: ['AND', 'B', 4, EXTENDED],
    0xE4: ['AND', 'B', 5, INDEXED],
    0xD4: ['AND', 'B', 3, DIRECT],
    0xC4: ['AND', 'B', 2, IMMEDIATE],
    0x78: ['ASL', '',  6, EXTENDED],
    0x68: ['ASL', '',  7, INDEXED],
    0x48: ['ASL', 'A', 2, INHERENT],
    0x58: ['ASL', 'B', 2, INHERENT],
    0x77: ['ASR', '',  6, EXTENDED],
    0x67: ['ASR', '',  7, INDEXED],
    0x47: ['ASR', 'A', 2, INHERENT],
    0x57: ['ASR', 'B', 2, INHERENT],
    0x24: ['BCC', '',  4, RELATIVE],
    0x25: ['BCS', '',  4, RELATIVE],
    0x27: ['BEQ', '',  4, RELATIVE],
    0x2C: ['BGE', '',  4, RELATIVE],
    0x2E: ['BGT', '',  4, RELATIVE],
    0x22: ['BHI', '',  4, RELATIVE],
    0xB5: ['BIT', 'A', 4, EXTENDED],
    0xA5: ['BIT', 'A', 5, INDEXED],
    0x95: ['BIT', 'A', 3, DIRECT],
    0x85: ['BIT', 'A', 2, IMMEDIATE],
    0xF5: ['BIT', 'B', 4, EXTENDED],
    0xE5: ['BIT', 'B', 5, INDEXED],
    0xD5: ['BIT', 'B', 3, DIRECT],
    0xC5: ['BIT', 'B', 2, IMMEDIATE],
    0x2F: ['BLE', '',  4, RELATIVE],
    0x23: ['BLS', '',  4, RELATIVE],
    0x2D: ['BLT', '',  4, RELATIVE],
    0x2B: ['BMI', '',  4, RELATIVE],
    0x26: ['BNE', '',  4, RELATIVE],
    0x2A: ['BPL', '',  4, RELATIVE],
    0x20: ['BRA', '',  4, RELATIVE],
    0x8D: ['BSR', '',  8, RELATIVE],
    0x28: ['BVC', '',  4, RELATIVE],
    0x29: ['BVS', '',  4, RELATIVE],
    0x11: ['CBA', '',  2, INHERENT],
    0x0C: ['CLC', '',  2, INHERENT],
    0x0E: ['CLI', '',  2, INHERENT],
    0x7F: ['CLR', '',  6, EXTENDED],
    0x6F: ['CLR', '',  7, INDEXED],
    0x4F: ['CLR', 'A', 2, INHERENT],
    0x5F: ['CLR', 'B', 2, INHERENT],
    0x0A: ['CLV', '',  2, INHERENT],
    0xB1: ['CMP', 'A', 4, EXTENDED],
    0xA1: ['CMP', 'A', 5, INDEXED],
    0x91: ['CMP', 'A', 3, DIRECT],
    0x81: ['CMP', 'A', 2, IMMEDIATE],
    0xF1: ['CMP', 'B', 4, EXTENDED],
    0xE1: ['CMP', 'B', 5, INDEXED],
    0xD1: ['CMP', 'B', 3, DIRECT],
    0xC1: ['CMP', 'B', 2, IMMEDIATE],
    0x73: ['COM', '',  6, EXTENDED],
    0x63: ['COM', '',  7, INDEXED],
    0x43: ['COM', 'A', 2, INHERENT],
    0x53: ['COM', 'B', 2, INHERENT],
    0xBC: ['CP',  'X', 5, EXTENDED],
    0xAC: ['CP',  'X', 6, INDEXED],
    0x9C: ['CP',  'X', 4, DIRECT],
    0x8C: ['CP',  'X', 3, LIMMEDIATE],
    0x19: ['DAA', '',  2, INHERENT],
    0x7A: ['DEC', '',  6, EXTENDED],
    0x6A: ['DEC', '',  7, INDEXED],
    0x4A: ['DEC', 'A', 2, INHERENT],
    0x5A: ['DEC', 'B', 2, INHERENT],
    0x34: ['DE',  'S', 4, INHERENT],
    0x09: ['DE',  'X', 4, INHERENT],
    0xB8: ['EOR', 'A', 4, EXTENDED],
    0xA8: ['EOR', 'A', 5, INDEXED],
    0x98: ['EOR', 'A', 3, DIRECT],
    0x88: ['EOR', 'A', 2, IMMEDIATE],
    0xF8: ['EOR', 'B', 4, EXTENDED],
    0xE8: ['EOR', 'B', 5, INDEXED],
    0xD8: ['EOR', 'B', 3, DIRECT],
    0xC8: ['EOR', 'B', 2, IMMEDIATE],
    0x7C: ['INC', '',  6, EXTENDED],
    0x6C: ['INC', '',  7, INDEXED],
    0x4C: ['INC', 'A', 2, INHERENT],
    0x5C: ['INC', 'B', 2, INHERENT],
    0x31: ['IN',  'S', 4, INHERENT],
    0x08: ['IN',  'X', 4, INHERENT],
    0x7E: ['JMP', '',  3, EXTENDED],
    0x6E: ['JMP', '',  4, INDEXED],
    0xBD: ['JSR', '',  8, EXTENDED],
    0xAD: ['JSR', '',  9, INDEXED],
    0xB6: ['LDA', 'A', 4, EXTENDED],
    0xA6: ['LDA', 'A', 5, INDEXED],
    0x96: ['LDA', 'A', 3, DIRECT],
    0x86: ['LDA', 'A', 2, IMMEDIATE],
    0xF6: ['LDA', 'B', 4, EXTENDED],
    0xE6: ['LDA', 'B', 5, INDEXED],
    0xD6: ['LDA', 'B', 3, DIRECT],
    0xC6: ['LDA', 'B', 2, IMMEDIATE],
    0xBE: ['LD',  'S', 5, EXTENDED],
    0xAE: ['LD',  'S', 6, INDEXED],
    0x9E: ['LD',  'S', 4, DIRECT],
    0x8E: ['LD',  'S', 3, LIMMEDIATE],
    0xFE: ['LD',  'X', 5, EXTENDED],
    0xEE: ['LD',  'X', 6, INDEXED],
    0xDE: ['LD',  'X', 4, DIRECT],
    0xCE: ['LD',  'X', 3, LIMMEDIATE],
    0x74: ['LSR', '',  6, EXTENDED],
    0x64: ['LSR', '',  7, INDEXED],
    0x44: ['LSR', 'A', 2, INHERENT],
    0x54: ['LSR', 'B', 2, INHERENT],
    0x70: ['NEG', '',  6, EXTENDED],
    0x60: ['NEG', '',  7, INDEXED],
    0x40: ['NEG', 'A', 2, INHERENT],
    0x50: ['NEG', 'B', 2, INHERENT],
    0x01: ['NOP', '',  2, INHERENT],
    0xBA: ['ORA', 'A', 4, EXTENDED],
    0xAA: ['ORA', 'A', 5, INDEXED],
    0x9A: ['ORA', 'A', 3, DIRECT],
    0x8A: ['ORA', 'A', 2, IMMEDIATE],
    0xFA: ['ORA', 'B', 4, EXTENDED],
    0xEA: ['ORA', 'B', 5, INDEXED],
    0xDA: ['ORA', 'B', 3, DIRECT],
    0xCA: ['ORA', 'B', 2, IMMEDIATE],
    0x36: ['PSH', 'A', 4, INHERENT],
    0x37: ['PSH', 'B', 4, INHERENT],
    0x32: ['PUL', 'A', 4, INHERENT],
    0x33: ['PUL', 'B', 4, INHERENT],
    0x79: ['ROL', '',  6, EXTENDED],
    0x69: ['ROL', '',  7, INDEXED],
    0x49: ['ROL', 'A', 2, INHERENT],
    0x59: ['ROL', 'B', 2, INHERENT],
    0x76: ['ROR', '',  6, EXTENDED],
    0x66: ['ROR', '',  7, INDEXED],
    0x46: ['ROR', 'A', 2, INHERENT],
    0x56: ['ROR', 'B', 2, INHERENT],
    0x3B: ['RTI', '', 10, INHERENT],
    0x39: ['RTS', '',  5, INHERENT],
    0x10: ['SBA', '',  2, INHERENT],
    0xB2: ['SBC', 'A', 4, EXTENDED],
    0xA2: ['SBC', 'A', 5, INDEXED],
    0x92: ['SBC', 'A', 3, DIRECT],
    0x82: ['SBC', 'A', 2, IMMEDIATE],
    0xF2: ['SBC', 'B', 4, EXTENDED],
    0xE2: ['SBC', 'B', 5, INDEXED],
    0xD2: ['SBC', 'B', 3, DIRECT],
    0xC2: ['SBC', 'B', 2, IMMEDIATE],
    0x0D: ['SEC', '',  2, INHERENT],
    0x0F: ['SEI', '',  2, INHERENT],
    0x0B: ['SEV', '',  2, INHERENT],
    0xB7: ['STA', 'A', 5, EXTENDED],
    0xA7: ['STA', 'A', 6, INDEXED],
    0x97: ['STA', 'A', 4, DIRECT],
    0xF7: ['STA', 'B', 5, EXTENDED],
    0xE7: ['STA', 'B', 6, INDEXED],
    0xD7: ['STA', 'B', 4, DIRECT],
    0xBF: ['ST',  'S', 6, EXTENDED],
    0xAF: ['ST',  'S', 7, INDEXED],
    0x9F: ['ST',  'S', 5, DIRECT],
    0xFF: ['ST',  'X', 6, EXTENDED],
    0xEF: ['ST',  'X', 7, INDEXED],
    0xDF: ['ST',  'X', 5, DIRECT],
    0xB0: ['SUB', 'A', 4, EXTENDED],
    0xA0: ['SUB', 'A', 5, INDEXED],
    0x90: ['SUB', 'A', 3, DIRECT],
    0x80: ['SUB', 'A', 2, IMMEDIATE],
    0xF0: ['SUB', 'B', 4, EXTENDED],
    0xE0: ['SUB', 'B', 5, INDEXED],
    0xD0: ['SUB', 'B', 3, DIRECT],
    0xC0: ['SUB', 'B', 2, IMMEDIATE],
    0x3F: ['SWI', '', 12, INHERENT],
    0x16: ['TAB', '',  2, INHERENT],
    0x06: ['TAP', '',  2, INHERENT],
    0x17: ['TBA', '',  2, INHERENT],
    0x07: ['TPA', '',  2, INHERENT],
    0x7D: ['TST', '',  6, EXTENDED],
    0x6D: ['TST', '',  6, INDEXED],
    0x4D: ['TST', 'A', 2, INHERENT],
    0x5D: ['TST', 'B', 2, INHERENT],
    0x30: ['TSX', '',  4, INHERENT],
    0x35: ['TXS', '',  4, INHERENT],
    0x3E: ['WAI', '',  9, INHERENT],
  };

  var INSTRUCTIONS_6809 = {
    0x3A  : ["ABX", ""         , INHERENT ,   3,   1],
    0x99  : ["ADC", "A"        , DIRECT   ,   4,   2],
    0xB9  : ["ADC", "A"        , EXTENDED ,   5,   3],
    0x89  : ["ADC", "A"        , IMMEDIATE,   2,   2],
    0xA9  : ["ADC", "A"        , INDEXED  ,   4,   2],
    0xD9  : ["ADC", "B"        , DIRECT   ,   4,   2],
    0xF9  : ["ADC", "B"        , EXTENDED ,   5,   3],
    0xC9  : ["ADC", "B"        , IMMEDIATE,   2,   2],
    0xE9  : ["ADC", "B"        , INDEXED  ,   4,   2],
    0x9B  : ["ADD", "A"        , DIRECT   ,   4,   2],
    0xBB  : ["ADD", "A"        , EXTENDED ,   5,   3],
    0x8B  : ["ADD", "A"        , IMMEDIATE,   2,   2],
    0xAB  : ["ADD", "A"        , INDEXED  ,   4,   2],
    0xDB  : ["ADD", "B"        , DIRECT   ,   4,   2],
    0xFB  : ["ADD", "B"        , EXTENDED ,   5,   3],
    0xCB  : ["ADD", "B"        , IMMEDIATE,   2,   2],
    0xEB  : ["ADD", "B"        , INDEXED  ,   4,   2],
    0xD3  : ["ADD", "D"        , DIRECT   ,   6,   2],
    0xF3  : ["ADD", "D"        , EXTENDED ,   7,   3],
    0xC3  : ["ADD", "D"        , IMMEDIATE,   4,   3],
    0xE3  : ["ADD", "D"        , INDEXED  ,   6,   2],
    0x94  : ["AND", "A"        , DIRECT   ,   4,   2],
    0xB4  : ["AND", "A"        , EXTENDED ,   5,   3],
    0x84  : ["AND", "A"        , IMMEDIATE,   2,   2],
    0xA4  : ["AND", "A"        , INDEXED  ,   4,   2],
    0xD4  : ["AND", "B"        , DIRECT   ,   4,   2],
    0xF4  : ["AND", "B"        , EXTENDED ,   5,   3],
    0xC4  : ["AND", "B"        , IMMEDIATE,   2,   2],
    0xE4  : ["AND", "B"        , INDEXED  ,   4,   2],
    0x1C  : ["AND", "CC"       , IMMEDIATE,   3,   2],
    0x38  : ["AND", "CC"       , IMMEDIATE,   4,   2, ILLEGAL],
    0x48  : ["ASL", "A"        , INHERENT ,   2,   1],
    0x58  : ["ASL", "B"        , INHERENT ,   2,   1],
    0x08  : ["ASL", ""         , DIRECT   ,   6,   2],
    0x78  : ["ASL", ""         , EXTENDED ,   7,   3],
    0x68  : ["ASL", ""         , INDEXED  ,   6,   2],
    0x07  : ["ASR", ""         , DIRECT   ,   6,   2],
    0x77  : ["ASR", ""         , EXTENDED ,   7,   3],
    0x67  : ["ASR", ""         , INDEXED  ,   6,   2],
    0x47  : ["ASR", "A"        , INHERENT ,   2,   1],
    0x57  : ["ASR", "B"        , INHERENT ,   2,   1],
    0x27  : ["BEQ", ""         , RELATIVE ,   3,   2],
    0x2C  : ["BGE", ""         , RELATIVE ,   3,   2],
    0x2E  : ["BGT", ""         , RELATIVE ,   3,   2],
    0x22  : ["BHI", ""         , RELATIVE ,   3,   2],
    0x24  : ["BCC", ""         , RELATIVE ,   3,   2],
    0x2F  : ["BLE", ""         , RELATIVE ,   3,   2],
    0x25  : ["BCS", ""         , RELATIVE ,   3,   2],
    0x23  : ["BLS", ""         , RELATIVE ,   3,   2],
    0x2D  : ["BLT", ""         , RELATIVE ,   3,   2],
    0x2B  : ["BMI", ""         , RELATIVE ,   3,   2],
    0x26  : ["BNE", ""         , RELATIVE ,   3,   2],
    0x2A  : ["BPL", ""         , RELATIVE ,   3,   2],
    0x20  : ["BRA", ""         , RELATIVE ,   3,   2],
    0x21  : ["BRN", ""         , RELATIVE ,   3,   2],
    0x8D  : ["BSR", ""         , RELATIVE ,   7,   2],
    0x28  : ["BVC", ""         , RELATIVE ,   3,   2],
    0x29  : ["BVS", ""         , RELATIVE ,   3,   2],
    0x95  : ["BIT", "A"        , DIRECT   ,   4,   2],
    0xB5  : ["BIT", "A"        , EXTENDED ,   5,   3],
    0x85  : ["BIT", "A"        , IMMEDIATE,   2,   2],
    0xA5  : ["BIT", "A"        , INDEXED  ,   4,   2],
    0xD5  : ["BIT", "B"        , DIRECT   ,   4,   2],
    0xF5  : ["BIT", "B"        , EXTENDED ,   5,   3],
    0xC5  : ["BIT", "B"        , IMMEDIATE,   2,   2],
    0xE5  : ["BIT", "B"        , INDEXED  ,   4,   2],
    0x0F  : ["CLR", ""         , DIRECT   ,   6,   2],
    0x7F  : ["CLR", ""         , EXTENDED ,   7,   3],
    0x6F  : ["CLR", ""         , INDEXED  ,   6,   2],
    0x4E  : ["CLR", "A"        , INHERENT ,   2,   1, ILLEGAL],
    0x4F  : ["CLR", "A"        , INHERENT ,   2,   1],
    0x5E  : ["CLR", "B"        , INHERENT ,   2,   1, ILLEGAL],
    0x5F  : ["CLR", "B"        , INHERENT ,   2,   1],
    0x91  : ["CMP", "A"        , DIRECT   ,   4,   2],
    0xB1  : ["CMP", "A"        , EXTENDED ,   5,   3],
    0x81  : ["CMP", "A"        , IMMEDIATE,   2,   2],
    0xA1  : ["CMP", "A"        , INDEXED  ,   4,   2],
    0xD1  : ["CMP", "B"        , DIRECT   ,   4,   2],
    0xF1  : ["CMP", "B"        , EXTENDED ,   5,   3],
    0xC1  : ["CMP", "B"        , IMMEDIATE,   2,   2],
    0xE1  : ["CMP", "B"        , INDEXED  ,   4,   2],
    0x9C  : ["CMP", "X"        , DIRECT   ,   6,   2],
    0xBC  : ["CMP", "X"        , EXTENDED ,   7,   3],
    0x8C  : ["CMP", "X"        , IMMEDIATE,   4,   3],
    0xAC  : ["CMP", "X"        , INDEXED  ,   6,   2],
    0x1093: ["CMP", "D"        , DIRECT   ,   7,   3],
    0x10B3: ["CMP", "D"        , EXTENDED ,   8,   4],
    0x1083: ["CMP", "D"        , IMMEDIATE,   5,   4],
    0x10A3: ["CMP", "D"        , INDEXED  ,   7,   3],
    0x119C: ["CMP", "S"        , DIRECT   ,   7,   3],
    0x11BC: ["CMP", "S"        , EXTENDED ,   8,   4],
    0x118C: ["CMP", "S"        , IMMEDIATE,   5,   4],
    0x11AC: ["CMP", "S"        , INDEXED  ,   7,   3],
    0x1193: ["CMP", "U"        , DIRECT   ,   7,   3],
    0x11B3: ["CMP", "U"        , EXTENDED ,   8,   4],
    0x1183: ["CMP", "U"        , IMMEDIATE,   5,   4],
    0x11A3: ["CMP", "U"        , INDEXED  ,   7,   3],
    0x109C: ["CMP", "Y"        , DIRECT   ,   7,   3],
    0x10BC: ["CMP", "Y"        , EXTENDED ,   8,   4],
    0x108C: ["CMP", "Y"        , IMMEDIATE,   5,   4],
    0x10AC: ["CMP", "Y"        , INDEXED  ,   7,   3],
    0x03  : ["COM", ""         , DIRECT   ,   6,   2],
    0x73  : ["COM", ""         , EXTENDED ,   7,   3],
    0x63  : ["COM", ""         , INDEXED  ,   6,   2],
    0x43  : ["COM", "A"        , INHERENT ,   2,   1],
    0x53  : ["COM", "B"        , INHERENT ,   2,   1],
    0x3C  : ["CWAI",""         , INHERENT ,  21,   2],
    0x19  : ["DAA", ""         , INHERENT ,   2,   1],
    0x0A  : ["DEC", ""         , DIRECT   ,   6,   2],
    0x0B  : ["DEC", ""         , DIRECT   ,   6,   2, ILLEGAL],
    0x7A  : ["DEC", ""         , EXTENDED ,   7,   3],
    0x7B  : ["DEC", ""         , EXTENDED ,   7,   3, ILLEGAL],
    0x6A  : ["DEC", ""         , INDEXED  ,   6,   2],
    0x6B  : ["DEC", ""         , INDEXED  ,   6,   2, ILLEGAL],
    0x4A  : ["DEC", "A"        , INHERENT ,   2,   1],
    0x4B  : ["DEC", "A"        , INHERENT ,   2,   1, ILLEGAL],
    0x5A  : ["DEC", "B"        , INHERENT ,   2,   1],
    0x5B  : ["DEC", "B"        , INHERENT ,   2,   1, ILLEGAL],
    0x98  : ["EOR", "A"        , DIRECT   ,   4,   2],
    0xB8  : ["EOR", "A"        , EXTENDED ,   5,   3],
    0x88  : ["EOR", "A"        , IMMEDIATE,   2,   2],
    0xA8  : ["EOR", "A"        , INDEXED  ,   4,   2],
    0xD8  : ["EOR", "B"        , DIRECT   ,   4,   2],
    0xF8  : ["EOR", "B"        , EXTENDED ,   5,   3],
    0xC8  : ["EOR", "B"        , IMMEDIATE,   2,   2],
    0xE8  : ["EOR", "B"        , INDEXED  ,   4,   2],
    0x1E  : ["EXG", ""         , REGISTER ,   8,   2],
    0x14  : ["!HCF",""         , INHERENT ,   2,   1, ILLEGAL],
    0x15  : ["!HCF",""         , INHERENT ,   2,   1, ILLEGAL],
    0xCD  : ["!HCF",""         , INHERENT ,   2,   1, ILLEGAL],
    0x0C  : ["INC", ""         , DIRECT   ,   6,   2],
    0x7C  : ["INC", ""         , EXTENDED ,   7,   3],
    0x6C  : ["INC", ""         , INDEXED  ,   6,   2],
    0x4C  : ["INC", "A"        , INHERENT ,   2,   1],
    0x5C  : ["INC", "B"        , INHERENT ,   2,   1],
    0x0E  : ["JMP", ""         , DIRECT   ,   3,   2],
    0x7E  : ["JMP", ""         , EXTENDED ,   3,   3],
    0x6E  : ["JMP", ""         , INDEXED  ,   3,   2],
    0x9D  : ["JSR", ""         , DIRECT   ,   7,   2],
    0xBD  : ["JSR", ""         , EXTENDED ,   8,   3],
    0xAD  : ["JSR", ""         , INDEXED  ,   7,   2],
    0x02  : ["LBRA",""         , LRELATIVE,   5,   3, ILLEGAL],
    0x16  : ["LBRA",""         , LRELATIVE,   5,   3],
    0x17  : ["LBSR",""         , LRELATIVE,   9,   3],
    0x1027: ["LBEQ",""         , LRELATIVE,   5,   4],
    0x102C: ["LBGE",""         , LRELATIVE,   5,   4],
    0x102E: ["LBGT",""         , LRELATIVE,   5,   4],
    0x1022: ["LBHI",""         , LRELATIVE,   5,   4],
    0x1024: ["LBCC",""         , LRELATIVE,   5,   4],
    0x102F: ["LBLE",""         , LRELATIVE,   5,   4],
    0x1025: ["LBCS",""         , LRELATIVE,   5,   4],
    0x1023: ["LBLS",""         , LRELATIVE,   5,   4],
    0x102D: ["LBLT",""         , LRELATIVE,   5,   4],
    0x102B: ["LBMI",""         , LRELATIVE,   5,   4],
    0x1026: ["LBNE",""         , LRELATIVE,   5,   4],
    0x102A: ["LBPL",""         , LRELATIVE,   5,   4],
    0x1021: ["LBRN",""         , LRELATIVE,   5,   4],
    0x1028: ["LBVC",""         , LRELATIVE,   5,   4],
    0x1029: ["LBVS",""         , LRELATIVE,   5,   4],
    0x96  : ["LD",  "A"        , DIRECT   ,   4,   2],
    0xB6  : ["LD",  "A"        , EXTENDED ,   5,   3],
    0x86  : ["LD",  "A"        , IMMEDIATE,   2,   2],
    0xA6  : ["LD",  "A"        , INDEXED  ,   4,   2],
    0xD6  : ["LD",  "B"        , DIRECT   ,   4,   2],
    0xF6  : ["LD",  "B"        , EXTENDED ,   5,   3],
    0xC6  : ["LD",  "B"        , IMMEDIATE,   2,   2],
    0xE6  : ["LD",  "B"        , INDEXED  ,   4,   2],
    0xDC  : ["LD",  "D"        , DIRECT   ,   5,   2],
    0xFC  : ["LD",  "D"        , EXTENDED ,   6,   3],
    0xCC  : ["LD",  "D"        , IMMEDIATE,   3,   3],
    0xEC  : ["LD",  "D"        , INDEXED  ,   5,   2],
    0xDE  : ["LD",  "U"        , DIRECT   ,   5,   2],
    0xFE  : ["LD",  "U"        , EXTENDED ,   6,   3],
    0xCE  : ["LD",  "U"        , IMMEDIATE,   3,   3],
    0xEE  : ["LD",  "U"        , INDEXED  ,   5,   2],
    0x9E  : ["LD",  "X"        , DIRECT   ,   5,   2],
    0xBE  : ["LD",  "X"        , EXTENDED ,   6,   3],
    0x8E  : ["LD",  "X"        , IMMEDIATE,   3,   3],
    0xAE  : ["LD",  "X"        , INDEXED  ,   5,   2],
    0x108E: ["LD",  "Y"        , IMMEDIATE,   4,   4],
    0x10DE: ["LD",  "S"        , DIRECT   ,   6,   3],
    0x10FE: ["LD",  "S"        , EXTENDED ,   7,   4],
    0x10CE: ["LD",  "S"        , IMMEDIATE,   4,   4],
    0x10EE: ["LD",  "S"        , INDEXED  ,   6,   3],
    0x109E: ["LD",  "Y"        , DIRECT   ,   6,   3],
    0x10BE: ["LD",  "Y"        , EXTENDED ,   7,   4],
    0x10AE: ["LD",  "Y"        , INDEXED  ,   6,   3],
    0x32  : ["LEA", "S"        , INDEXED  ,   4,   2],
    0x33  : ["LEA", "U"        , INDEXED  ,   4,   2],
    0x30  : ["LEA", "X"        , INDEXED  ,   4,   2],
    0x31  : ["LEA", "Y"        , INDEXED  ,   4,   2],
    0x04  : ["LSR", ""         , DIRECT   ,   6,   2],
    0x05  : ["LSR", ""         , DIRECT   ,   6,   2, ILLEGAL],
    0x74  : ["LSR", ""         , EXTENDED ,   7,   3],
    0x75  : ["LSR", ""         , EXTENDED ,   7,   3, ILLEGAL],
    0x64  : ["LSR", ""         , INDEXED  ,   6,   2],
    0x65  : ["LSR", ""         , INDEXED  ,   6,   2, ILLEGAL],
    0x44  : ["LSR", "A"        , INHERENT ,   2,   1],
    0x45  : ["LSR", "A"        , INHERENT ,   2,   1, ILLEGAL],
    0x54  : ["LSR", "B"        , INHERENT ,   2,   1],
    0x55  : ["LSR", "B"        , INHERENT ,   2,   1, ILLEGAL],
    0x3D  : ["MUL", ""         , INHERENT ,  11,   1],
    0x00  : ["NEG", ""         , DIRECT   ,   6,   2],
    0x01  : ["NEG", ""         , DIRECT   ,   6,   2, ILLEGAL],
    0x70  : ["NEG", ""         , EXTENDED ,   7,   3],
    0x71  : ["NEG", ""         , EXTENDED ,   7,   3, ILLEGAL],
    0x72  : ["NECO",""         , EXTENDED ,   7,   3, ILLEGAL],
    0x60  : ["NEG", ""         , INDEXED  ,   6,   2],
    0x61  : ["NEG", ""         , INDEXED  ,   6,   2, ILLEGAL],
    0x62  : ["NECO",""         , INDEXED  ,   6,   2, ILLEGAL],
    0x40  : ["NEG", "A"        , INHERENT ,   2,   1],
    0x41  : ["NEG", "A"        , INHERENT ,   2,   1, ILLEGAL],
    0x42  : ["NECO","A"        , INHERENT ,   2,   1, ILLEGAL],
    0x50  : ["NEG", "B"        , INHERENT ,   2,   1],
    0x51  : ["NEG", "B"        , INHERENT ,   2,   1, ILLEGAL],
    0x52  : ["NECO","B"        , INHERENT ,   2,   1, ILLEGAL],
    0x12  : ["NOP", ""         , INHERENT ,   2,   1],
    0x1B  : ["NOP", ""         , INHERENT ,   2,   1, ILLEGAL],
    0x87  : ["DROP",""         , IMMEDIATE,   2,   2, ILLEGAL],
    0xC7  : ["DROP",""         , IMMEDIATE,   2,   2, ILLEGAL],
    0x9A  : ["OR", "A"         , DIRECT   ,   4,   2],
    0xBA  : ["OR", "A"         , EXTENDED ,   5,   3],
    0x8A  : ["OR", "A"         , IMMEDIATE,   2,   2],
    0xAA  : ["OR", "A"         , INDEXED  ,   4,   2],
    0xDA  : ["OR", "B"         , DIRECT   ,   4,   2],
    0xFA  : ["OR", "B"         , EXTENDED ,   5,   3],
    0xCA  : ["OR", "B"         , IMMEDIATE,   2,   2],
    0xEA  : ["OR", "B"         , INDEXED  ,   4,   2],
    0x1A  : ["OR", "CC"        , IMMEDIATE,   3,   2],
    0x34  : ["PSHS",""         , IMMEDIATE,   5,   2],
    0x36  : ["PSHU",""         , IMMEDIATE,   5,   2],
    0x35  : ["PULS",""         , IMMEDIATE,   5,   2],
    0x37  : ["PULU",""         , IMMEDIATE,   5,   2],
    0x3E  : ["RESET",""        , INHERENT ,  15,   1, ILLEGAL],
    0x09  : ["ROL", ""         , DIRECT   ,   6,   2],
    0x79  : ["ROL", ""         , EXTENDED ,   7,   3],
    0x69  : ["ROL", ""         , INDEXED  ,   6,   2],
    0x49  : ["ROL", "A"        , INHERENT ,   2,   1],
    0x59  : ["ROL", "B"        , INHERENT ,   2,   1],
    0x06  : ["ROR", ""         , DIRECT   ,   6,   2],
    0x76  : ["ROR", ""         , EXTENDED ,   7,   3],
    0x66  : ["ROR", ""         , INDEXED  ,   6,   2],
    0x46  : ["ROR", "A"        , INHERENT ,   2,   1],
    0x56  : ["ROR", "B"        , INHERENT ,   2,   1],
    0x3B  : ["RTI", ""         , INHERENT ,   6,   1],
    0x39  : ["RTS", ""         , INHERENT ,   5,   1],
    0x92  : ["SBC", "A"        , DIRECT   ,   4,   2],
    0xB2  : ["SBC", "A"        , EXTENDED ,   5,   3],
    0x82  : ["SBC", "A"        , IMMEDIATE,   2,   2],
    0xA2  : ["SBC", "A"        , INDEXED  ,   4,   2],
    0xD2  : ["SBC", "B"        , DIRECT   ,   4,   2],
    0xF2  : ["SBC", "B"        , EXTENDED ,   5,   3],
    0xC2  : ["SBC", "B"        , IMMEDIATE,   2,   2],
    0xE2  : ["SBC", "B"        , INDEXED  ,   4,   2],
    0x1D  : ["SEX", ""         , INHERENT ,   2,   1],
    0x97  : ["ST",  "A"        , DIRECT   ,   4,   2],
    0xB7  : ["ST",  "A"        , EXTENDED ,   5,   3],
    0xA7  : ["ST",  "A"        , INDEXED  ,   4,   2],
    0xD7  : ["ST",  "B"        , DIRECT   ,   4,   2],
    0xF7  : ["ST",  "B"        , EXTENDED ,   5,   3],
    0xE7  : ["ST",  "B"        , INDEXED  ,   4,   2],
    0xDD  : ["ST",  "D"        , DIRECT   ,   5,   2],
    0xFD  : ["ST",  "D"        , EXTENDED ,   6,   3],
    0xED  : ["ST",  "D"        , INDEXED  ,   5,   2],
    0xDF  : ["ST",  "U"        , DIRECT   ,   5,   2],
    0xFF  : ["ST",  "U"        , EXTENDED ,   6,   3],
    0xEF  : ["ST",  "U"        , INDEXED  ,   5,   2],
    0x9F  : ["ST",  "X"        , DIRECT   ,   5,   2],
    0xBF  : ["ST",  "X"        , EXTENDED ,   6,   3],
    0xAF  : ["ST",  "X"        , INDEXED  ,   5,   2],
    0x10EF: ["ST",  "S"        , INDEXED  ,   6,   3],
    0x10DF: ["ST",  "S"        , DIRECT   ,   6,   3],
    0x10FF: ["ST",  "S"        , EXTENDED ,   7,   4],
    0x109F: ["ST",  "Y"        , DIRECT   ,   6,   3],
    0x10BF: ["ST",  "Y"        , EXTENDED ,   7,   4],
    0x10AF: ["ST",  "Y"        , INDEXED  ,   6,   3],
    0x90  : ["SUB", "A"        , DIRECT   ,   4,   2],
    0xB0  : ["SUB", "A"        , EXTENDED ,   5,   3],
    0x80  : ["SUB", "A"        , IMMEDIATE,   2,   2],
    0xA0  : ["SUB", "A"        , INDEXED  ,   4,   2],
    0xD0  : ["SUB", "B"        , DIRECT   ,   4,   2],
    0xF0  : ["SUB", "B"        , EXTENDED ,   5,   3],
    0xC0  : ["SUB", "B"        , IMMEDIATE,   2,   2],
    0xE0  : ["SUB", "B"        , INDEXED  ,   4,   2],
    0x93  : ["SUB", "D"        , DIRECT   ,   6,   2],
    0xB3  : ["SUB", "D"        , EXTENDED ,   7,   3],
    0x83  : ["SUB", "D"        , IMMEDIATE,   4,   3],
    0xA3  : ["SUB", "D"        , INDEXED  ,   6,   2],
    0x3F  : ["SWI", ""         , INHERENT ,  19,   1],
    0x103F: ["SWI2",""         , INHERENT ,  20,   2],
    0x113F: ["SWI3",""         , INHERENT ,  20,   2],
    0x13  : ["SYNC",""         , INHERENT ,   4,   1],
    0x1F  : ["TFR", ""         , REGISTER ,   7,   2],
    0x0D  : ["TST", ""         , DIRECT   ,   6,   2],
    0x7D  : ["TST", ""         , EXTENDED ,   7,   3],
    0x6D  : ["TST", ""         , INDEXED  ,   6,   2],
    0x4D  : ["TST", "A"        , INHERENT ,   2,   1],
    0x5D  : ["TST", "B"        , INHERENT ,   2,   1],
    0x1010: ["!TRACE",""       , INHERENT ,   2,   2, ILLEGAL],
    0x1011: ["!WTF",""         , INHERENT ,   2,   2, ILLEGAL],
    0x1110: ["!WTF",""         , INHERENT ,   2,   2, ILLEGAL],
    0x1111: ["!WTF",""         , INHERENT ,   2,   2, ILLEGAL],
    0x18  : ["!SCC",""         , INHERENT ,   3,   1, ILLEGAL],
    }


  function error(msg) {
    throw new Error(fmt('ERROR [%s:%4x]: %s', self.name, PC, msg));
  }

  function warning(msg) {
    console.log(fmt('WARNING [%s:%4x]: %s', self.name, PC, msg));
  }

  function fetch8(address, noerror, norom) {
    if (!noerror && address in READ_HOOKS)
      READ_HOOKS[address](address);

    if (!norom) {
      for (var offset in ROM) {
        var delta = address - offset;
        var rom = ROM[offset];
        if (0 <= delta && delta < rom.length)
          return rom[delta];
      }
    }

    if (address < RAM.length)
      return RAM[address];

    for (var i = 0; i < PIAS.length; ++i) {
      var pia = PIAS[i];
      var offset = address - pia.address;
      if (0 <= offset && offset < 4)
        return pia.read(offset);
    }

    if (address >= IO.start && address < IO.start + IO.length)
      return IO[address];

    if (!noerror)
      warning('Read from invalid address $' + x16(address));
  }

  function fetch16(address, noerr) {
    return fetch8(address, noerr) * 256 + fetch8(address + 1, noerr);
  }

  this.fetchRam = function (address) { return RAM[address] }

  function nextByte() {
    return fetch8(PC++);
  }

  function nextWord() {
    return fetch16((PC += 2) - 2);
  }

  function s8(u) { return u - ((u & 0x80) << 1) }
  function s16(u) { return u - ((u & 0x8000) << 1) }

  var NZ = 'NZ', NZC = 'NZC', NZV = 'NZV', NZVC = 'NZVC';

  function store8(addr, value, flags) {
    var uvalue = value & 0xFF;
    if (typeof addr === 'string') {
      switch (addr) {
      case 'CC': CC(uvalue); break;
      case 'A': A = uvalue; break;
      case 'B': B = uvalue; break;
      case 'DP': DP = uvalue; break;
      default: error('Invalid store8 destination ' + addr);
      }
    } else if (addr < RAM.length) {
      RAM[addr] = uvalue;
    } else {
      for (var i = 0; i < PIAS.length; ++i) {
        var pia = PIAS[i];
        var offset = addr - pia.address;
        if (0 <= offset && offset < 4) {
          pia.write(offset, value);
          break;
        }
      }
      if (i >= PIAS.length) {
        if (addr >= IO.start && addr < IO.start + IO.length)
          IO[addr] = uvalue;
        else
          warning('Invalid store to address $' + x16(addr));
      }
    }
    if (flags)
      tst8(flags, value);
    if (addr in WRITE_HOOKS)
      WRITE_HOOKS[addr](uvalue, addr);
  }

  function store16(addr, value, flags) {
    if (typeof addr === 'number') {
      store8(addr, value >> 8);
      store8(addr+1, value & 0xFF);
    } else {
      switch (addr) {
      case 'D': D(value); break;
      case 'X': X = value; break;
      case 'Y': Y = value; break;
      case 'S': S = value; break;
      case 'U': U = value; break;
      case 'PC': PC = value; break;
      default: error('Invalid store16 destination ' + addr);
      }
    }
    if (flags)
      tst16(flags, value);
  }

  this.patch = function (address, value) {
    for (var offset in ROM) {
      var delta = address - offset;
      var rom = ROM[offset];
      if (0 <= delta && delta < rom.length)
        return rom[delta] = value;
    }
    error('No rom for patch');
  }

  function tst8(flags, value) {
    if (flags.indexOf('N') > -1) N = 1 & (value >> 7);
    if (flags.indexOf('Z') > -1) Z = (0 === (value & 0xFF)) ? 1 : 0;
    if (flags.indexOf('V') > -1) V = (value < -0x80 || value > 0x7F) ? 1 : 0;
    if (flags.indexOf('C') > -1) C = 1 & value >> 8;
  }

  function tst16(flags, value) {
    if (flags.indexOf('N') > -1) N = 1 & (value >> 15);
    if (flags.indexOf('Z') > -1) Z = (0 === (value & 0xFFFF)) ? 1 : 0;
    if (flags.indexOf('V') > -1) V = (value < -0x8000 || value > 0x7FFF) ? 1 : 0;
    if (flags.indexOf('C') > -1) C = 1 & value >> 16;
  }


  function reg(r, v) {
    if (undef(v)) {
      switch (r) {
      case 'CC': return CC();
      case 'A':  return A;
      case 'B':  return B;
      case 'D':  return D();
      case 'DP': return DP;
      case 'X':  return X;
      case 'Y':  return Y;
      case 'S':  return S;
      case 'U':  return U;
      case 'PC': return PC;
      default: error('Invalid reg name ' + r);
      }
    } else {
      switch (r) {
      case 'CC': CC(v); break;
      case 'A':  A = v; break;
      case 'B':  B = v; break;
      case 'D':  D(v); break;
      case 'DP': DP = v; break;
      case 'X':  X = v; break;
      case 'Y':  Y = v; break;
      case 'S':  S = v; break;
      case 'U':  U = v; break;
      case 'PC': PC = v; break;
      default: error(fmt('Invalid reg name "%s"', r));
      }
    }
  }

  // 6800 points below last push, 6809 points at it
  var stackAdjust = (partNumber === 6809) ? 0 : 1;

  function push8(value, stack) {
    stack = stack || 'S';
    reg(stack, 0xFFFF & (reg(stack) - 1));
    store8(reg(stack) + stackAdjust, value);
  }
  function push16(value, stack) {
    stack = stack || 'S';
    reg(stack, 0xFFFF & (reg(stack) - 2));
    store16(reg(stack) + stackAdjust, value);
  }

  function pull8(stack) {
    stack = stack || 'S';
    reg(stack, reg(stack) + 1);
    return fetch8(reg(stack) - 1 + stackAdjust);
  }
  function pull16(stack) {
    stack = stack || 'S';
    reg(stack, reg(stack) + 2);
    return fetch16(reg(stack) - 2 + stackAdjust);
  }

  // Unsigned 2's complement adds
  function add8(v1, v2, c, flags) {
    if (typeof c === 'string') {  // c arg is optional
      flags = c;
      c = 0;
    }
    var sum = (v1 & 0xFF) + (v2 & 0xFF) + (c & 0xFF);
    var result = sum & 0xFF;
    if (flags.indexOf('N') > -1) N = 1 & (result >> 7);
    if (flags.indexOf('Z') > -1) Z = result ? 0 : 1;
    if (flags.indexOf('V') > -1) V = 1 & ((v1 ^ v2 ^ sum ^ (sum>>1)) >> 7);
    if (flags.indexOf('C') > -1) C = 1 & (sum >> 8);
    return result;
  }

  function add16(v1, v2, c, flags) {
    if (typeof c === 'string') {  // c arg is optional
      flags = c;
      c = 0;
    }
    var sum = (v1 & 0xFFFF) + (v2 & 0xFFFF) + (c & 0xFFFF);
    var result = sum & 0xFFFF;
    if (flags.indexOf('N') > -1) N = 1 & (result >> 15);
    if (flags.indexOf('Z') > -1) Z = result ? 0 : 1;
    if (flags.indexOf('V') > -1) V = 1 & ((v1 ^ v2 ^ sum ^ (sum>>1)) >> 15);
    if (flags.indexOf('C') > -1) C = 1 & (sum >> 16);
    return result;
  }

  function sub8(v1, v2, c, flags) {
    if (typeof c === 'string') {  // c arg is optional
      flags = c;
      c = 0;
    }
    var sum = v1 - v2 - c;
    var result = sum & 0xFF;
    if (flags.indexOf('N') > -1) N = 1 & (result >> 7);
    if (flags.indexOf('Z') > -1) Z = result ? 0 : 1;
    if (flags.indexOf('V') > -1) V = 1 & ((v1 ^ v2 ^ sum ^ (sum>>1)) >> 7);
    if (flags.indexOf('C') > -1) C = 1 & (sum >> 8);
    return result;
  }

  function sub16(v1, v2, c, flags) {
    if (typeof c === 'string') {  // c arg is optional
      flags = c;
      c = 0;
    }
    var sum = v1 - v2 - c;
    var result = sum & 0xFFFF;
    if (flags.indexOf('N') > -1) N = 1 & (result >> 15);
    if (flags.indexOf('Z') > -1) Z = result ? 0 : 1;
    if (flags.indexOf('V') > -1) V = 1 & ((v1 ^ v2 ^ sum ^ (sum>>1)) >> 15);
    if (flags.indexOf('C') > -1) C = 1 & (sum >> 16);
    return result;
  }

  function tstH(v1, v2, c) {
    H = (((0xF & v1) + (0xF & v2) + (c || 0)) >> 4) & 1;
  }


  function step6800() {
    if (self.RESET) {
      reset();
      elapse(1); // ?
      return;
    }
    if (self.NMI && defined(S)) {
      nmi6800();
      elapse(1); // ?
      return;
    }
    if (self.IRQ && !I) {
      irq6800();
      elapse(1); // ?
      return;
    }
    if (self.WAIT) {
      elapse(1);
      return;
    }

    /*
    if (defined(A) && A != (0xFF & A)) error('Invalid A: ' + A);
    if (defined(B) && B != (0xFF & B)) error('Invalid B: ' + B);
    if (defined(X) && X !== (0xFFFF & X)) error('Invalid X: ' + X);
    if (defined(S) && S !== (0xFFFF & S)) error('Invalid S: ' + S);
*/

    var opPC = PC;

    var opcode = nextByte();
    var record = INSTRUCTIONS_6800[opcode];

    if (!record) error('Invalid opcode ' + opcode + ' @ ' + PC);

    var mnemonic = record[0];
    var register = record[1];
    var cycles = record[2];
    var addrmode = record[3];

    elapse(cycles);

    var width = (register === 'X' || register === 'S') ? 16 : 8;

    var addr;
    switch (addrmode) {
    case DIRECT:      addr = nextByte(); break;
    case EXTENDED:    addr = nextWord(); break;
    case IMMEDIATE:   addr = PC++; break;
    case LIMMEDIATE:  addr = PC; PC += 2; break;
    case INDEXED:     addr = X + nextByte(); break;
    case RELATIVE:    addr = PC + s8(nextByte()) + 1; break;
    case INHERENT:    addr = register; break;
    default: error('Invalid addressing mode ' + addrmode); break;
    }

    var R;
    if (register) R = reg(register);

    var store = (width === 16) ? store16 : store8;
    var add =   (width === 16) ? add16   : add8;
    var sub =   (width === 16) ? sub16   : sub8;
    var tst =   (width === 16) ? tst16   : tst8;

    function fetch() {
      fetch = null;  // Call only once!
      if (typeof addr === 'string')
        return R;
      else if (width === 16)
        return fetch16(addr);
      else
        return fetch8(addr);
    }


    if (mnemonic === "ABA") {
      tstH(A, B);
      A = add(A, B, NZVC);

    } else if (mnemonic === 'ADC') {
      var v = fetch();
      tstH(R, v, C)
      store(register, add(R, v, C, NZVC));

    } else if (mnemonic === 'ADD') {
      var v = fetch();
      tstH(R, v);
      store(register, add(R, v, NZVC));

    } else if (mnemonic === 'AND') {
      store(register, R & fetch(), (register === 'CC') ? '' : NZV);

    } else if (mnemonic === 'ASL') {
      var v = fetch();
      store(addr, add(v, v, NZVC));

    } else if (mnemonic === 'ASR') {
      var v = fetch();
      store(addr, v >> 1, NZ);
      C = v & 1;
      V = N ^ C;

    } else if (mnemonic === 'BCC') {
      if (!C) PC = addr;
    } else if (mnemonic === 'BCS') {
      if (C) PC = addr;
    } else if (mnemonic === 'BEQ') {
      if (Z) PC = addr;
    } else if (mnemonic === 'BGE') {
      if (N === V) PC = addr;
    } else if (mnemonic === 'BGT') {
      if (N === V && !Z) PC = addr;
    } else if (mnemonic === 'BHI') {
      if (!Z && !C) PC = addr;
    } else if (mnemonic === 'BLE') {
      if (N !== V || Z) PC = addr;
    } else if (mnemonic === 'BLS') {
      if (Z || C) PC = addr;
    } else if (mnemonic === 'BLT') {
      if (N !== V) PC = addr;
    } else if (mnemonic === 'BMI') {
      if (N) PC = addr;
    } else if (mnemonic === 'BNE') {
      if (!Z) PC = addr;
    } else if (mnemonic === 'BPL') {
      if (!N) PC = addr;
    } else if (mnemonic === 'BRA') {
      PC = addr;
    } else if (mnemonic === 'BSR') {
      push16(PC)
      PC = addr;
    } else if (mnemonic === 'BVC') {
      if (!V) PC = addr;
    } else if (mnemonic === 'BVS') {
      if (V) PC = addr;

    } else if (mnemonic === 'BIT') {
      tst(NZV, R & fetch());

    } else if (mnemonic === 'CBA') {
      sub(A, B, NZVC);

    } else if (mnemonic === 'CLC') {
      C = 0;

    } else if (mnemonic === 'CLI') {
      I = 0;

    } else if (mnemonic === 'CLR') {
      store(addr, 0);
      N = V = C = 0; Z = 1;

    } else if (mnemonic === 'CLV') {
      V = 0;

    } else if (mnemonic === 'CMP' || mnemonic === 'CP') {
      sub(R, fetch(), NZVC);

    } else if (mnemonic === 'COM') {
      store(addr, ~fetch(), NZ);
      V = 0;
      C = 1;

    } else if (mnemonic === 'DAA') {
      var ahi = (A >> 4) & 0xF, alo = A & 0xF;
      if (C || ahi > 9 || (ahi > 8 && alo > 9)) {
        A += 0x60;
        C = 1;
      }
      if (H || alo > 9)
        A += 6;
      A &= 0xFF;
      tst(NZ, A);

    } else if (mnemonic === 'DEC') {
      store(addr, sub(fetch(), 1, NZV));

    } else if (mnemonic === 'DE') {
      store(addr, sub(fetch(), 1, register === 'X' ? 'Z' : null));

    } else if (mnemonic === 'EOR') {
      store(register, R ^ fetch(), NZV);

    } else if (mnemonic === 'INC') {
      store(addr, add(fetch(), 1, NZV));

    } else if (mnemonic === 'IN') {
      store(addr, add(fetch(), 1, register === 'X' ? 'Z' : null));

    } else if (mnemonic === 'JMP') {
      PC = addr;

    } else if (mnemonic === 'JSR') {
      push16(PC);
      PC = addr;

    } else if (mnemonic === 'LDA' || mnemonic === 'LD') {
      store(register, fetch(), NZ);
      V = 0;

    } else if (mnemonic === 'LSR') {
      var v = fetch();
      store(addr, 0x7F & (v >> 1), NZ);
      C = v & 1;
      V = N ^ Z;

    } else if (mnemonic === 'NEG') {
      storeDoesntReturnAnythingSoThisIsABug();
      var result = store(addr, -fetch(), NZ);
      V = (result === 0x80) ? 1 : 0;
      C = 1 - Z;

    } else if (mnemonic === 'NOP') {
      ;

    } else if (mnemonic === 'ORA') {
      store(register, R | fetch(), NZ);
      V = 0;

    } else if (mnemonic === 'PSH') {
      push8(R);

    } else if (mnemonic === 'PUL') {
      reg(register, pull8());

    } else if (mnemonic === 'ROL') {
      store(addr, (fetch() << 1) | C, NZC);
      V = N ^ C;

    } else if (mnemonic === 'ROR') {
      var v = fetch();
      var msb = C << 7;
      store(addr, msb | (v >> 1), NZ);
      C = v & 1;
      V = N ^ C;

    } else if (mnemonic === 'RTI') {
      CC(pull8());
      B = pull8();
      A = pull8();
      X = pull16();
      PC = pull16();

    } else if (mnemonic === 'RTS') {
      PC = pull16()

    } else if (mnemonic === 'SBA') {
      A = sub(A, B, NZVC);

    } else if (mnemonic === 'SBC') {
      store(register, sub(R, fetch(), C, NZVC));

    } else if (mnemonic === 'SEC') {
      C = 1;

    } else if (mnemonic === 'SEI') {
      I = 1;

    } else if (mnemonic === 'SEV') {
      V = 1;

    } else if (mnemonic === 'STA' || mnemonic === 'ST') {
      store(addr, R, NZ);
      V = 0;

    } else if (mnemonic === 'SUB') {
      store(register, sub(R, fetch(), NZVC));

    } else if (mnemonic === 'SWI') {
      push16(PC);
      push16(X);
      push8(A);
      push8(B);
      push8(CC());
      if (mnemonic === 'SWI') // ?
        I = F = 1;
      PC = fetch16(0xFFFA);

    } else if (mnemonic === 'TAB') {
      B = A;
      tst(NZ, B);
      V = 0;

    } else if (mnemonic === 'TBA') {
      A = B;
      tst(NZ, A);
      V = 0;

    } else if (mnemonic === 'TAP') {
      CC(A);

    } else if (mnemonic === 'TPA') {
      A = CC();

    } else if (mnemonic === 'TST') {
      V = 0;
      C = 0;
      tst(NZ, fetch());

    } else if (mnemonic === 'TSX') {
      X = S;

    } else if (mnemonic === 'TXS') {
      S = X;

    } else if (mnemonic === 'WAI') {
      push16(PC);
      push16(X);
      push8(A);
      push8(B);
      push8(CC());
      self.WAIT = true;
      return false;

    } else {
      console.error("Invalid mnemonic " + mnemonic);
      return false;
    }

    // If in a tight busy loop, we can save ourselves some cycles.
    // There's no single instruction that changes anything and loops,
    // so we're just waiting for an interrupt.
    if (PC === opPC)
      self.WAIT = true;

    return true;
  }


  function step6809(traceHook) {
    if (self.RESET) {
      reset();
      elapse(1);
      return;
    }
    if (self.NMI) {
      if (defined(S)) {
        nmi6809();
        elapse(1);
        return;
      } else if (self.WAIT === 'sync') {
        self.WAIT = false;
      }
    }
    if (self.FIRQ) {
      if (!F) {
        firq();
        elapse(1);
        return;
      } else if (self.WAIT === 'sync') {
        self.WAIT = false;
      }
    }
    if (self.IRQ) {
      if (!I) {
        irq6809();
        elapse(1);
        return;
      } else if (self.WAIT === 'sync') {
        self.WAIT = false;
      }
    }

    if (self.WAIT) {
      elapse(1);
      return;
    }

    var opPC = PC;
    var opcode = nextByte();
    if (opcode === 0x10 || opcode === 0x11)
      opcode = opcode * 256 + nextByte();
    var record = INSTRUCTIONS_6809[opcode];
    var fallback = !record && (opcode & 0xFF in INSTRUCTIONS_6809);
    if (fallback)
      record = INSTRUCTIONS_6809[opcode & 0xFF];
    if (!record)
      error(fmt('Unrecognised opcode: %2x', opcode));
    var mnemonic = record[0];
    var register = record[1];
    var mode = record[2];
    var timing = record[3];
    var bytes = record[4];
    var illegal = record[5] || fallback;
    var autoincrement;

    var width = BITS[register];

    if (fallback)
      timing += 1;

    elapse(timing);

    var addr;
    if (mode === EXTENDED) {
      addr = nextWord();

    } else if (mode === INHERENT) {
      addr = register;

    } else if (mode === RELATIVE) {
      addr = s8(nextByte());
      addr += PC;

    } else if (mode === LRELATIVE) {
      addr = s16(nextWord());
      addr += PC;

    } else if (mode === DIRECT) {
      addr = DP * 256 + nextByte();

    } else if (mode === REGISTER) {
      TFR_REGS = ['D', 'X', 'Y',  'U',  'S', 'PC',  null, null,
                  'A', 'B', 'CC', 'DP'];
      var postbyte = nextByte();
      addr = [TFR_REGS[postbyte & 0xF], TFR_REGS[postbyte >> 4]];

    } else if (mode === IMMEDIATE) {
      addr = PC;
      PC += (width === 16) ? 2 : 1;

    } else if (mode === INDEXED) {
      var postbyte = nextByte();
      var subregister = 'XYUS'[(postbyte>>5) & 0x3];
      var indirect = ((postbyte & 0x10) !== 0);
      var submode = postbyte & 0xF
      if (postbyte === 0x9F) {
        // Extended indirect
        var ptr = nextWord();
        addr = fetch16(ptr);
        elapse(5);
      } else if ((postbyte & 0x80) === 0) {
        // 5-bit offset from register contained in postbyte
        if (indirect) submode -= 0x10;
        addr = reg(subregister) + submode;
        elapse(1);
      } else {
        var addr = reg(subregister);
        var offset;
        if (submode === 0x4) {
          // No offset from register
          offset = 0;
        } else if (submode === 0x8) {
          // 8-bit offset from register
          offset =  s8(nextByte());
          elapse(1);
        } else if (submode === 0x9) {
          // 16-bit offset from register
          offset = s16(nextWord());
          elapse(4);
        } else if (submode === 0x6) {
          // A-register offset from register
          offset = A;
          elapse(1);
        } else if (submode === 0x5) {
          // B-register offset from register
          offset = B;
          elapse(1);
        } else if (submode === 0xB) {
          // D-register offset from register
          offset = D();
          elapse(4);
        } else if (submode === 0) {
          // Post-increment by 1
          autoincrement = 1;
          offset = 0;
          elapse(2);
        } else if (submode === 1) {
          // Post-increment by 2
          autoincrement = 2;
          offset = 0;
          elapse(3);
        } else if (submode === 2) {
          // Pre-decrement by 1
          autoincrement = -1;
          offset = -1;  // it's a pre-decrement
          elapse(2);
        } else if (submode === 3) {
          // Pre-decrement by 2
          autoincrement = -2;
          offset = -2;  // it's a pre-decrement
          elapse(3);
        } else if (submode === 0xC) {
          // 8-bit offset from PC
          addr = 0;
          offset = s8(nextByte());
          elapse(1);
        } else if (submode === 0xD) {
          // 16-bit offset from PC
          addr = 0;
          offset = s16(nextWord());
          elapse(5);
        } else {
          error(fmt("Invalid indexing submode %2x in postbyte %2x", submode, postbyte))
        }

        addr += offset;

        if (indirect) {
          addr = fetch16(addr);
          elapse(3);
        }
      }
    }

    // TODO: this might need to move down below the R = reg() line, very possibly
    if (autoincrement && subregister === register)
      debugger;

    if (autoincrement)
      reg(subregister, reg(subregister) + autoincrement);

    var store = (width === 16) ? store16 : store8;
    var add =   (width === 16) ? add16   : add8;
    var sub =   (width === 16) ? sub16   : sub8;
    var tst =   (width === 16) ? tst16   : tst8;

    var R;
    if (register) R = reg(register);

    function fetch() {
      fetch = null;  // Call only once!
      if (typeof addr === 'string')
        return R;
      else if (width === 16)
        return fetch16(addr);
      else
        return fetch8(addr);
    }

    if (mnemonic === 'ABX') {
      X = 0xFFFF & (X + B);

    } else if (mnemonic === 'ADC') {
      var v = fetch();
      tstH(R, v, C)
      store(register, add(R, v, C, NZVC));

    } else if (mnemonic === 'ADD') {
      var v = fetch();
      tstH(R, v);
      store(register, add(R, v, NZVC));

    } else if (mnemonic === 'AND') {
      store(register, R & fetch(), (register === 'CC') ? '' : NZV);

    } else if (mnemonic === 'ASL') {
      store(addr, fetch() << 1, NZC);
      V = N ^ C;

    } else if (mnemonic === 'ASR') {
      var v = fetch();
      store(addr, v >> 1, NZ);
      C = v & 1;

    } else if (mnemonic.substr(-3) === 'BCC') {
      if (!C) { PC = addr; if (mnemonic[0] === 'L') elapse(1); }
    } else if (mnemonic.substr(-3) === 'BCS') {
      if (C) { PC = addr; if (mnemonic[0] === 'L') elapse(1); }
    } else if (mnemonic.substr(-3) === 'BEQ') {
      if (Z) { PC = addr; if (mnemonic[0] === 'L') elapse(1); }
    } else if (mnemonic.substr(-3) === 'BGE') {
      if (N === V) { PC = addr; if (mnemonic[0] === 'L') elapse(1); }
    } else if (mnemonic.substr(-3) === 'BGT') {
      if (N === V && !Z) { PC = addr; if (mnemonic[0] === 'L') elapse(1); }
    } else if (mnemonic.substr(-3) === 'BHI') {
      if (!Z && !C) { PC = addr; if (mnemonic[0] === 'L') elapse(1); }
    } else if (mnemonic.substr(-3) === 'BLE') {
      if (N !== V || Z) { PC = addr; if (mnemonic[0] === 'L') elapse(1); }
    } else if (mnemonic.substr(-3) === 'BLS') {
      if (Z || C) { PC = addr; if (mnemonic[0] === 'L') elapse(1); }
    } else if (mnemonic.substr(-3) === 'BLT') {
      if (N !== V) { PC = addr; if (mnemonic[0] === 'L') elapse(1); }
    } else if (mnemonic.substr(-3) === 'BMI') {
      if (N) { PC = addr; if (mnemonic[0] === 'L') elapse(1); }
    } else if (mnemonic.substr(-3) === 'BNE') {
      if (!Z) { PC = addr; if (mnemonic[0] === 'L') elapse(1); }
    } else if (mnemonic.substr(-3) === 'BPL') {
      if (!N) { PC = addr; if (mnemonic[0] === 'L') elapse(1); }
    } else if (mnemonic.substr(-3) === 'BRA') {
      PC = addr;
    } else if (mnemonic.substr(-3) === 'BRN') {
      ;
    } else if (mnemonic.substr(-3) === 'BSR') {
      push16(PC)
      PC = addr;
    } else if (mnemonic.substr(-3) === 'BVC') {
      if (!V) { PC = addr; if (mnemonic[0] === 'L') elapse(1); }
    } else if (mnemonic.substr(-3) === 'BVS') {
      if (V) { PC = addr; if (mnemonic[0] === 'L') elapse(1); }

    } else if (mnemonic === 'BIT') {
      tst(NZV, R & fetch());

    } else if (mnemonic === 'CLR') {
      store(addr, 0, NZVC);

    } else if (mnemonic === 'CMP') {
      sub(R, fetch(), NZVC);

    } else if (mnemonic === 'COM') {
      store(addr, ~fetch(), NZV);
      C = 1;

    } else if (mnemonic === 'CWAI') {
      CC(CC() & fetch());
      E = 1;
      push16(PC);
      push16(U);
      push16(Y);
      push16(X);
      push8(DP);
      push8(B);
      push8(A);
      push8(CC());
      self.WAIT = true;

    } else if (mnemonic === 'DAA') {
      var ahi = (A >> 4) & 0xF, alo = A & 0xF;
      if (C || ahi > 9 || (ahi > 8 && alo > 9)) {
        A += 0x60;
        C = 1;
      }
      if (H || alo > 9)
        A += 6;
      A &= 0xFF;
      tst(NZ, A);

    } else if (mnemonic === 'DEC') {
      store(addr, sub(fetch(), 1, NZV));

    } else if (mnemonic === 'EOR') {
      store(register, R ^ fetch(), NZV);

    } else if (mnemonic === 'EXG') {
      var tmp = addr[0] ? reg(addr[0]) : -1
      if (addr[0])
        reg(addr[0], addr[1] ? reg(addr[1]) : -1)
      if (addr[1])
        reg(addr[1], tmp);

    } else if (mnemonic === 'INC') {
      store(addr, add(fetch(), 1, NZV));

    } else if (mnemonic === 'JMP') {
      PC = addr;

    } else if (mnemonic === 'JSR') {
      push16(PC);
      PC = addr;

    } else if (mnemonic === 'LD') {
      store(register, fetch(), NZV);

    } else if (mnemonic === 'LEA') {
      store(register, addr);
      if (register === 'X' || register === 'Y')
        tst('Z', addr);

    } else if (mnemonic === 'LSR') {
      var v = fetch();
      store(addr, v >> 1, NZ);
      C = v & 1;

    } else if (mnemonic === 'MUL') {
      D(A * B);
      Z = D() ? 0 : 1;
      C = 1 & (B >> 7);

    } else if (mnemonic === 'NEG') {
      var v = fetch();
      C = (v !== 0) ? 1 : 0;
      store(addr, sub(0, v, NZV));

    } else if (mnemonic === 'NOP') {
      ;

    } else if (mnemonic === '!HCF') {
      error('Halting and catching fire as instructed.');

    } else if (mnemonic === 'OR') {
      store(register, R | fetch(), register === 'CC' ? '' : NZV);

    } else if (mnemonic.substr(0,3) === 'PSH') {
      var stack = mnemonic.substr(-1);
      var mask = fetch()
      if (mask & 0x80) { elapse(2); push16(PC, stack); }
      if (mask & 0x40) { elapse(2); push16(reg((stack === 'S') ? 'U' : 'S'), stack); }
      if (mask & 0x20) { elapse(2); push16(Y, stack); }
      if (mask & 0x10) { elapse(2); push16(X, stack); }
      if (mask & 0x08) { elapse(1); push8(DP, stack); }
      if (mask & 0x04) { elapse(1); push8(B, stack); }
      if (mask & 0x02) { elapse(1); push8(A, stack); }
      if (mask & 0x01) { elapse(1); push8(CC(), stack); }

    } else if (mnemonic.substr(0,3) === 'PUL') {
      var stack = mnemonic.substr(-1);
      var mask = fetch()
      if (mask & 0x01) { elapse(1); CC(pull8(stack)); }
      if (mask & 0x02) { elapse(1); A  = pull8(stack); }
      if (mask & 0x04) { elapse(1); B  = pull8(stack); }
      if (mask & 0x08) { elapse(1); DP = pull8(stack); }
      if (mask & 0x10) { elapse(2); X  = pull16(stack); }
      if (mask & 0x20) { elapse(2); Y  = pull16(stack); }
      if (mask & 0x40) { elapse(2); reg(stack === 'S' ? 'U' : 'S', pull16(stack)); }
      if (mask & 0x80) { elapse(2); PC = pull16(stack); }

    } else if (mnemonic === 'ROL') {
      store(addr, (fetch() << 1) | C, NZVC);

    } else if (mnemonic === 'ROR') {
      var v = fetch();
      var msb = C << 7;
      C = v & 1;
      store(addr, msb | (v >> 1), NZ);

    } else if (mnemonic === 'RTI') {
      CC(pull8());
      if (E) {
        A = pull8();
        B = pull8();
        DP = pull8();
        X = pull16();
        Y = pull16();
        U = pull16();
        elapse(9);
      }
      PC = pull16();

    } else if (mnemonic === 'RTS') {
      PC = pull16()

    } else if (mnemonic === 'SBC') {
      store(register, sub(R, fetch(), C, NZVC));

    } else if (mnemonic === 'SEX') {
      tst8(NZ, B);
      D(s8(B));

    } else if (mnemonic === 'ST') {
      store(addr, R, NZV);

    } else if (mnemonic === 'SUB') {
      store(register, sub(R, fetch(), NZVC));

    } else if (mnemonic.substr(0,3) === 'SWI' || mnemonic === 'RESET') {
      E = 1;
      push16(PC);
      push16(U);
      push16(Y);
      push16(X);
      push8(DP);
      push8(B);
      push8(A);
      push8(CC());
      if (mnemonic === 'SWI')
        I = F = 1;
      PC = fetch16({SWI: 0xFFFA,
                    SWI2: 0xFFF4,
                    SWI3: 0xFFF2,
                    RESET: 0xFFFE}[mnemonic]);

    } else if (mnemonic === 'SYNC') {
      self.WAIT = 'sync';

    } else if (mnemonic === 'TFR') {
      if (addr[0])
        reg(addr[0], addr[1] ? reg(addr[1]) : -1);

    } else if (mnemonic === 'TST') {
      tst(NZV, fetch());

      /*
    } else if (mnemonic === '!TRACE') {
      console.log(clock(),
                  trace6809(opPC, PC-opPC,
                            fmt('%s%s  %s', mnemonic, register, param)));
*/

    } else {
      error(fmt('Unimplemented opcode %4x / %s', opcode, mnemonic));
    }

    return true;
  }

  self.reset = reset;

  function reset(addr) {
    self.FIRQ = self.IRQ = self.NMI = self.RESET = false;
    self.S = undefined;
    self.WAIT = false;
    if (undef(addr))
      addr = fetch16(0xFFFE);
    //console.log('\t<' + self.name + '.RESET>');
    PC = addr;
  }

  function nmi6800() {
    self.NMI = false;
    self.WAIT = false;
    console.log('\t<' + self.name + '.NMI>');
    push16(PC);
    push16(X);
    push8(A);
    push8(B);
    push8(CC());
    I = 1;
    PC = fetch16(0xFFFC);
  }

  function nmi6809() {
    self.NMI = false;
    self.WAIT = false;
    var addr = fetch16(0xFFFC);
    //console.log('\t<' + self.name + '.NMI>');
    E = 1;
    push16(PC);
    push16(U);
    push16(Y);
    push16(X);
    push8(DP);
    push8(B);
    push8(A);
    push8(CC());
    F = I = 1;
    PC = addr;
  }

  function firq() {
    self.FIRQ = false;
    self.WAIT = false;
    //console.log('\t<' + self.name + '.FIRQ>');
    E = 0;
    push16(PC);
    push8(CC());
    F = I = 1;
    PC = fetch16(0xFFF6);
  }

  function irq6800() {
    self.IRQ = false;
    self.WAIT = false;
    //console.log('\t<' + self.name + '.IRQ>');
    push16(PC);
    push16(X);
    push8(A);
    push8(B);
    push8(CC());
    I = 1;
    PC = fetch16(0xFFF8);
  }

  function irq6809() {
    self.IRQ = false;
    self.WAIT = false;
    //console.log('\t<' + self.name + '.IRQ>');
    E = 1;
    push16(PC);
    push16(U);
    push16(Y);
    push16(X);
    push8(DP);
    push8(B);
    push8(A);
    push8(CC());
    I = 1;
    PC = fetch16(0xFFF8);
  }

  this.showram = function (andzero) {
    console.log('RAM:');
    for (var i = 0; i < RAM.length; ++i)
      if (defined(RAM[i]) && (andzero || RAM[i]))
        console.log('  ' + x16(i) + ': ' + x8(RAM[i]) + ' ' + s8(RAM[i]));
  }

  this.vectors = function () {
    var result = {
      'reset': fetch16(0xFFFE),
      'nmi': fetch16(0xFFFC),
      'swi': fetch16(0xFFFA),
      'irq': fetch16(0xFFF8)
    }
    if (partNumber === 6809) {
      result['firq'] = fetch16(0xFFF6);
      result['swi2'] = fetch16(0xFFF4);
      result['swi3'] = fetch16(0xFFF2);
    }
    return result;
  }

  this.time = function (func) {
    var cpustart = clock();
    var realstart = +new Date();
    func();
    var realelapsed = (+new Date() - realstart) / 1000;
    var cpuelapsed = clock() - cpustart;
    return cpuelapsed / realelapsed / 1000000;  // Speed in MHz
  }

  this.run = function (limit) {
    var initial = clock();
    for (var i = 0; !self.WAIT && !((clock()-initial) >= limit); ++i) {
      var pc = PC;
      if (!this.step())
        break;
      if (pc === PC) {
        console.error('INFINITE LOOP');
        break;
      }
    }
  }

  function elapse(cycles) {
    CLOCK += cycles;
    if (CLOCK > 1000000) {
      ++MCLOCK;
      CLOCK -= 1000000;
    }
  }

  this.clock = clock;
  function clock() { return (MCLOCK + CLOCK / 1000000) / clockRate }

  this.hookWrite = function (addr, callback) { WRITE_HOOKS[addr] = callback }
  this.hookRead = function (addr, callback) { READ_HOOKS[addr] = callback }

  this.connectPia = function (pia, addr) {
    pia.address = addr;
    PIAS.push(pia);
  }

  this.fetch8 = fetch8;
  this.fetch16 = fetch16;
  this.store8 = store8;
  this.store16 = store16;

  this.reg = reg;

  this.PC = function (v) {
    if (undef(v)) return PC;
    PC = v;
  }

  if (partNumber === 6809) {
    this.step = step6809;
    this.disassemble = disassemble6809;
  } else {
    this.step = step6800;
    this.disassemble = disassemble6800;
  }
}


function Pia6821(mpu, address) {
  var CR = new Uint8Array(2);  // [A, B] in each case
  var P = new Uint8Array(2);
  var DDR = new Uint8Array(2);
  var OUTPUT = [null, null];

  mpu.connectPia(this, address);

  function reset() { CR[0] = CR[1] = P[0] = P[1] = DDR[0] = DDR[1] = 0 }

  function interruptEnabledOnIndex(index) {
    var cr = CR[index];
    return cr & 0x1 || (!(cr & 0x20) && (cr & 0x08));
  }

  this.connectPeripheralOutput = function (index, callback) { OUTPUT[index] = callback }

  this.peripheralInput = function (index, value, mask) {
    if (undef(mask)) mask = ~DDR[index];
    mask &= ~DDR[index];
    P[index] = (value & mask) | (P[index] & ~mask);
    if (interruptEnabledOnIndex(index))
      mpu.IRQ = true;
  }

  this.write = function (offset, value) {
    var type = offset & 1;
    var index = offset >> 1;
    if (type === 0) {
      if (CR[index] & 0x4) {
        P[index] = (value & DDR[index]) | (P[index] & ~DDR[index]);
        if (OUTPUT[index])
          OUTPUT[index](P[index]);
      } else {
        DDR[index] = value;
      }
    } else {
      CR[index] = value;
    }
  }

  this.read = function (offset) {
    var type = offset & 1;
    var index = offset >> 1;
    if (type === 0) {
      if (CR[index] & 0x4) {
        return P[index];
      } else {
        return DDR[index];
      }
    } else {
      var result = CR[index];
      if (CR[index] & 0xC0) {
        if (interrupEnabledOnIndex(index))
          mpu.IRQ = false;
        CR[index] &= 0x3F;  // clear interrupt status bits
      }
      return result;
    }
  }

  this.toString = function () {
    return fmt('%4x: A[%s] %2x %2x / B[%s] %2x %2x', address,
               DDR[0] === 0 ? 'ii' : DDR[0] === 0xFF ? 'oo' : x8(DDR[0]), P[0], CR[0],
               DDR[1] === 0 ? 'ii' : DDR[1] === 0xFF ? 'oo' : x8(DDR[1]), P[1], CR[1]);
  }
}


exports.Mpu = exports.Cpu = Cpu;
exports.Pia = Pia6821;
exports.x16 = x16;
exports.x8 = x8;