import sys, os, getopt
sys.path.append(os.path.join(os.path.dirname(__file__), 'build'))
from d6802 import OPS

ROM = {}

opts,args = getopt.getopt(sys.argv[1:], '')
for arg in args:
  addr,rom = arg.split('=')
  ROM[int(addr, 16)] = map(ord, open(rom, 'rb').read())

def memByte(address):
  for offset, rom in ROM.items():
    delta = address - offset
    if 0 <= delta and delta < len(rom):
      return rom[delta]
  error('Bad address %X' % address)

def memWord(address):
  return memByte(address) * 256 + memByte(address + 1)

def memSbyte(address):
  u = memByte(address)
  if u & 0x80:
    u -= 0x100
  return u

def memShort(address):
  u = memWord(address)
  if u & 0x8000:
    u -= 0x10000
  return u

def nextByte():
  global PC
  result = memByte(PC)
  PC += 1
  return result

def nextWord():
  global PC
  result = memWord(PC)
  PC += 2
  return result

def nextSbyte():
  u = memSbyte(PC)
  nextByte()
  return u

def nextShort():
  u = memShort(PC)
  nextWord()
  return u

TODO = set()
TODO.add(memWord(0xFFF8))
TODO.add(memWord(0xFFFA))
TODO.add(memWord(0xFFFC))
TODO.add(memWord(0xFFFE))
print TODO

DISA = {}

NOFALL = ['BRA', 'JMP', 'RTI', 'RTS']

while TODO:
  addr = TODO.pop()
  if addr not in DISA:
    PC = addr
    opcode = nextByte()
    dasm = OPS.get(opcode, '???')
      
    if 'PC + $nn' in dasm:
      dest = nextSbyte() + addr + 2
      dasm = dasm.replace('PC + $nn', '$%04X' % dest)
      TODO.add(dest)
    elif 'nnnn' in dasm:
      dasm = dasm.replace('nnnn', '%04X' % nextWord())
    elif 'nn' in dasm:
      dasm = dasm.replace('nn', '%02X' % nextByte())

    if dasm.split()[0] not in NOFALL:
      TODO.add(PC)

    bytes = ' '.join(['%02X' % memByte(b) for b in range(addr, PC)])

    #for b in range(addr+1, PC):
    #  VISITED(b)

    DISA[addr] = '%04X: %-9s %s' % (addr, bytes, dasm)

ks = DISA.keys()
ks.sort()
for k in ks:
  print DISA[k]

