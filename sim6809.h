#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <setjmp.h>

#include <stack>
#include <deque>

typedef  int8_t  int8;
typedef uint8_t uint8;
typedef  int16_t  int16;
typedef uint16_t uint16;

class MC6809 {

  union {
    struct { 
      int8 B; 
      int8 A;
    };
    int16 D;
  } accum;

  uint16 X, Y, S, U;
  uint16 PC;
  uint8 DP;

  union {
    struct {
      uint8 E:1;
      uint8 F:1;
      uint8 H:1;
      uint8 I:1;
      uint8 N:1;
      uint8 Z:1;
      uint8 V:1;
      uint8 C:1;
    };
    int8 CC;
  } cc;

  int clock;

  struct memblock {
    uint16 start;
    uint16 length;
    uint8* mem;
  };
  
  uint8 ram_[0xD000];
  std::deque<memblock> rom_;
  std::stack<jmp_buf*> callStack;

  const char* assembly;

  void loadRom(uint16 addr, const char *filename) {
    memblock block;
    block.start = addr;
    FILE *f = fopen(filename, "rb");
    fseek(f, 0, SEEK_END);
    block.length = ftell(f);
    fseek(f, 0, SEEK_SET);

    block.mem = (uint8*) malloc(block.length);
    fread(block.mem, block.length, 1, f);
    fclose(f);

    rom_.push_back(block);
  }

  void elapse(int cycles) {
    trace();
    clock += cycles;
  }

  int8 input8(uint16 address) {
    std::deque<memblock>::iterator i;
    for (i = rom_.begin(); i != rom_.end(); ++i)
      if (i->start <= address && address < i->start + i->length)
        return i->mem[address - i->start];
    
    if (address < sizeof(ram_))
      return ram_[address];

    error("input8(): Attempt to access undefined address");
  }

  int16 input16(uint16 address) {
    return input8(address) * 256 + input8(address + 1);
  }

  void output(uint16 address, int8 value) { output8(address, value); }
  void output8(uint16 address, int8 value) {
    if (address < sizeof(ram_))
      ram_[address] = value;
    else
      error("output8(): Attempt to access undefined address");
  }

  void output(uint16 address, int16 value) { output16(address, value); }
  void output16(uint16 address, int16 value) {
    output8(address, value >> 8);
    output8(address+1, value & 0xFF);
  }

  void error(const char* problem) {
    fprintf(stderr, "ERROR: %s\n", problem);
    exit(-1);
  }

  void sync() {
    error("UNIMPLEMENTED: sync");
  }

public: 
  MC6809() :
    assembly(0),
    clock(0),
    X(0), Y(0), S(0), U(0),
    PC(0), DP(0) {
    accum.D = 0;
    cc.CC = 0;
  }

  void trace() {
    printf("%04X: <%02X %02X:%02X x%04X y%04X s%04X u%04X %c%c%c%c%c%c%c%c %d",
           PC, DP, uint8(accum.A), uint8(accum.B), X, Y, S, U, 
           cc.E?'e':'_', cc.F?'f':'_', cc.H?'h':'_', cc.I?'i':'_', 
           cc.N?'n':'_', cc.Z?'z':'_', cc.V?'v':'_', cc.C?'c':'_', clock);
    if (assembly)
      printf(" | %s", assembly + 30);
    printf("\n");
  }

  void go();
};

int main() {
  MC6809 cpu;
  cpu.go();
  return 0;
}

#define A accum.A
#define B accum.B
#define D accum.D
#define E cc.E
#define F cc.F
#define H cc.H
#define I cc.I
#define N cc.N
#define Z cc.Z
#define V cc.V
#define C cc.C
#define CC cc.CC
