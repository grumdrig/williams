Williams Arcade Machine Emulator
================================

These are a couple incarnations of a Williams arcade machine emulator. The partially functioning one is the JavaScript/HTML version, but it's slow. See below for the abandoned idea version.

See also [my Go version](https://bitbucket.org/grumdrig/williams-go) which works at least better than this one.

And see also [my Rust version](https://bitbucket.org/grumdrig/williams-rust) which is currently mothballed.


Links
-----

* [Sean Riddle's Williams Info](http://seanriddle.com/willy.html)
* [6809 Emulation Page](http://koti.mbnet.fi/~atjs/mc6809/)
* [Robotron FPGA project](https://github.com/sharebrained/robotron-fpga)
* [Robotron Manuals](http://www.robotron-2084.co.uk/manualsrobotron.html)
* [Detailed Defender PCB Theory](http://www.robotron-2084.co.uk/manuals/defender/defender_later_pcb_theory.pdf)


Abandoned Idea
--------------

The concept I was playing with here was to essentially compile arcade
ROMs to C code so that rather than emulating the CPU, it could be
turned into native code just by compiling. Thus each machine
instruction is virtually disassembled and turned into equivalent C
code.

The disassembled-then-generated code is realized as a giant `switch`
statement, which `switch`es on `PC`, the program counter. So each
memory address is a `case` in the `switch`, and during sequential
operation, we fall through from one `case`/instruction address to the
next. Each case block is also `label`ed, so that branching
instructions can be handled by a `goto`.

Implementing branching as a `goto` is only possible, however, when the
destination address or offset is hard-coded, rather than computed. In
these cases, we set the new value of `PC` and `break` out of the
`switch`, to renter at the `case` corresponding to the branched-to
`PC`.

One problem that remains with this approach, however, is that code
entry points are not known without either emulating the entire
codebase, or iterating development until no new entry points are
found, so that is what we must do.

Another problem with this approach is that even if it works, it might
not be better in any significant way than emulation.

Bank switching is another complication to this approach.

The apparent problem that this technique wouldn't work with
self-modifying code isn't an actual problem since we're in ROM, I
think. Unless the execution pointer goes to RAM at some point, which I
doubt.

Test
----
    % make loop

