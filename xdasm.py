import sys, os, string

INHERENT = 'inherent'
IMMEDIATE = 'immediate'
DIRECT = 'direct'
INDEXED = 'indexed'
EXTENDED = 'extended'
RELATIVE = 'relative'
REGISTER = 'register'

ILLEGAL = 'illegal'

BITS = {
  'A': 8,
  'B': 8,
  'D': 16,
  'X': 16,
  'Y': 16,
  'S': 16,
  'U': 16,
  'PC': 16,
  'DP': 8,
  'CC': 8,
  }


OPCODES = {
  0x3A  : ("ABX", ""         , INHERENT ,   3,   1, "-----"),
  0x99  : ("ADC", "A"        , DIRECT   ,   4,   2, "aaaaa"),
  0xB9  : ("ADC", "A"        , EXTENDED ,   5,   3, "aaaaa"),
  0x89  : ("ADC", "A"        , IMMEDIATE,   2,   2, "aaaaa"),
  0xA9  : ("ADC", "A"        , INDEXED  ,   4,   2, "aaaaa"),
  0xD9  : ("ADC", "B"        , DIRECT   ,   4,   2, "aaaaa"),
  0xF9  : ("ADC", "B"        , EXTENDED ,   5,   3, "aaaaa"),
  0xC9  : ("ADC", "B"        , IMMEDIATE,   2,   2, "aaaaa"),
  0xE9  : ("ADC", "B"        , INDEXED  ,   4,   2, "aaaaa"),
  0x9B  : ("ADD", "A"        , DIRECT   ,   4,   2, "aaaaa"),
  0xBB  : ("ADD", "A"        , EXTENDED ,   5,   3, "aaaaa"),
  0x8B  : ("ADD", "A"        , IMMEDIATE,   2,   2, "aaaaa"),
  0xAB  : ("ADD", "A"        , INDEXED  ,   4,   2, "aaaaa"),
  0xDB  : ("ADD", "B"        , DIRECT   ,   4,   2, "aaaaa"),
  0xFB  : ("ADD", "B"        , EXTENDED ,   5,   3, "aaaaa"),
  0xCB  : ("ADD", "B"        , IMMEDIATE,   2,   2, "aaaaa"),
  0xEB  : ("ADD", "B"        , INDEXED  ,   4,   2, "aaaaa"),
  0xD3  : ("ADD", "D"        , DIRECT   ,   6,   2, "-aaaa"),
  0xF3  : ("ADD", "D"        , EXTENDED ,   7,   3, "-aaaa"),
  0xC3  : ("ADD", "D"        , IMMEDIATE,   4,   3, "-aaaa"),
  0xE3  : ("ADD", "D"        , INDEXED  ,   6,   2, "-aaaa"),
  0x94  : ("AND", "A"        , DIRECT   ,   4,   2, "-aa0-"),
  0xB4  : ("AND", "A"        , EXTENDED ,   5,   3, "-aa0-"),
  0x84  : ("AND", "A"        , IMMEDIATE,   2,   2, "-aa0-"),
  0xA4  : ("AND", "A"        , INDEXED  ,   4,   2, "-aa0-"),
  0xD4  : ("AND", "B"        , DIRECT   ,   4,   2, "-aa0-"),
  0xF4  : ("AND", "B"        , EXTENDED ,   5,   3, "-aa0-"),
  0xC4  : ("AND", "B"        , IMMEDIATE,   2,   2, "-aa0-"),
  0xE4  : ("AND", "B"        , INDEXED  ,   4,   2, "-aa0-"),
  0x1C  : ("AND", "CC"       , IMMEDIATE,   3,   2, "ddddd"),
  0x38  : ("AND", "CC"       , IMMEDIATE,   4,   2, ILLEGAL),
  0x48  : ("ASL", "A"        , INHERENT ,   2,   1, "naaas"),
  0x58  : ("ASL", "B"        , INHERENT ,   2,   1, "naaas"),
  0x08  : ("ASL", ""         , DIRECT   ,   6,   2, "naaas"),
  0x78  : ("ASL", ""         , EXTENDED ,   7,   3, "naaas"),
  0x68  : ("ASL", ""         , INDEXED  ,   6,   2, "naaas"),
  0x07  : ("ASR", ""         , DIRECT   ,   6,   2, "rw"),
  0x77  : ("ASR", ""         , EXTENDED ,   7,   3, "rw"),
  0x67  : ("ASR", ""         , INDEXED  ,   6,   2, "rw"),
  0x47  : ("ASR", "A"        , INHERENT ,   2,   1, "rw"),
  0x57  : ("ASR", "B"        , INHERENT ,   2,   1, "rw"),
  0x27  : ("BEQ", ""         , RELATIVE ,   3,   2, "!Z"),
  0x2C  : ("BGE", ""         , RELATIVE ,   3,   2, "N == V"),
  0x2E  : ("BGT", ""         , RELATIVE ,   3,   2, "(N == V) && !Z"),
  0x22  : ("BHI", ""         , RELATIVE ,   3,   2, "!Z && !C"),
  0x24  : ("BCC", ""         , RELATIVE ,   3,   2, "!C"),
  0x2F  : ("BLE", ""         , RELATIVE ,   3,   2, "(N != V) || Z"),
  0x25  : ("BCS", ""         , RELATIVE ,   3,   2, "C"),
  0x23  : ("BLS", ""         , RELATIVE ,   3,   2, "Z || C"),
  0x2D  : ("BLT", ""         , RELATIVE ,   3,   2, "N != V"),
  0x2B  : ("BMI", ""         , RELATIVE ,   3,   2, "N"),
  0x26  : ("BNE", ""         , RELATIVE ,   3,   2, "Z"),
  0x2A  : ("BPL", ""         , RELATIVE ,   3,   2, "!N"),
  0x20  : ("BRA", ""         , RELATIVE ,   3,   2, ""),
  0x21  : ("BRN", ""         , RELATIVE ,   3,   2, "-----"),
  0x8D  : ("BSR", ""         , RELATIVE ,   7,   2, "-----"),
  0x28  : ("BVC", ""         , RELATIVE ,   3,   2, "!V"),
  0x29  : ("BVS", ""         , RELATIVE ,   3,   2, "V"),
  0x95  : ("BIT", "A"        , DIRECT   ,   4,   2, "-aa0-"),
  0xB5  : ("BIT", "A"        , EXTENDED ,   5,   3, "-aa0-"),
  0x85  : ("BIT", "A"        , IMMEDIATE,   2,   2, "-aa0-"),
  0xA5  : ("BIT", "A"        , INDEXED  ,   4,   2, "-aa0-"),
  0xD5  : ("BIT", "B"        , DIRECT   ,   4,   2, "-aa0-"),
  0xF5  : ("BIT", "B"        , EXTENDED ,   5,   3, "-aa0-"),
  0xC5  : ("BIT", "B"        , IMMEDIATE,   2,   2, "-aa0-"),
  0xE5  : ("BIT", "B"        , INDEXED  ,   4,   2, "-aa0-"),
  0x0F  : ("CLR", ""         , DIRECT   ,   6,   2, "-0100"),
  0x7F  : ("CLR", ""         , EXTENDED ,   7,   3, "-0100"),
  0x6F  : ("CLR", ""         , INDEXED  ,   6,   2, "-0100"),
  0x4E  : ("CLR", "A"        , INHERENT ,   2,   1, ILLEGAL),
  0x4F  : ("CLR", "A"        , INHERENT ,   2,   1, "-0100"),
  0x5E  : ("CLR", "B"        , INHERENT ,   2,   1, ILLEGAL),
  0x5F  : ("CLR", "B"        , INHERENT ,   2,   1, "-0100"),
  0x91  : ("CMP", "A"        , DIRECT   ,   4,   2, "uaaaa"),
  0xB1  : ("CMP", "A"        , EXTENDED ,   5,   3, "uaaaa"),
  0x81  : ("CMP", "A"        , IMMEDIATE,   2,   2, "uaaaa"),
  0xA1  : ("CMP", "A"        , INDEXED  ,   4,   2, "uaaaa"),
  0xD1  : ("CMP", "B"        , DIRECT   ,   4,   2, "uaaaa"),
  0xF1  : ("CMP", "B"        , EXTENDED ,   5,   3, "uaaaa"),
  0xC1  : ("CMP", "B"        , IMMEDIATE,   2,   2, "uaaaa"),
  0xE1  : ("CMP", "B"        , INDEXED  ,   4,   2, "uaaaa"),
  0x9C  : ("CMP", "X"        , DIRECT   ,   6,   2, "-aaaa"),
  0xBC  : ("CMP", "X"        , EXTENDED ,   7,   3, "-aaaa"),
  0x8C  : ("CMP", "X"        , IMMEDIATE,   4,   3, "-aaaa"),
  0xAC  : ("CMP", "X"        , INDEXED  ,   6,   2, "-aaaa"),
  0x1093: ("CMP", "D"        , DIRECT   ,   7,   3, "-aaaa"),
  0x10B3: ("CMP", "D"        , EXTENDED ,   8,   4, "-aaaa"),
  0x1083: ("CMP", "D"        , IMMEDIATE,   5,   4, "-aaaa"),
  0x10A3: ("CMP", "D"        , INDEXED  ,   7,   3, "-aaaa"),
  0x119C: ("CMP", "S"        , DIRECT   ,   7,   3, "-aaaa"),
  0x11BC: ("CMP", "S"        , EXTENDED ,   8,   4, "-aaaa"),
  0x118C: ("CMP", "S"        , IMMEDIATE,   5,   4, "-aaaa"),
  0x11AC: ("CMP", "S"        , INDEXED  ,   7,   3, "-aaaa"),
  0x1193: ("CMP", "U"        , DIRECT   ,   7,   3, "-aaaa"),
  0x11B3: ("CMP", "U"        , EXTENDED ,   8,   4, "-aaaa"),
  0x1183: ("CMP", "U"        , IMMEDIATE,   5,   4, "-aaaa"),
  0x11A3: ("CMP", "U"        , INDEXED  ,   7,   3, "-aaaa"),
  0x109C: ("CMP", "Y"        , DIRECT   ,   7,   3, "-aaaa"),
  0x10BC: ("CMP", "Y"        , EXTENDED ,   8,   4, "-aaaa"),
  0x108C: ("CMP", "Y"        , IMMEDIATE,   5,   4, "-aaaa"),
  0x10AC: ("CMP", "Y"        , INDEXED  ,   7,   3, "-aaaa"),
  0x03  : ("COM", ""         , DIRECT   ,   6,   2, "~"),
  0x73  : ("COM", ""         , EXTENDED ,   7,   3, "~"),
  0x63  : ("COM", ""         , INDEXED  ,   6,   2, "~"),
  0x43  : ("COM", "A"        , INHERENT ,   2,   1, "~"),
  0x53  : ("COM", "B"        , INHERENT ,   2,   1, "~"),
  0x3C  : ("CWA", "I"        , INHERENT ,  21,   2, "ddddd"),
  0x19  : ("DAA", ""         , INHERENT ,   2,   1, "-aa0a"),
  0x0A  : ("DEC", ""         , DIRECT   ,   6,   2, "-aaa-"),
  0x0B  : ("DEC", ""         , DIRECT   ,   6,   2, ILLEGAL),
  0x7A  : ("DEC", ""         , EXTENDED ,   7,   3, "-aaa-"),
  0x7B  : ("DEC", ""         , EXTENDED ,   7,   3, ILLEGAL),
  0x6A  : ("DEC", ""         , INDEXED  ,   6,   2, "-aaa-"),
  0x6B  : ("DEC", ""         , INDEXED  ,   6,   2, ILLEGAL),
  0x4A  : ("DEC", "A"        , INHERENT ,   2,   1, "-aaa-"),
  0x4B  : ("DEC", "A"        , INHERENT ,   2,   1, ILLEGAL),
  0x5A  : ("DEC", "B"        , INHERENT ,   2,   1, "-aaa-"),
  0x5B  : ("DEC", "B"        , INHERENT ,   2,   1, ILLEGAL),
  0x98  : ("EOR", "A"        , DIRECT   ,   4,   2, "-aa0-"),
  0xB8  : ("EOR", "A"        , EXTENDED ,   5,   3, "-aa0-"),
  0x88  : ("EOR", "A"        , IMMEDIATE,   2,   2, "-aa0-"),
  0xA8  : ("EOR", "A"        , INDEXED  ,   4,   2, "-aa0-"),
  0xD8  : ("EOR", "B"        , DIRECT   ,   4,   2, "-aa0-"),
  0xF8  : ("EOR", "B"        , EXTENDED ,   5,   3, "-aa0-"),
  0xC8  : ("EOR", "B"        , IMMEDIATE,   2,   2, "-aa0-"),
  0xE8  : ("EOR", "B"        , INDEXED  ,   4,   2, "-aa0-"),
  0x1E  : ("EXG", ""         , REGISTER ,   8,   2, "ccccc"),
  0x14  : ("HCF", ""         , INHERENT ,   2,   1, ILLEGAL),
  0x15  : ("HCF", ""         , INHERENT ,   2,   1, ILLEGAL),
  0xCD  : ("HCF", ""         , INHERENT ,   2,   1, ILLEGAL),
  0x0C  : ("INC", ""         , DIRECT   ,   6,   2, "-aaa-"),
  0x7C  : ("INC", ""         , EXTENDED ,   7,   3, "-aaa-"),
  0x6C  : ("INC", ""         , INDEXED  ,   6,   2, "-aaa-"),
  0x4C  : ("INC", "A"        , INHERENT ,   2,   1, "-aaa-"),
  0x5C  : ("INC", "B"        , INHERENT ,   2,   1, "-aaa-"),
  0x0E  : ("JMP", ""         , DIRECT   ,   3,   2, "-----"),
  0x7E  : ("JMP", ""         , EXTENDED ,   3,   3, "-----"),
  0x6E  : ("JMP", ""         , INDEXED  ,   3,   2, "-----"),
  0x9D  : ("JSR", ""         , DIRECT   ,   7,   2, "-----"),
  0xBD  : ("JSR", ""         , EXTENDED ,   8,   3, "-----"),
  0xAD  : ("JSR", ""         , INDEXED  ,   7,   2, "-----"),
  0x02  : ("LBRA",""         , RELATIVE ,   5,   3, ILLEGAL),
  0x16  : ("LBRA",""         , RELATIVE ,   5,   3, ""),
  0x17  : ("LBSR",""         , RELATIVE ,   9,   3, "-----"),
  0x1027: ("LBEQ",""         , RELATIVE , 5/6,   4, "Z"),
  0x102C: ("LBGE",""         , RELATIVE , 5/6,   4, "N == V"),
  0x102E: ("LBGT",""         , RELATIVE , 5/6,   4, "(N == V) && !Z"),
  0x1022: ("LBHI",""         , RELATIVE , 5/6,   4, "!Z && !C"),
  0x1024: ("LBCC",""         , RELATIVE , 5/6,   4, "!C"),
  0x102F: ("LBLE",""         , RELATIVE , 5/6,   4, "(N != V) || Z"),
  0x1025: ("LBCS",""         , RELATIVE , 5/6,   4, "C"),
  0x1023: ("LBLS",""         , RELATIVE , 5/6,   4, "Z || C"),
  0x102D: ("LBLT",""         , RELATIVE , 5/6,   4, "N != V"),
  0x102B: ("LBMI",""         , RELATIVE , 5/6,   4, "N"),
  0x1026: ("LBNE",""         , RELATIVE , 5/6,   4, "!Z"),
  0x102A: ("LBPL",""         , RELATIVE , 5/6,   4, "!N"),
  0x1021: ("LBRN",""         , RELATIVE , 5/6,   4, "-----"),
  0x1028: ("LBVC",""         , RELATIVE , 5/6,   4, "!V"),
  0x1029: ("LBVS",""         , RELATIVE , 5/6,   4, "V"),
  0x96  : ("LD",  "A"        , DIRECT   ,   4,   2, "-aa0-"),
  0xB6  : ("LD",  "A"        , EXTENDED ,   5,   3, "-aa0-"),
  0x86  : ("LD",  "A"        , IMMEDIATE,   2,   2, "-aa0-"),
  0xA6  : ("LD",  "A"        , INDEXED  ,   4,   2, "-aa0-"),
  0xD6  : ("LD",  "B"        , DIRECT   ,   4,   2, "-aa0-"),
  0xF6  : ("LD",  "B"        , EXTENDED ,   5,   3, "-aa0-"),
  0xC6  : ("LD",  "B"        , IMMEDIATE,   2,   2, "-aa0-"),
  0xE6  : ("LD",  "B"        , INDEXED  ,   4,   2, "-aa0-"),
  0xDC  : ("LD",  "D"        , DIRECT   ,   5,   2, "-aa0-"),
  0xFC  : ("LD",  "D"        , EXTENDED ,   6,   3, "-aa0-"),
  0xCC  : ("LD",  "D"        , IMMEDIATE,   3,   3, "-aa0-"),
  0xEC  : ("LD",  "D"        , INDEXED  ,   5,   2, "-aa0-"),
  0xDE  : ("LD",  "U"        , DIRECT   ,   5,   2, "-aa0-"),
  0xFE  : ("LD",  "U"        , EXTENDED ,   6,   3, "-aa0-"),
  0xCE  : ("LD",  "U"        , IMMEDIATE,   3,   3, "-aa0-"),
  0xEE  : ("LD",  "U"        , INDEXED  ,   5,   2, "-aa0-"),
  0x9E  : ("LD",  "X"        , DIRECT   ,   5,   2, "-aa0-"),
  0xBE  : ("LD",  "X"        , EXTENDED ,   6,   3, "-aa0-"),
  0x8E  : ("LD",  "X"        , IMMEDIATE,   3,   3, "-aa0-"),
  0xAE  : ("LD",  "X"        , INDEXED  ,   5,   2, "-aa0-"),
  0x108E: ("LD",  "Y"        , IMMEDIATE,   4,   4, "-aa0-"),
  0x10DE: ("LD",  "S"        , DIRECT   ,   6,   3, "-aa0-"),
  0x10FE: ("LD",  "S"        , EXTENDED ,   7,   4, "-aa0-"),
  0x10CE: ("LD",  "S"        , IMMEDIATE,   4,   4, "-aa0-"),
  0x10EE: ("LD",  "S"        , INDEXED  ,   6,   3, "-aa0-"),
  0x109E: ("LD",  "Y"        , DIRECT   ,   6,   3, "-aa0-"),
  0x10BE: ("LD",  "Y"        , EXTENDED ,   7,   4, "-aa0-"),
  0x10AE: ("LD",  "Y"        , INDEXED  ,   6,   3, "-aa0-"),
  0x32  : ("LEA", "S"        , INDEXED  ,   4,   2, "-----"),
  0x33  : ("LEA", "U"        , INDEXED  ,   4,   2, "-----"),
  0x30  : ("LEA", "X"        , INDEXED  ,   4,   2, "--a--"),
  0x31  : ("LEA", "Y"        , INDEXED  ,   4,   2, "--a--"),
  0x04  : ("LSR", ""         , DIRECT   ,   6,   2, "-0a-s"),
  0x05  : ("LSR", ""         , DIRECT   ,   6,   2, ILLEGAL),
  0x74  : ("LSR", ""         , EXTENDED ,   7,   3, "-0a-s"),
  0x75  : ("LSR", ""         , EXTENDED ,   7,   3, ILLEGAL),
  0x64  : ("LSR", ""         , INDEXED  ,   6,   2, "-0a-s"),
  0x65  : ("LSR", ""         , INDEXED  ,   6,   2, ILLEGAL),
  0x44  : ("LSR", "A"        , INHERENT ,   2,   1, "-0a-s"),
  0x45  : ("LSR", "A"        , INHERENT ,   2,   1, ILLEGAL),
  0x54  : ("LSR", "B"        , INHERENT ,   2,   1, "-0a-s"),
  0x55  : ("LSR", "B"        , INHERENT ,   2,   1, ILLEGAL),
  0x3D  : ("MUL", ""         , INHERENT ,  11,   1, "--a-a"),
  0x00  : ("NEG", ""         , DIRECT   ,   6,   2, "-"),
  0x01  : ("NEG", ""         , DIRECT   ,   6,   2, ILLEGAL),
  0x70  : ("NEG", ""         , EXTENDED ,   7,   3, "-"),
  0x71  : ("NEG", ""         , EXTENDED ,   7,   3, ILLEGAL),
  0x72  : ("NECO",""         , EXTENDED ,   7,   3, ILLEGAL),
  0x60  : ("NEG", ""         , INDEXED  ,   6,   2, "-"),
  0x61  : ("NEG", ""         , INDEXED  ,   6,   2, ILLEGAL),
  0x62  : ("NECO",""         , INDEXED  ,   6,   2, ILLEGAL),
  0x40  : ("NEG", "A"        , INHERENT ,   2,   1, "-"),
  0x41  : ("NEG", "A"        , INHERENT ,   2,   1, ILLEGAL),
  0x42  : ("NECO","A"        , INHERENT ,   2,   1, ILLEGAL),
  0x50  : ("NEG", "B"        , INHERENT ,   2,   1, "-"),
  0x51  : ("NEG", "B"        , INHERENT ,   2,   1, ILLEGAL),
  0x52  : ("NECO","B"        , INHERENT ,   2,   1, ILLEGAL),
  0x12  : ("NOP", ""         , INHERENT ,   2,   1, "-----"),
  0x1B  : ("NOP", ""         , INHERENT ,   2,   1, ILLEGAL),
  0x87  : ("DROP",""         , IMMEDIATE,   2,   2, ILLEGAL),
  0xC7  : ("DROP",""         , IMMEDIATE,   2,   2, ILLEGAL),
  0x9A  : ("OR", "A"         , DIRECT   ,   4,   2, "-aa0-"),
  0xBA  : ("OR", "A"         , EXTENDED ,   5,   3, "-aa0-"),
  0x8A  : ("OR", "A"         , IMMEDIATE,   2,   2, "-aa0-"),
  0xAA  : ("OR", "A"         , INDEXED  ,   4,   2, "-aa0-"),
  0xDA  : ("OR", "B"         , DIRECT   ,   4,   2, "-aa0-"),
  0xFA  : ("OR", "B"         , EXTENDED ,   5,   3, "-aa0-"),
  0xCA  : ("OR", "B"         , IMMEDIATE,   2,   2, "-aa0-"),
  0xEA  : ("OR", "B"         , INDEXED  ,   4,   2, "-aa0-"),
  0x1A  : ("OR", "CC"        , IMMEDIATE,   3,   2, "ddddd"),
  0x34  : ("PSH", "S"        , IMMEDIATE,   5,   2, "-----"),
  0x36  : ("PSH", "U"        , IMMEDIATE,   5,   2, "-----"),
  0x35  : ("PUL", "S"        , IMMEDIATE,   5,   2, "ccccc"),
  0x37  : ("PUL", "U"        , IMMEDIATE,   5,   2, "ccccc"),
  0x3E  : ("RESET",""        , INHERENT ,  15,   1, ILLEGAL),
  0x09  : ("ROL", ""         , DIRECT   ,   6,   2, "-aaas"),
  0x79  : ("ROL", ""         , EXTENDED ,   7,   3, "-aaas"),
  0x69  : ("ROL", ""         , INDEXED  ,   6,   2, "-aaas"),
  0x49  : ("ROL", "A"        , INHERENT ,   2,   1, "-aaas"),
  0x59  : ("ROL", "B"        , INHERENT ,   2,   1, "-aaas"),
  0x06  : ("ROR", ""         , DIRECT   ,   6,   2, "rw"),
  0x76  : ("ROR", ""         , EXTENDED ,   7,   3, "rw"),
  0x66  : ("ROR", ""         , INDEXED  ,   6,   2, "rw"),
  0x46  : ("ROR", "A"        , INHERENT ,   2,   1, "rw"),
  0x56  : ("ROR", "B"        , INHERENT ,   2,   1, "rw"),
  0x3B  : ("RTI", ""         , INHERENT ,6/15,   1, "-----"),
  0x39  : ("RTS", ""         , INHERENT ,   5,   1, "-----"),
  0x92  : ("SBC", "A"        , DIRECT   ,   4,   2, "uaaaa"),
  0xB2  : ("SBC", "A"        , EXTENDED ,   5,   3, "uaaaa"),
  0x82  : ("SBC", "A"        , IMMEDIATE,   2,   2, "uaaaa"),
  0xA2  : ("SBC", "A"        , INDEXED  ,   4,   2, "uaaaa"),
  0xD2  : ("SBC", "B"        , DIRECT   ,   4,   2, "uaaaa"),
  0xF2  : ("SBC", "B"        , EXTENDED ,   5,   3, "uaaaa"),
  0xC2  : ("SBC", "B"        , IMMEDIATE,   2,   2, "uaaaa"),
  0xE2  : ("SBC", "B"        , INDEXED  ,   4,   2, "uaaaa"),
  0x1D  : ("SEX", ""         , INHERENT ,   2,   1, "-aa0-"),
  0x97  : ("ST",  "A"        , DIRECT   ,   4,   2, "-aa0-"),
  0xB7  : ("ST",  "A"        , EXTENDED ,   5,   3, "-aa0-"),
  0xA7  : ("ST",  "A"        , INDEXED  ,   4,   2, "-aa0-"),
  0xD7  : ("ST",  "B"        , DIRECT   ,   4,   2, "-aa0-"),
  0xF7  : ("ST",  "B"        , EXTENDED ,   5,   3, "-aa0-"),
  0xE7  : ("ST",  "B"        , INDEXED  ,   4,   2, "-aa0-"),
  0xDD  : ("ST",  "D"        , DIRECT   ,   5,   2, "-aa0-"),
  0xFD  : ("ST",  "D"        , EXTENDED ,   6,   3, "-aa0-"),
  0xED  : ("ST",  "D"        , INDEXED  ,   5,   2, "-aa0-"),
  0xDF  : ("ST",  "U"        , DIRECT   ,   5,   2, "-aa0-"),
  0xFF  : ("ST",  "U"        , EXTENDED ,   6,   3, "-aa0-"),
  0xEF  : ("ST",  "U"        , INDEXED  ,   5,   2, "-aa0-"),
  0x9F  : ("ST",  "X"        , DIRECT   ,   5,   2, "-aa0-"),
  0xBF  : ("ST",  "X"        , EXTENDED ,   6,   3, "-aa0-"),
  0xAF  : ("ST",  "X"        , INDEXED  ,   5,   2, "-aa0-"),
  0x10EF: ("ST",  "S"        , INDEXED  ,   6,   3, "-aa0-"),
  0x10DF: ("ST",  "S"        , DIRECT   ,   6,   3, "-aa0-"),
  0x10FF: ("ST",  "S"        , EXTENDED ,   7,   4, "-aa0-"),
  0x109F: ("ST",  "Y"        , DIRECT   ,   6,   3, "-aa0-"),
  0x10BF: ("ST",  "Y"        , EXTENDED ,   7,   4, "-aa0-"),
  0x10AF: ("ST",  "Y"        , INDEXED  ,   6,   3, "-aa0-"),
  0x90  : ("SUB", "A"        , DIRECT   ,   4,   2, "uaaaa"),
  0xB0  : ("SUB", "A"        , EXTENDED ,   5,   3, "uaaaa"),
  0x80  : ("SUB", "A"        , IMMEDIATE,   2,   2, "uaaaa"),
  0xA0  : ("SUB", "A"        , INDEXED  ,   4,   2, "uaaaa"),
  0xD0  : ("SUB", "B"        , DIRECT   ,   4,   2, "uaaaa"),
  0xF0  : ("SUB", "B"        , EXTENDED ,   5,   3, "uaaaa"),
  0xC0  : ("SUB", "B"        , IMMEDIATE,   2,   2, "uaaaa"),
  0xE0  : ("SUB", "B"        , INDEXED  ,   4,   2, "uaaaa"),
  0x93  : ("SUB", "D"        , DIRECT   ,   6,   2, "-aaaa"),
  0xB3  : ("SUB", "D"        , EXTENDED ,   7,   3, "-aaaa"),
  0x83  : ("SUB", "D"        , IMMEDIATE,   4,   3, "-aaaa"),
  0xA3  : ("SUB", "D"        , INDEXED  ,   6,   2, "-aaaa"),
  0x3F  : ("SWI", ""         , INHERENT ,  19,   1, "-----"),
  0x103F: ("SWI2",""         , INHERENT ,  20,   2, "-----"),
  0x113F: ("SWI3",""         , INHERENT ,  20,   2, "-----"),
  0x13  : ("SYNC",""         , INHERENT ,   2,   1, "-----"),
  0x1F  : ("TFR", ""         , REGISTER ,   7,   2, "ccccc"),
  0x0D  : ("TST", ""         , DIRECT   ,   6,   2, "r"),
  0x7D  : ("TST", ""         , EXTENDED ,   7,   3, "r"),
  0x6D  : ("TST", ""         , INDEXED  ,   6,   2, "r"),
  0x4D  : ("TST", "A"        , INHERENT ,   2,   1, "r"),
  0x5D  : ("TST", "B"        , INHERENT ,   2,   1, "r"),
  0x1010: ("!WTF",""         , INHERENT ,   2,   2, ILLEGAL),
  0x1011: ("!WTF",""         , INHERENT ,   2,   2, ILLEGAL),
  0x1110: ("!WTF",""         , INHERENT ,   2,   2, ILLEGAL),
  0x1111: ("!WTF",""         , INHERENT ,   2,   2, ILLEGAL),
  0x18  : ("!SCC",""         , INHERENT ,   3,   1, ILLEGAL),
  }


OPERATORS = {
  'ADC': '+',
  'ADD': '+',
  'AND': '&',
  'BIT': '&',
  'CMP': '-',
  'COM': '~value',
  'DEC': '- 1',
  'EOR': '^',
  'INC': '+ 1',
  'NEG': '-value',
  'NECO':'(C ? ~value : -value)',
  'OR':  '|',
  'SBC': '-',
  'SUB': '-',

  'BEQ':  '!Z',
  'LBEQ': '!Z',
  'BGE':  'N == V',
  'LBGE': 'N == V',
  'BGT':  '(N == V) && !Z',
  'LBGT': '(N == V) && !Z',
  'BHI':  '!Z && !C',
  'LBHI': '!Z && !C',
  'BCC':  '!C',
  'LBCC': '!C',
  'BLE':  '(N != V) || Z',
  'LBLE': '(N != V) || Z',
  'BCS':  'C',
  'LBCS': 'C',
  'BLS':  'Z || C',
  'LBLS': 'Z || C',
  'BLT':  'N != V',
  'LBLT': 'N != V',
  'BMI':  'N',
  'LBMI': 'N',
  'BNE':  'Z',
  'LBNE': 'Z',
  'BPL':  '!N',
  'LBPL': '!N',
  'BVC':  '!V',
  'LBVC': '!V',
  'BVS':  'V',
  'LBVS': 'V',
  'BRA':  None,
  'LBRA': None,
  }



TEST_PROGRAM = [0xC9, 9,
                0xC9, 8,
                0x3A,
                0xCB, 200,
                0xC9, 60,
                0xC3, 1, 1,
                0xC9, -8,
                0xC4, 24,
                0x1C, 5,
                0x24, 2,
                0x48,
                0x57,
                0x3A,
                ]

# Globals

OUTPUT = None
INDENTATION = 0;
O_PC = None # location of current instruction
PC = None # program counter of currently parsing instruction
ROM = {}
TODO = set()
DONE = {}
A_CODE = {}
C_CODE = {}
C_INDENTATION = 0
VERBOSE = False


def gen(*s):
  ss = ' '.join(map(str, s))
  global C_INDENTATION, O_PC
  if '}' in ss:
    C_INDENTATION -= 1
  line = ' ' * C_INDENTATION * 2 + ss
  if s and (ss[0] not in string.punctuation) and (ss[-1] not in '{}'):
    line = line + ';'
  if VERBOSE:
    print >>sys.stderr, line
  if O_PC != None:
    C_CODE[O_PC] = C_CODE.setdefault(O_PC, '') + line + '\n'
  if '{' in ss:
    C_INDENTATION += 1

def dasm(mnemonic, mode, bytes):
  addrpc = PC
  addr = addressing(mode, bytes)
  bytes = PC - addrpc
  mem = [memByte(b) for b in range(O_PC, PC)]
  line = ("%04X: %-24s%-7s%s" %
          (O_PC, ' '.join(['%02X' % b for b in mem]), mnemonic, addr))
  if False:
    # Show program code as strings
    str = [c if c in string.printable else '.' for c in map(chr, mem)]
    line = '%-54s ; "%s"' % (line, ''.join(str))
  elif False:
    # Show addressing mode
    line += '  ;' + mode
  A_CODE[O_PC] = line
  return line

def error(msg):
  print >>sys.stderr, 'ERROR:', msg
  sys.exit(-1)

def tst(flags='', result='result', N=None, Z=None, V=None, C=None, ):
  if N != None: flags += 'N'
  if Z != None: flags += 'Z'
  if V != None: flags += 'V'
  if C != None: flags += 'C'
  tests = { 'N': N or ('1 & (%s >> (sizeof(%s)*8-1))' % (result,result)),
            'Z': Z or ('(%s == 0)' % result),
            'V': V or '0',
            'C': C or '1'}
  gen('; '.join([f + ' = ' + tests[f] for f in flags]))

def pull(stack, reg):
  gen('%s = input%d(%s)' % (reg, BITS[reg], stack))
  gen('%s += %d' % (stack, BITS[reg]/8))

def push(stack, *regs):
  for reg in regs:
    gen('%s -= %d' % (stack, BITS[reg]/8))
    gen('output%d(%s, %s)' % (BITS[reg], stack, reg))

def result(value, bits=8):
  gen('const int%d result = %s' % (bits, value))

def output(dest, value='result'):
  if dest in BITS:
    gen('%s = %s' % (dest, value))
  else:
    gen('output(%s, %s)' % (dest, value))

def setjmp(addr):
  gen('jmp_buf env')
  gen('if (setjmp(env) == 0) {')
  gen('callStack.push(&env)')
  goto(addr)
  gen('}')

def longjmp():
  gen('jmp_buf* env = callStack.top()')
  gen('callStack.pop()')
  gen('longjmp(*env, 1)')

_t = 0
def temporary():
  global _t
  _t += 1
  return 't%d' % _t

def tfrRegisters(postbyte):
  TFR_REGS = ['D', 'X', 'Y',      'U',  'S', 'PC',  None, None,
              'A', 'B', 'CC', 'DP', None, None, None, None];
  return TFR_REGS[postbyte & 0xF], TFR_REGS[postbyte >> 4]

def load(offset, filename):
  ROM[offset] = map(ord, open(filename, 'rb').read())

def inRom(address):
  for offset, rom in ROM.items():
    delta = address - offset
    if 0 <= delta and delta < len(rom):
      return True
  return False

def memByte(address):
  for offset, rom in ROM.items():
    delta = address - offset
    if 0 <= delta and delta < len(rom):
      return rom[delta]
  error('Bad address %X' % address)

def memWord(address):
  return memByte(address) * 256 + memByte(address + 1)

def memSbyte(address):
  u = memByte(address)
  if u & 0x80:
    u -= 0x100
  return u

def memShort(address):
  u = memWord(address)
  if u & 0x8000:
    u -= 0x10000
  return u

def nextByte():
  global PC
  result = memByte(PC)
  PC += 1
  return result

def nextWord():
  global PC
  result = memWord(PC)
  PC += 2
  return result

def nextSbyte():
  u = memSbyte(PC)
  nextByte()
  return u

def nextShort():
  u = memShort(PC)
  nextWord()
  return u

def goto(address, condition=None):
  if type(address) == type(''):
    gen('PC = %s' % address)
    gen('continue')
    return
  gen(('if (%s) ' % condition if condition else '') + 'goto L_%04X' % address)
  if address not in DONE:
    TODO.add(address)

def step(pc):
  if pc in DONE:
    if DONE[pc] == 0:
      error('Overlapped code')
    return

  if not inRom(pc):
    gen('error("Bad code location: %X")' % pc)
    DONE[pc] = -1
    return

  global O_PC, PC
  PC = pc
  O_PC = pc

  gen('case 0x%04X: L_%04X: PC = 0x%X' % (PC, PC, PC))

  opcode = nextByte()
  if opcode in (0x10, 0x11):
    opcode = opcode * 256 + nextByte()
  record = OPCODES.get(opcode)
  fallback = False
  if not record and opcode > 0xFF:
    fallback = True
    record = OPCODES.get(opcode & 0xFF)
  if not record:
    error('Unrecognised opcode: $%02X' % opcode)
  mnemonic, register, mode, timing, bytes, flags = record
  bits = BITS.get(register)
  op = OPERATORS.get(mnemonic)
  if fallback:
    timing += 1

  bytes -= 1 if fallback or opcode < 0x100 else 2

  illegal = fallback or flags == ILLEGAL
  if False and illegal:
    error('Illegal opcode')

  gen('{')

  postOpcodePC = PC
  gen('assembly = "%s"' % dasm(mnemonic + register, mode, bytes))
  actualbytes = PC - postOpcodePC
  if actualbytes < bytes or (mode != INDEXED and bytes < actualbytes):
    error('INST SIZE MISMATCH: expected:%d saw:%d' % (bytes, actualbytes))
  DONE[O_PC] = PC - O_PC
  for a in range(O_PC + 1, PC):
    DONE[a] = 0  # address used, but not start of an instruction
  PC = postOpcodePC

  fallthru = True

  if type(timing) == type((1,2)):
    timing = timing[0]
  gen('elapse(%d)' % timing)

  #
  # Do the op
  #

  if mnemonic == 'ABX':
    gen('X = X + uint8(B)')

  elif mnemonic in ('ADC', 'SBC'):
    autofetch('l', register, mode, bytes)
    gen('int sum = %s %svalue %s C' % (register,op,op))
    result('sum')
    tst('NZ', C='1 & (sum >> 8)', V='C ^ N')
    gen('H = ((int(%s & 0xF) + int(%svalue & 0xF) + int(%sC)) >> 4) & 1' %
        (register,op,op))
    output(register or 'addr')

  elif mnemonic in ('ADD', 'CMP', 'SUB'):
    autofetch('l', register, mode, bytes)
    gen('int sum = int(%s) %s int(value)' % (register, op))
    result('sum')
    tst('NZ')
    if mnemonic == 'ADD' and bits == 8:
      gen('H = ((int(%s & 0xF) + int(%svalue & 0xF)) >> 4) & 1' %
          (register, op))
    gen('C = 1 & (sum >> 8)')
    gen('V = C ^ N')
    if mnemonic != 'CMP':
      output(register or 'addr')

  elif mnemonic in ('AND', 'BIT', 'OR', 'EOR'):
    autofetch('l', register, mode, bytes)
    result('%s %s value' % (register, op), bits)
    tst('NZV')
    if mnemonic != 'BIT':
      output(register or 'addr')

  elif mnemonic == 'ASL':
    autofetch('m', register, mode, bytes)
    result('value << 1')
    tst('NZ', V='1 & ((value >> 6) ^ (value >> 7))', C='value >> 7')
    output(register or 'addr')

  elif mnemonic == 'ASR':
    autofetch('m', register, mode, bytes)
    result('value >> 1')
    tst('ZN', C='1 & value')
    output(register or 'addr')

  elif mnemonic == 'ROL':
    autofetch('m', register, mode, bytes)
    result('value << 1 | C')
    tst('NZ', V='1 & ((value >> 6) ^ (value >> 7))', C='value >> 7')
    output(register or 'addr')

  elif mnemonic == 'ROR':
    autofetch('m', register, mode, bytes)
    result('C << 7 | value >> 1')
    gen('N = C; C = 1 & value')
    tst('Z')
    output(register or 'addr')

  elif mnemonic == 'LSR':
    autofetch('m', register, mode, bytes)
    result('unsigned(value) >> 1')
    tst('Z', N='0', C='1 & value')
    output(register or 'addr')

  elif mnemonic == 'CLR':
    autofetch('a', register, mode, bytes)
    result('0')
    gen('N = V = C = 0; Z = 1')
    output(register or 'addr')

  elif mnemonic in ('COM', 'NEG', 'NECO'):
    autofetch('m', register, mode, bytes)
    result(op)
    tst('NZVC')
    output(register or 'addr')

  elif mnemonic in ('INC', 'DEC'):
    autofetch('m', register, mode, bytes)
    result('value %s' % op)
    tst('NZ', V='(value == 0x7F)' if mnemonic == 'INC' else '(value == -0x80)')
    output(register or 'addr')

  elif mnemonic == 'DROP':
    nextByte();
    gen('N = 1, Z = V = 0')

  elif mnemonic == 'JMP':
    goto(address(mode))
    fallthru = False

  elif mnemonic in ('BSR', 'JSR'):
    addr = (address(mode) if mnemonic == 'JSR' else PC + nextByte())
    setjmp(addr)

  elif mnemonic == 'DAA':
    gen('int ahi = (A >> 4) & 0xF')
    gen('int alo = A & 0xF')
    gen('if (C || ahi > 9 || (ahi > 8 && alo > 9)) A = (ahi + 6) << 4 + alo')
    gen('if (H || alo > 9) A += 6')

  elif mnemonic in ('LD', 'ST'):
    auto = mnemonic[:1].lower()
    autofetch(auto, register, mode, bytes)
    result('value', bits)
    tst('NZV')
    output(register if mnemonic == 'LD' else 'addr')

  elif mnemonic == 'LEA':
    autofetch('a', register, mode, bytes)
    gen('%s = addr' % register)
    if register in 'XY':
      tst('Z', register)
    #
    #cpu[register] = cpu.addr(mode, bytes)
    #cpu.Z = (cpu[register] == 0)

  elif mnemonic == 'MUL':
    gen('D = A * B')
    gen('Z = !D; C = (B >> 7) & 1')
    #
    #cpu.D(cpu.A * cpu.B)
    #cpu.Z(!cpu.D)
    #cpu.C(cpu.B >> 7)

  elif mnemonic in ('NOP', 'BRN', 'LBRN', '!WTF'):
    pass

  elif mnemonic == 'HCF':
    gen('exit(1)')
    fallthru = False
    #
    #sys.exit(1)

  elif mnemonic in ('PUL', 'PSH'):
    postbyte = nextByte()
    PUL_REGS = ['PC', 'U/S', 'Y', 'X', 'DP', 'B', 'A', 'CC']
    PUL_REGS[1] = 'U' if register == 'S' else 'S'
    r = range(0, 8)
    if mnemonic == 'PSH':
      r = r[::-1]
    for i in r:
      if (1 << i) & postbyte:
        r = PUL_REGS[7-i]
        if mnemonic == 'PUL':
          pull(register, r)
          if r == 'PC':
            fallthru = False
        else:
          push(register, r)

  elif mnemonic == 'RTI':
    pull('S', 'CC')
    gen('if (E) {')
    pull('S', 'A')
    pull('S', 'B')
    pull('S', 'DP')
    pull('S', 'X')
    pull('S', 'Y')
    pull('S', 'U')
    gen('}')
    #pull('S', 'PC')
    longjmp()

  elif mnemonic == 'RTS':
    longjmp()
    fallthru = False

  elif mnemonic == '!SCC':
    gen('CC = 0; Z = V, H = I')

  elif mnemonic == 'SEX':
    tst('N', 'B')
    tst('Z', 'Z')
    gen('if (B & 0x80) {')
    gen('N = 1, A = 0xFF')
    gen('} else {')
    gen('N = 0, A = 0')
    gen('}')

  elif mnemonic[:3] in ('SWI', 'RES'):
    gen('E = 1')
    push('S', 'U', 'Y', 'X', 'DP', 'B', 'A', 'CC')
    if mnemonic == 'SWI':
      gen('I = 1, F = 1')
    vector = {'SWI': 0xFFFA,
              'SWI2': 0xFFF4,
              'SWI3': 0xFFF2,
              'RESET': 0xFFFE}[mnemonic]
    setjmp(memWord(vector))

  elif mnemonic == 'SYNC':
    gen('sync()')

  elif mnemonic == 'TFR':
    d,s = tfrRegisters(nextByte())
    if d:
      gen('%s = %s' % (d, s or '0xFFFF'))

  elif mnemonic == 'EXG':
    r0,r1 = tfrRegisters(nextByte())
    if r0 == None:
      gen('%s = 0xFFFF' % r1)
    elif r1 == None:
      gen('%s = 0xFFFF' % r0)
    else:
      gen('int tmp = %s' % r0)
      gen('%s = %s' % (r0, r1))
      gen('%s = tmp' % r1)
      if BITS[r0] != BITS[r1]:
        error('EXG is all wrong when registers have different sizes')

  elif mnemonic == 'TST':
    autofetch('m', register, mode, bytes)
    tst('NZV', 'value')

  elif mnemonic[:1] == 'B' or mnemonic[:2] == 'LB':
    # Branch instructions
    offset = nextSbyte() if bytes == 1 else nextShort()
    goto(PC + offset, op)
    fallthru = (op != None)

  else:
    error('Unimplemented opcode 0x%X / %s' % (opcode, mnemonic))

  gen('}')
  gen()

  if fallthru and PC not in DONE:
    TODO.add(PC)


# Auto-fetch and auto-output flags
# m: modify (read or write) the register or memory location
# s: read register, write to memory location
# l: read memory location, write to register
# a: just calculate the memory address
def autofetch(auto, register, mode, bytes):
  if auto == 's' or (auto == 'm' and mode == INHERENT):
    gen('const int%d value = %s' % (BITS[register], register))

  if mode == IMMEDIATE:
    value = 0
    for i in range(bytes):
      value = value * 256 + nextByte()
    gen('const int%d value = %d' % (bytes * 8, value))

  addr = address(mode, bytes)
  if addr != None:
    if type(addr) == type(1):
      addr = '0x%04X' % addr
    gen('const uint16 addr = %s' % addr)
    if auto in 'ml':
      gen('const int8 value = input8(addr)')


def address(mode, bytes=1):
  if mode == EXTENDED:
    return nextWord()

  elif mode == DIRECT:
    return 'DP * 256 + %d' % nextByte()

  elif mode == INDEXED:
    postbyte = nextByte()
    register = 'XYUS'[(postbyte>>5) & 0x3]
    indirect = (postbyte & 0x10 != 0)
    mode = postbyte & 0xF

    if postbyte == 0x9F:
      # Extended indirect
      return 'input16(0x%04X)' % nextWord()
    elif (postbyte & 0x80) == 0:
      # 5-bit offset from register contained in postbyte
      return "%s %s %d" % (register, '-' if indirect else '+', mode)

    offset = None
    if mode == 0x4:
      # Zero offset from register
      offset = register
      register = None
    elif mode == 0x8:
      # 8-bit offset from register
      offset = nextSbyte()
    elif mode == 0x9:
      offset = nextShort()
    elif mode == 0x6:
      offset = 'A'
    elif mode == 0x5:
      offset = 'B'
    elif mode == 0xA:
      offset = 'D'
    elif mode == 0:
      # Postincrement
      offset = -1  # compensation
      gen('%s += 1' % register)
    elif mode == 1:
      # Postincrement
      offset = -2  # compensation
      gen('%s += 2' % register)
    elif mode == 2:
      # Predecrement
      gen('%s -= 1' % register)
      offset = 0
    elif mode == 3:
      # Predecrement
      gen('%s -= 2' % register)
      offset = 0
    elif mode == 0xC:
      register = None
      offset = nextSbyte()
    elif mode == 0xD:
      register = None
      offset = nextShort()
    else:
      gen('error("Invalid indexing mode %02X in postbyte %02X")' %
          (mode, postbyte))
      offset = 0

    if register != None:
      offset = "%s + %s" % (register, offset)

    if indirect:
      offset = 'input%d(%s)' % (bytes * 8, offset)

    return offset


def addressing(mode, bytes=1):
  if mode == INHERENT:
    return ''

  elif mode == IMMEDIATE:
    return '#$' + ''.join(['%02X' % nextByte() for i in range(bytes)])

  elif mode == EXTENDED:
    return '$%04X' % nextWord()

  elif mode == DIRECT:
    return '<$%02X' % nextByte()

  elif mode == REGISTER:
    return ','.join([r or '$FF' for r in tfrRegisters(nextByte())])

  elif mode == RELATIVE:
    offset = nextSbyte() if bytes == 1 else nextShort()
    return '$%04X' % (0xFFFF & (O_PC + offset))

  elif mode == INDEXED:
    postbyte = nextByte()
    register = 'XYUS'[(postbyte>>5) & 0x3]
    indirect = (postbyte & 0x10 != 0)
    mode = postbyte & 0xF
    offset = None
    if postbyte == 0x9F:
      # Extended indirect
      return '[$%04X]' % nextWord()
    elif (postbyte & 0x80) == 0:
      # 5-bit offset from register contained in postbyte
      return '%d,%s' % (-mode if indirect else mode, register)
    if mode == 0x4:
      offset = 0
    elif mode == 0x8:
      # 8-bit offset from register
      offset = nextSbyte()
    elif mode == 0x9:
      offset = nextShort()
    elif mode == 0x6:
      offset = 'A'
    elif mode == 0x5:
      offset = 'B'
    elif mode == 0xA:
      offset = 'D'
    elif mode == 0:
      register += '+'
      offset = 0
    elif mode == 1:
      register += '++'
      offset = 0
    elif mode == 2:
      register = '-' + register
      offset = 0
    elif mode == 3:
      register = '--' + register
      offset = 0
    elif mode == 0xC:
      register = 'PC'
      offset = nextSbyte()
    elif mode == 0xD:
      register = 'PC'
      offset = nextShort()
    else:
      gen('error("Invalid indexing mode %02X in postbyte %02X")' %
          (mode, postbyte))
      offset = 0

    result = ','.join((str(offset or ''), register))
    if indirect:
      result = '[%s]' % result
    return result


def main():
  import getopt
  global VERBOSE
  a_output = None
  c_output = None
  entrypoints = []
  roms = {}
  opts,args = getopt.getopt(sys.argv[1:], 'vo:a:e:')
  for o,a in opts:
    if o == '-v':
      VERBOSE += 1
    elif o == '-o':
      c_output = open(a, 'w')
    elif o == '-a':
      a_output = open(a, 'w')
    elif o == '-e':
      entrypoints.append(int(a, 16))
  for arg in args:
    addr,rom = arg.split('=')
    roms[int(addr, 16)] = rom

  global O_PC
  O_PC = -1
  gen('#include "../sim6809.h"')
  gen()
  gen('void MC6809::go() {')
  for addr,rom in roms.items():
    load(addr, rom)
    gen('loadRom(0x%04X, "%s")' % (addr, rom))
  gen()
  gen('PC = 0x%04X' % memWord(0xFFFE))
  gen('for (;;) {')
  gen('switch (PC) {')

  for i in range(0xFFF2, 0x10000, 2):
    entrypoints.append(memWord(i))
  for a in entrypoints:
    TODO.add(a)
  while TODO:
    step(TODO.pop())

  O_PC = 0x10000
  gen('default:')
  gen('fprintf(stderr, "At PC = %04X:\\n", PC)')
  gen('error("No code generated for address")')
  gen()
  if TODO:
    gen('// TODO: ' + ' '.join(['0x%04X' % a for a in TODO]));
  gen('trace()')
  gen('} // switch')
  gen('} // for')
  gen('} // go')

  if a_output:
    ks = A_CODE.keys()
    ks.sort()
    for k in ks:
      a_output.write(A_CODE[k] + '\n')

  if c_output:
    ks = C_CODE.keys()
    ks.sort()
    for k in ks:
      c_output.write(C_CODE[k])

if __name__ == '__main__':
  main()
