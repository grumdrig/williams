"""Usage: python translate6802.py ACTION
ACTION:
  -e  Produce javascript emulator step function
  -d  Procuce disassembly table
"""
import sys
from collections import namedtuple

if sys.argv[1] not in ['-e', '-d', '-i']:
  print >>sys.stderr, __doc__
  sys.exit(2)

Op = namedtuple('Op', 'mnemonic register mode opcode ds hinzvc clocks notes')
OPS = {}

inst = ''
indent = 0
def prepgen():
  global inst
  if inst and '-e' in sys.argv:
    if inst.count('next') > 1:
      error('Double-use of next in ' + inst)
    print inst
  inst = ''
def gen(s, dindent=0):
  global inst, indent
  indent -= 2 * s.count('}')
  if dindent <= 0 and not s.count('}') and not s.count('{'):
    s += ';'
  inst += (' ' * indent) + s + '\n'
  indent += 2 * s.count('{')
  indent += dindent

def error(m):
  print >>sys.stderr, "ERROR", m
  sys.exit(3)

OFFSETS = {
  'I': 0,
  'E': 0,
  'X': -0x10,
  'D': -0x20,
  'M': -0x30,
  'R': 0
  }
BYTES = {
  'I': 1,
  'E': 3,
  'X': 2,
  'D': 2,
  'M': None,
  'R': 2,
  }
MODES = {
  'I': 'INHERENT',
  'E': 'EXTENDED',
  'X': 'INDEXED',
  'D': 'DIRECT',
  'M': 'IMMEDIATE',
  'R': 'RELATIVE'
  }

OVERRIDES = {
  'ASR': """var a = ADDR; var v = fetch(a); C = 1 & v;
            store(a, NZ(s8(v >> 1))); V = N ^ C;""",
  'ASRr':"C = 1 & REGISTER; REGISTER = NZ(s8(v >> 1)); V = N ^ C;",
  'BSR': "var o = FETCH; pushw(PC); PC += o;",
  'DAA': """var ahi = (A >> 4) & 0xF, alo = A & 0xF;
            if (C || ahi > 9 || (ahi > 8 && alo > 9)) A = (ahi + 6) << 4 + alo;
            if (H || alo > 9) A += 6;""",
  'JSR': "var o = ADDR; pushw(PC); PC = o;",
  'JMP': "PC = ADDR;",
  'LSR': """var a = ADDR; var v = fetch(a); C = 1 & v;
            store(a, NZ(0x7F & (v >> 1))); V = N ^ C;""",
  'LSRr':"C = 1 & REGISTER; REGISTER = NZ(0x7F & (v >> 1)); V = N ^ C;",
  'PSH': "push(REGISTER)",
  'PUL': "REGISTER = pull()",
  'ROL': "var a = ADDR; store(a, NZVC(fetch(a) << 1)); V = N ^ C;",
  'ROLr':"REGISTER = NZVC(REGISTER << 1); V = N ^ C;",
  'ROR': """var a = ADDR, v = fetch(a);
            store(a, NZ(C << 7 | v >> 1)); C = 1 & v; V = N ^ C;""",
  'RORr':"""var c = REGISTER & 1; REGISTER = NZ(C << 7 | REGISTER >> 1);
            C = c; V = N ^ C;""",
  'RTI': "CC(pull()); B = pull(); A = pull(); X = pullw(); PC = pullw();",
  'RTS': "PC = pullw();",
  'SWI': "pushw(PC); pushw(X); push(A); push(B); push(CC()); PC = fetchw(0xFFFA);",
  'TAP': "CC(A);",
  'TPA': "A = CC();",
  'WAI': "pushw(PC); pushw(X); push(A); push(B); push(CC()); wait(); return false;",
  }
  
ADDR = {
  'DIRECT': 'nextByte()',
  'EXTENDED': 'nextWord()',
  'INDEXED': 'X + nextByte()',
  }

VALUE = {
  'DIRECT': 'fetch(nextByte())',
  'EXTENDED': 'fetch(nextWord())',
  'IMMEDIATEw': 'nextWord()',
  'IMMEDIATE': 'nextByte()',
  'INDEXED': 'fetch(X + nextByte())',
  'RELATIVE': 'nextSbyte()'
}

DASM = {
  'DIRECT': '[$nn]',
  'EXTENDED': '[$nnnn]',
  'IMMEDIATEw': '#$nnnn',
  'IMMEDIATE': '#$nn',
  'INDEXED': '[X + $nn]',
  'RELATIVE': 'PC + $nn',
  'INHERENT': ''
  }


gen('', 2)
gen('function step() {')
gen('var opcode = nextByte()')
gen('switch (opcode) {')

DISASSEMBLER = {}

for l in open('reference/6802', 'r').readlines()[83:195]:
  if l[2] < 'A' or 'Z' < l[2]:
    continue
  mnem = l[1:4]
  reg = l[4].strip()
  ds = l[6].strip()
  opcode = int(l[8:10], 16)
  hinzvc = l[11:17]
  iexdmr = l[18:24]
  clocks = l[25]
  desc = l[27:50].strip()
  notes = l[51:63].strip()

  for m,v in zip('IEXDMR',iexdmr):
    if v == ' ':
      continue
    bytes = BYTES[m] or { 'X':2, '*': 3 }[v]
    oc = opcode + OFFSETS[m]
    mode = MODES[m]
    if '-i' in sys.argv:
      print "0x%02X: ['%s', '%s', '%s', %d]," % (oc, mnem, reg, mode, int(clocks, 16))
    if mode == 'IMMEDIATE' and v == '*':
      mode = 'IMMEDIATEw'
    OPS[oc] = (mnem, reg, mode, '%02X' % oc, ds, hinzvc, clocks, notes)
    dasm = DASM[mode]
    notes = notes.replace('v', '|').replace('x', '^')
    if notes[1:2] == '=':
      dest = notes[0]
      result = notes[2:]
    else:
      dest = None
      result = notes
    width = 'w' if dest == 'X' or dest == 'S' or result == 'X' or result == 'S' else ''
    if mnem == 'CPX': width = 'w'
    prepgen()
    #gen('if (opcode === 0x%02X) {  // %s%s %s   %s' % (oc, mnem, reg, dasm, mode))
    gen('case 0x%02X:' % oc, 2);
    gen('trace("%s%s", "%s")' % (mnem, reg, dasm))
    gen('CLOCK += %d' % int(clocks, 16))

    DISASSEMBLER[oc] = ('%s%s %s' % (mnem, reg, dasm)).strip()

    if mnem in OVERRIDES:
      ovr = OVERRIDES[mnem]
      if reg: ovr = OVERRIDES.get(mnem + 'r', ovr)
      for line in ovr. \
              replace('REGISTER', reg or '**ERR**REG'+mnem). \
              replace('ADDR', ADDR.get(mode, '**ERR**ADDR'+mode)). \
              replace('FETCH', VALUE.get(mode, '**ERR**FETCH'+mode)).split(';'):
        line = line.strip()
        if line:
          gen(line)
      #gen('}')
      gen('break', -2)
      OPS[oc] = inst
      continue
        
    result = ' '.join(result).replace('P C', 'PC')

    if '+' in result:
      result = ', '.join(['unSign%s(%s)' % (width, term.strip())
                          for term in result.split('+')])
    elif '-' in result[1:]:
      terms = result.split('-')
      t0 = 'unSign%s(%s)' % (width, terms[0])
      result = ', '.join([t0] + ['unSign%s(-%s)' % (width, term.strip())
                                 for term in terms[1:]])


    if ds == 's':
      value = VALUE[mode]
      result = result.replace('s', value)

    result = result.replace('fetch', 'fetch' + width)

    checker = ''
    for flag,v in zip('HINZVC', hinzvc):
      if v == '-':
        continue
      elif flag == 'H':
        h = result.split('+')
        h = ['0xF & %s' % t.strip() for t in h]
        h = ' + '.join(h)
        h = '1 & ((%s) >> 4)' % h
        gen('H = %s' % h.replace('next', 'mem'))
      elif v in '01':
        gen('%s = %s' % (flag, v))
      elif v == '*' or (v == '?' and flag == 'V'):
        checker += flag
      elif v == '?':
        pass
      else:
        error("I'm lost %s %s" % (flag, v))

    if checker:
      result = "%s%s(%s)" % (checker, width, result)
        
    if ds == 'd':
      addr = ADDR[mode];
      if notes.count('d') > 1:
        gen('var addr = %s' % addr)
        addr = 'addr'
        result = result.replace('d', 'fetch(addr)')
      gen('store%s(%s, %s)' % (width, addr, result))
    elif ds == 'a':
      gen('var offset = nextSbyte()')
      jump = 'PC += offset'
      if notes[:3] == 'If ':
        gen('if (%s)' % ' '.join(notes[3:]).replace('=', '=='), 2)
        gen(jump, -2)
      else:
        gen(jump)
    elif dest:
      gen('%s = %s' % (dest, result))
    else:
      gen(result)
    gen('break', -2)
    OPS[oc] = inst
    prepgen()

if '-d' in sys.argv:
  ks = DISASSEMBLER.keys()
  ks.sort()
  print 'OPS = {'
  for k in ks:
    v = DISASSEMBLER[k]
    print '  0x%02X: "%s",' % (k, v)
  print '}'

prepgen()
gen('default:', 2)
gen('trace("???")')
gen('console.error("Invalid opcode " + opcode)')
gen('return false', -2)
gen('}')
gen('return true')
gen('}')
prepgen()




  
#0        01        12        23        34        45        56           
#0123456789012345678901234567890123456789012345678901234567890123
#|ADCA s|B9|*-****| XXXX |4|Add with Carry         |A=A+s+C     |
