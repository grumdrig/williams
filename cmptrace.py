#! /usr/bin/env python
"""Compares trace output from different implementations of Williams emulators"""

import sys, re

def norm(s):
	return re.sub('_', '0', s)

def main():
	print 'Comparing', ', '.join(sys.argv[1:])
	aa = open(sys.argv[1], 'r').readlines()
	bb = open(sys.argv[2], 'r').readlines()
	for i in range(0, min(len(aa), len(bb))):
		a = aa[i]
		b = bb[i]
		if (a[4] != ':') and (b[4] != ':'):
			continue
		a = re.sub(r'(?m)[ \t]+', ' ', a)
		b = re.sub(r'(?m)[ \t]+', ' ', b)
		for j in range(0, min(len(a), len(b))):
			if '_' in a[j] + b[j]:
				a = a[:j] + '_' + a[j+1:]
				b = b[:j] + '_' + b[j+1:]
		if a != b:
			print "Mismatch at line %d" % i
			if i >= 1: print aa[i-1],
			if i >= 1: print bb[i-1]
			print a,
			print b
			print aa[i+1],
			print bb[i+1],
			return
	print "Success after %d lines" % i



if __name__ == "__main__":
	main()