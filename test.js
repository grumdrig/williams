var Mpu = require('./emu680X').Mpu;
var williams = require('./williams');

var main = new Mpu(6809);

//main.createRam(0, 0xD000);
main.loadRom(0xF000, 'build/test.bin');
main.reset(0xF000);

if (typeof window === 'undefined') {
  main.verbose(true);
  main.run(400);
  main.showram();
}
