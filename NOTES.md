Insts with potentially different impacts on flags in 6800 and 6809
CMP
COM
DEC
INC
JMP
JSR
NEG
ROL
ROR
RTI
TST

Code that selects the ROM bank at $0000, I think
FE48: 86 01           LDA  #$01
FE4A: B7 C9 00        STA  $C900

Code that initializes the palette to the 16 values at F60B is at F459.

Main labels:
D012: clear screen subroutine

Sound labels:
F4EE: memcpy subroutine

Defender SWI without S set up: F728


Robo main 74EB: Wait for bit 2 of ROM PIA channel A (0xC80C) to be set
(by vtrace thing). (Part of subroutine starting at 6F03). After that,
I gets cleared.


DC56 = IRQ entry points:

9845 seems to hold info about which bank is selected. And/or

IRQ seems to expecting an interrupt on ROM PIA B (which is configured
for output)

 CR - bit 7: control line 1 interrupt status (0 = idle, 1 = request) RO
      bit 6: control line 2 interrupt status (0 = idle, 1 = request) RO
      bit 5: control line 2 direction (0 = input, 1 = output) RW
      bit 4: control line 2 mode (0 = 1->0, 1 = 0->1 or 'follow') RW
      bit 3: control line 2 enable (0 = disable, 1 = enable) RW
      bit 2: PDR/DDR select (0 = DDR, 1 = PDR) RW
      bit 1: control line 1 mode (0 = 1->0, 1 = 0->1) RW
      bit 0: control line 1 enable (0 = disable, 1 = enable) RW

Popular values for PIA control registers:
04 0000.0100 line2 disabled;             PDR; line1 disabled
14 0001.0100 line2 disabled (follow);    PDR; line1 disabled
34 0011.0100 line2 disabled;             PDR; line1 disabled
35 0011.0101 line2 disabled;             PDR; line1 follow; enabled
37 0011.0111 line2 disabled;             PDR; line1 1->0;   enabled
3C 0011.1100 line2 enabled follow input; PDR; line1 disabled


MYSTERY: Apparently Defender is supposed to play the same sound on
startup that Robotron does, and if you manually signal an IRQ, it
does. But the main CPU code doesn't seem to trigger that, whereas
Robotron's does. Mysterious.

MYSTERY: The instruction at $FCDD in defend.snd fetches from address
$EFFD, which should be invalid. Might just be a harmless bug.

MYSTERY: Are the games supposed to go into diagnostic mode on startup,
or does that depend on the coin door being closed or something?
Because right now, that's what it does.

Robo:

Carpet-making sequence loop falls out at FDAB, I think the validation
loops falls out at FDEB. Then switch to rom bank at FE08. Then there's
some other validation loop that runs. Then I think that's all over at
F47C where the screen wipe to black happens before too long at all.

The screen wipe subroutine is at D87C. The RTS (PUL) is at DB9A.

5F99 is the subroutine to display "initiial tests indicate operational".

F37F is a time-waster loop subroutine (with return addr stored in Y)

A loop at D137 clears mem from 9800 or so to BF70

D099/D4FC is a subroutine
D036/D6EC is a subroutine

In fact D000 onward is a big list of subroutines

74EB is the loops that waits for bit 1 (0x2) (the advance key I think)
of C80C to be set

The memory test is the Y-return subroutine
   F472: JMP $FD65
so 3 NOPs there would skip it. ...

That doesn't work but patching BRN instructions at FDF1 FDE9 would
skip the memory check, I think.

D154: JSR 6F03  leads to the loop waiting for the advance button to be pressed
Then it seems like the stack gets messed up and we return somewhere
crazy. So here's trying to sort that out.
After RTS S=BF70
D165: JSR D0A2 but S is same on return
D17A: JSR D054/D281 - something bad within this call
  after JSR S=BF6E
  PSHS $42 (U,A)  S=BF6B
  Then stack+3 gets altered, which should be PC (*):
    the pc on the stack -> U
    [D17D] -> X
    D17F -> pc on the stack
      so it's all a crazy way to skip over 2 bytes, and load them into X instead
  D28B: BSR D25A S=BF69
    PSHS $62     S=BF64
    D27F: PULS $E2 (rts) then S=BF6B which is right and PC is right
  D28D: PULS $C2  S=BF70

    which looks right, but appears to skip over D17D:ASR $A086, which
    kind of looks like garbage a little, maybe because of (*). So
    maybe there's not really a problem here, it just looked like it
    because my breakpoint and D17D got skipped over.

    So yeah that all seems okay.

D17F: ...
D186: ANDCC 0  - so ready to handle IRQ

Then just after that the IRQ gets triggered and shit goes bad. So the
problem might all be in the IRQ which probably never gets triggered
until this point in the code.

If I suppress the IRQ, the code just loops looking for a change in
[9810] (at D198).

OKAY! The IRQ handler:

Before IRQ S=BF70
When it fires S=BF64
After the RTI it's BF70 again...But E seems to be set, which is wierd

DC56: Expects IRQ came from C80E
Then not long after there's lots of button handling code

DC94: JSR DD3E
  RTI and bug!
