;Blit Test Williams data file for 6809dasm
;please send additions/changes to seanriddle <at> airosurf <dot> com
;look for newest files at http://www.seanriddle.com

;This is a modified version of my RAM DUMP/ROM SWITCH/CMOS LOAD software
; for my multigame.  I do a series of blits, then jump to the RAM DUMP.

;$F000-F3FF is the code
;$FFF2-FFFF is CPU vectors

;date 3/18/98

label 9900 game_gfx
label 9902 game
label 9904 counter
label c000 color_registers
label c400 outport
label c804 widget_pia_dataa
label c805 widget_pia_ctrla
label c806 widget_pia_datab
label c807 widget_pia_ctrlb
label c80c rom_pia_dataa
label c80d rom_pia_ctrla
label c80e rom_pia_datab
label c80f rom_pia_ctrlb
label C900 rom_enable_scr_ctrl
label CA00 start_blitter
label CA01 blitter_mask
label CA02 blitter_source
label CA04 blitter_dest
label CA06 blitter_w_h
label cbff watchdog
label ec00 blit_table
label ef00 TIME_BLITS
label f000 RESET
label f02e RAM_DUMP
label f0d8 CHECK_INPUT
label f0ed LOAD_CMOS
label f112 DO_BLITS
label f1c1 sound_table
label f1c8 DELAY_300
label f1cb DELAY_X
label f1d3 SETUP_WIDGET_PIA
label f220 TRASH_CMOS
label f270 text
label f278 text_end
label f280 RAM_CODE
label f2cb HANG
label f2d0 RAM_CODE_END
label f2e0 BLIT_GFX
label f300 DRAW_GFX
label f320 blank_palette
label f330 ZERO_PALETTE
label f334 LOAD_PALETTE
label f341 LOAD_MYPALETTE
label f350 CLEAR_SCREEN
label f370 mypalette
label f3c0 CMOS_CODE
label f400 CMOS_CODE_END
label f800 CMOS_IMAGE_END

commentline 0 Software ROM Switcher
data 0-8fff
commentline 9000 RAM
data 9000-bfff
commentline C000 I/O space
data c000-cfff
commentline d000 game icons
data d000-d3ff
data d400-d7ff
data d800-dbff
data dc00-dfff
data e000-e3ff
data e400-e7ff
data e800-ebff
data ec00-eeff
data ef1e-efff
commentline f000 reset routine
comment f000 turn off ints
comment f002 setup stack
comment f006 setup ROM PIA
comment f009 ROM PIA A all input (coin door switches)
comment f014 ROM PIA B all output (sound)
comment f023 watchdog data
comment f02e disable ROM
comment f033 output 'RAM DUMP'
comment f05e output 0000-97FF
comment f0db check auto/up
comment f0df check advance
comment f0e6 check h/s reset
comment f123 enable ROM
comment f128 clear screen RAM
comment f188 blank area
comment f18b blit game graphics
comment f19b wait for plyr1/plyr2
comment f1ab wait for release
comment f1b2 next game
comment f1e8 colors to black
comment f1eb clear RAM
comment f1ee reset PIA regs
comment f205 reset sound board
data f213-f21f
data f260-f26f
ascii f270-f277
data f278-f27f
data f2d0-f2df
data f31b-f31f
data f320-f32f
data f347-f34f
data f360-f36f
data f370-f37f
data f380-f3af
comment f3b0 select game ROM
comment f3b3 jump to reset vector
data f3b7-f3bf
comment f3c0 wait for start
comment f3d4 load cmos image
comment f3e7 set colors to green
data f3f8-f3ff
comment f400 cmos image
data f400-f7ff
data f800-ffdf
ascii ffe0-ffef
data fff0-fff1
commentline fff2 CPU vectors
data fff2-fff3
comment fff2 SWI3 vector
data fff4-fff5
comment fff4 SWI2 vector
data fff6-fff7
comment fff6 FIRQ vector
data fff8-fff9
comment fff8 IRQ vector
data fffa-fffb
comment fffA SWI vector
data fffc-fffd
comment fffc NMI vector
comment fffe RESET vector
data fffe-ffff
