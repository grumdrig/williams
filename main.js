// Call this with node to run the simulator non-visually

var emu680X = require('./emu680X.js')
var williams = require('./williams.js')
var games = require('./games.js')

var game = games.game;
var main = game.main;
var sound = game.sound;

// console.log(game.name + (game.label ? ' (' + game.label + ')' : ''));

var SYSTEM_CLOCK = 0;
var VERTICAL_COUNT = 0;
var FRAME_START = 0;
var FRAME_COUNT = 0;

var hardstep = false;
var paused = false;

main.cyclesPerFrame = 1 / 60;       // 1 MHz CPU at 60 Hz refresh rate
sound.cyclesPerFrame = 0.895 / 60;  // 895 kHz (or so) at 60 Hz

var lastFrame;
var lastMain;
main.realTime = 0;
main.cpuTime = 0;

main.breakpoints = {};
sound.breakpoints = {};

function $() { return {} }

main.reset()
sound.reset()

function vsync() {
  ++FRAME_COUNT;
  var frameEnd = SYSTEM_CLOCK + 1/60;  // 60 Hz
  while (hardstep || main.hardstep || sound.hardstep ||
         (!paused && Math.min(main.clock(), sound.clock()) < frameEnd)) {
    var mpu = (main.clock() < sound.clock()) ? main : sound;

    if (mpu === sound) {
   	  // console.log(mpu.disassemble(mpu.PC()).long)
	  }

    mpu.step();
    mpu.hardstep = false;
    hardstep = false;

    if (mpu.breakpoints[mpu.PC()] && !paused) {
      console.log(fmt('Break at %s.%4x', mpu.name, mpu.PC()));
      pause(true);
    }

    if (mpu === main) {
      var vcount = ((main.clock() * 60 * 256) >> 0) % 256;
      if (VERTICAL_COUNT !== vcount) {
        VERTICAL_COUNT = vcount;
        main.store8(game.video_counter_address, VERTICAL_COUNT & 0xFC);

        if (VERTICAL_COUNT === 240 || VERTICAL_COUNT === 0)
          game.rompia.peripheralInput(1, 0xFF);
        if (VERTICAL_COUNT === 0)
          break;  // That's a vblank folks
      }
    }
  }

  if (!paused) {
    if (lastFrame) {
      main.cpuTime += main.clock() - lastMain;
      main.realTime += (+new Date() - lastFrame) / 1000;
      var speed = main.cpuTime / main.realTime;
      // $('#speed').innerHTML = speed.toPrecision(3) + ' MHz';
    }
    lastFrame = +new Date();
    lastMain = main.clock();
  } else {
    lastFrame = null;
  }

  SYSTEM_CLOCK = Math.min(frameEnd, main.clock(), sound.clock());

  // if (!paused)
  //   requestAnimationFrame(vsync);
}

for (var i = 0; i < 1000; ++i)
	vsync();
