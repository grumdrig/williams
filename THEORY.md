Williams Hardware and Software Theory
=====================================

This is my summary of the Williams arcade platform as relevant to
emulating it.

The hardware features two Motorola microprocessors: a 6809e, the main
CPU clocked at 1 MHz, and a 6802/6808 for sound generation, clocked at
894.886 kHz. They interact via a 6821 PIA. The sound DAC is an MC1408.

Main CPU
--------

The 6809e is identical, internally, to a 6809. (Not sure if the latter
is used, ever, in Williams arcades.)

Here is the memory map for the main CPU:

    Range     Read / Write
    --------- ---------------
    0000-8FFF ROM / Video DRAM
    9000-97FF Video DRAM
    9800-BFFF DRAM
    C000-CBFF Input / Output (see I/O section)
    CC00-CFFF CMOS SRAM (for high scores, e.g.)
    D000-F000 ROM

(Sinistar has SRAM at $D000; Defender lacks the ROM at $0000, and
bank-switches between four different ROMs and I/O at $C000-$CFFF, by
writing to $D000.)

I/O
---

The MPUs interact with each other and peripherals using 6821 PIAs. The
main CPU has two (the "widget" and "rom" PIAs) and the sound CPU has
one. Each PIA has two nearly identical channels, A and B.

The sound PIA is configured for output on channel A. The output is
directed to the D-A converter; the output waveform simply takes on the
unsigned 8-bit value sent from the PIA at any given time.

Channel B is configured for input, by which it receives triggers from
the main MPU to play different sounds, via channel B of the rom PIA.

    C000-C00F Palette (BBGGGRRR format)

    C804      Widget PIA Data A
      :0          move up
      :1          move down
      :2          move left
      :3          move right
      :4          1 player
      :5          2 players
      :6          fire up
      :7          fire down
    C805      Widget PIA Ctrl A
    C806      Widget PIA Ctrl B
      :0          fire left
      :1          fire right
    C807      Widget PIA Ctrl B
      :5-3        110 = player 2 controls, 111 = player 1 controls

    C80C-C80F PIA (7 bits coin door, 7 out to sound, 4 diagnotic LED)
    C80C      ROM PIA Data A
      :0          auto up
      :1          advance
      :2          right coin
      :3          high score reset
      :4          left coin
      :5          center coin
      :6          slam door tilt
      :7          hand shake from sound board
    C80D      ROM PIA Ctrl A
      :2?         LED 7-segment
    C80E      ROM PIA Data B
      :0-5        to sound board
      :6-7        LED 7-segment (Blaster: select sound board)
    C80F      ROM PIA Ctrl B
      :2?         LED 7-segment

    C900:0    Select RAM or ROM bank in 0000-BFFF, supposedly
    C900:1    Flip screen for cocktail game
    C900:3    (Protect top of screen in Sinistar)

    CA00      Blitter start as determined by bits:
        :0        linear / screen format (256 byte pixel offset) for source
        :1        linear / screen format for destination
        :2        sync reads with E clock? (for DRAM-DRAM blits)
        :3        transparency mode? (only blit non-0)
        :4        solid mode? (source color given by CA01)
        :5        rotate image one pixel right?
        :6        don't blit even pixels
        :7        don't blit odd pixels       
    CA01      Blitter mask / solid color for CA00:4==1
    CA02-CA03 Blitter source location
    CA04-CA05 Blitter dest location
    CA06      Blitter width
    CA07      Blitter height

    CB00      "Video address bus VA8-VA13 placed on data bus D2-D7"
    CBFF      Watchdog (0x39 must be written here every 8 VSYNCs (133 ms))

    CC00      Somthing to do with the ROM board PIA
    CC10      Similarly (p.31 of Defender Theory of Operation)

    
Sound CPU
---------

The sound CPU is a 6802 or 6808. (The latter only lacks the feature of
the first 32 bytes of RAM being non-volatile.) Both are identical to
the 6800 microprocessor, except for the addition of 128 bytes of
on-board RAM at $0000.

Here is the memory map used by the sound CPU. (The parenthesized
portions are only available in Sinistar, I believe.):

    Range     Memory
    --------- ----------------
    0000-007F In-CPU RAM
    0100-1FFF (RAM)
    2000-2FFF (Yamaha 2151)
    0400-0403 PIA sound output
    6000-6FFF (HC5516 DAC in Sinistar)
    7000-7FFF ("Bank I/O")
    8000-AFFF ("Speech and sound ROM")
    B000-EFFF (Sinistar voice support)
    F000-FFFF ROM

All other addresses are not used.



Video
-----

Video is 304 wide by 256 high by 4 bit pixel depth at 60 Hz. Each
4-bit color indexes into the 16x8 bit palette at 0xC000-0xC00F, each
entry of which has the format BBGGGRRR (two bits of blue, three of
green, three of red). 

The video buffer is at address 0 with length 0x9800. The order of
pixels is column-wise, by bytes, each byte having a width of two
pixels. That is, with the origin at the top left, byte 0 represents
screen coordinates (0,0) and (1,0) (in the high and low nibbles
respectively); byte 1 represents (0,1) and (1,1); byte 2 is (0,2),
(1,2), etc.

Reportedly two columns and one row are not displayed in the original
game; I am not sure which ones.

The 6 high bits of the vertical video address of the video decoder are
available to read at $CB00. An interrupt apparently occurs on the ROM
PIA when the vertical video address is at or above $F0.

