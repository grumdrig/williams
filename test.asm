
        org $f000

l_go:
        ;; set up palette
        clr $c000               ; black

        lda #$07                ; red
        sta $c001

        lda #$38                ; green
        sta $c002

        lda #$C0                ; blue
        sta $c003

        lda #$C7                ; magenta
        sta $c004

        lda #$3F                ; yellow
        sta $c005

        lda #$F8                ; cyan
        sta $c006

        lda #$01
        sta $c007
        lda #$02
        sta $c008
        lda #$04
        sta $c009
        lda #$08
        sta $c00A
        lda #$10
        sta $c00B
        lda #$20
        sta $c00C
        lda #$40
        sta $c00D
        lda #$80
        sta $c00E
        
        lda #$ff                ; white
        sta $c00f
        
        
        clr $cf0a               ; address
        clr $cf0b

        clr $cf0c               ; color

        ldd 0
big_loop:
        ldx #0

inner_loop:     
        std ,x++
        cmpx #$9800
        bcc inner_loop

        addd #$1111
        fcb $10,$10             ; trace
        jmp big_loop

bullshit:
        lbne bs2
bs2:    
        leax 5,x
        leax 8,x
        leax 15,x
        leax 16,x
        leax -1,x
        leax -2,x
        leax -15,x
        leax -16,x
        leax -17,x
        leax -18,x
        