MAINROM=d000=robotron/robotron.sba \
	e000=robotron/robotron.sbb \
	f000=robotron/robotron.sbc \
	0000=robotron/robotron.sb1 \
	1000=robotron/robotron.sb2 \
	2000=robotron/robotron.sb3 \
	3000=robotron/robotron.sb4 \
	4000=robotron/robotron.sb5 \
	5000=robotron/robotron.sb6 \
	6000=robotron/robotron.sb7 \
	7000=robotron/robotron.sb8 \
	8000=robotron/robotron.sb9

SOUNDROM=f000=robotron/robotron.snd

PROM=0000=robotron/decoder.4 \
     0200=robotron/decoder.6

ENTRIES=-e F47C -e F475
ENTRIES=

run: build/robotron_main
	$<

loop: 
	loop.py make -- xdasm.py sim6809.h Makefile


build/robotron_main: build/robotron_main.cc sim6809.h build/
	g++ -Werror -o $@ $<

build/robotron_main.cc: xdasm.py build/ Makefile
	python $< -o $@ -a build/robotron_main.a -v $(ENTRIES) $(MAINROM)


build/robotron_sound: build/robotron_sound.cc sim6802.h build/
	g++ -o $@ $<

build/robotron_sound.cc: xdasm.py build/ Makefile
	python $< -o $@ -a build/robotron_sound.a -v $(SOUNDROM)


build/:
	mkdir $@


clean:
	rm -rf build/

# 6802

build/d6802.py: translate6802.py reference/6802
	python $< -d > $@

dasmsnd: dasm6802.py build/d6802.py
	python $< f000=robotron/robotron.snd

# Informational stuff


dasm: build/0000.asm build/d000.asm build/snd.asm

build/snd.asm:
	tools/dasm09/dasm09 -offset 0xf000 robotron/robotron.snd > $@

build/0000.asm: build/0000.rom
	tools/dasm09/dasm09 -offset 0x0000 $< > $@

build/d000.asm: build/d000.rom
	tools/dasm09/dasm09 -offset 0xd000 $< > $@

build/0000.rom:
	cat robotron/robotron.sb{1,2,3,4,5,6,7,8,9} > $@

build/d000.rom:
	cat robotron/robotron.sb{a,b,c} > $@

# assembly

build/test.bin: test.asm
	~/lib/as9/as9 test.asm -bin l
	mv test.bin test.lst build/
