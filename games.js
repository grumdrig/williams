var emu = require('./emu680X');
var williams = require('./williams');

var readRoms = williams.readRoms;

var game = {
  rom: typeof window != 'undefined' && window.location.search.replace(/\W/g, '') || 'stargate',
  main: new emu.Mpu(6809, 1.000000),
  sound: new emu.Mpu(6808, 0.895000),
};

switch(game.rom) {
case 'defender':
  game.name = 'Defender';
  game.label = 'Red';
  game.main.loadRom(0xD000, readRoms('defend.1', 'defend.4', 'defend.2', 'defend.3'));
  game.bankC000 = {
    1: readRoms('defend.9', 'defend.12'),
    2: readRoms('defend.8', 'defend.11'),
    3: readRoms('defend.7', 'defend.10'),
    7: readRoms('defend.6')
  };
  game.sound.loadRom(0xF800, 'defend.snd');
  break;

case 'defendg':
  game.name = 'Defender';
  game.label = 'Green';
  game.main.loadRom(0xD000, readRoms('defeng01.bin', 'defeng04.bin',
                                     'defeng02.bin', 'defeng03.bin'));
  game.bankC000 = {
    1: readRoms('defeng09.bin', 'defeng12.bin'),
    2: readRoms('defeng08.bin', 'defeng11.bin'),
    3: readRoms('defeng07.bin', 'defeng10.bin'),
    7: readRoms('defeng06.bin')
  };
  game.sound.loadRom(0xF800, 'defend.snd');
  break;

case 'defendw':
  game.name = 'Defender';
  game.label = 'White';
  game.main.loadRom(0xD000, readRoms('wb01.bin', 'defeng02.bin', 'wb03.bin'));
  game.bankC000 = {
    1: readRoms('defeng09.bin', 'defeng12.bin'),
    2: readRoms('defeng08.bin', 'defeng11.bin'),
    3: readRoms('defeng07.bin', 'defeng10.bin'),
    7: readRoms('defeng06.bin')
  };
  game.sound.loadRom(0xF800, 'defend.snd');
  break;

case 'stargate':
  game.name = 'Stargate';
  game.bank0000 = readRoms('01', '02', '03', '04', '05', '06', '07', '08', '09');
  game.main.loadRom(0xD000, readRoms('10', '11', '12'));
  game.sound.loadRom(0xF800, 'sg.snd');
  break;

case 'robotron':
  game.name = 'Robotron';
  game.label = 'Solid Blue';
  game.bank0000 = readRoms('robotron.sb1', 'robotron.sb2', 'robotron.sb3',
                           'robotron.sb4', 'robotron.sb5', 'robotron.sb6',
                           'robotron.sb7', 'robotron.sb8', 'robotron.sb9');
  game.main.loadRom(0xD000, readRoms('robotron.sba', 'robotron.sbb', 'robotron.sbc'));
  game.sound.loadRom(0xF000, 'robotron.snd');
  // Monkey-patch-out the memory test
  // (but not quite right)
  //game.main.patch(0xF472, 0x12);
  //game.main.patch(0xF473, 0x12);
  //game.main.patch(0xF474, 0x12);
  break;

case 'robotryo':
  game.name = 'Robotron';
  game.label = 'Yellow/Orange';
  game.bank0000 = readRoms('robotron.sb1', 'robotron.sb2', 'robotron.yo3',
                           'robotron.yo4', 'robotron.yo5', 'robotron.yo6',
                           'robotron.yo7', 'robotron.sb8', 'robotron.sb9');
  game.main.loadRom(0xD000, readRoms('robotron.yoa', 'robotron.yob', 'robotron.yoc'));
  game.sound.loadRom(0xF000, 'robotron.snd');
  break;

case 'joust':
  game.name = 'Joust';
  game.label = 'White/Green';
  game.bank0000 = readRoms('3006-13.1b', '3006-14.2b', '3006-15.3b',
                           '3006-16.4b', '3006-17.5b', '3006-18.6b',
                           '3006-19.7b', '3006-20.8b', '3006-21.9b');
  game.main.loadRom(0xD000, readRoms('3006-22.10b', '3006-23.11b', '3006-24.12b'));
  game.sound.loadRom(0xF000, 'joust.snd');
  break;

case 'joustr':
  game.name = 'Joust';
  game.label = 'Solid Red';
  game.bank0000 = readRoms('joust.wg1', 'joust.wg2', 'joust.wg3',
                           'joust.sr4', 'joust.wg5', 'joust.sr6',
                           'joust.sr7', 'joust.sr8', 'joust.sr9');
  game.main.loadRom(0xD000, readRoms('joust.sra', 'joust.srb', 'joust.src'));
  game.sound.loadRom(0xF000, 'joust.snd');

  break;

case 'joustwr':
  game.name = 'Joust';
  game.label = 'White/Red';
  game.banks0000 = readRoms('joust.wg1', 'joust.wg2', 'joust.wg3',
                            'joust.wg4', 'joust.wg5', 'joust.wg6',
                            'joust.wr7', 'joust.wg8', 'joust.wg9');
  game.main.loadRom(0xD000, readRoms('joust.wra', 'joust.wgb', 'joust.wgc'));
  game.sound.loadRom(0xF000, 'joust.snd');
  break;

case 'blittest':
  game.name = 'Blit Test';
  game.main.loadRam(0x0000, fs.readFileSync('reference/blittest.bin'));
  var pal = [0x00,0xff,0x07,0x05,0x38,0xc7,0xc0,0x80,0xa4,0xe8,0x14,0x90,0x3f,0x51,0x0a,0x67];
  for (var i = 0; i < 16; ++i)
    game.main.store8(0xC000+i, Math.random() * 256);
  // Stub sound
  game.sound.createRam(0, 0x10000);
  var i = 0;
  for (var i = 0xF000; i < 0xF030; ++i) {
    game.sound.store8(i++, 0x7E);
    game.sound.store16(i++, 0xF000);
  }
  game.sound.store16(0xFFFE, 0xF000);
  break;

default:
  alert('Unrecognized ROM set: ' + game.rom);
  throw new Error('Unrecognized ROM set: ' + game.rom);
}

if (game.name === 'Defender') {
  // Older, defender hardware configuration
  game.video_counter_address = 0xC800;
  game.watchdog = 0xC3F3;
  game.widgetpia = new emu.Pia(game.main, 0xCC04);
  game.rompia = new emu.Pia(game.main, 0xCC00);
  game.main.defineIO(0xC010, 0xD001 - 0xC010);
  game.main.keymap = {
    '7': [game.rompia,    0, 0, 'auto up'],
    '8': [game.rompia,    0, 1, 'advance'],
    '6': [game.rompia,    0, 2, 'right coin'],
    '9': [game.rompia,    0, 3, 'high score reset'],
    '5': [game.rompia,    0, 4, 'left coin'],
    'T': [game.rompia,    0, 5, 'center coin'],
    'L': [game.widgetpia, 0, 0, 'fire'],
    'K': [game.widgetpia, 0, 1, 'thrust'],
    'B': [game.widgetpia, 0, 2, 'spart bomb'],
    'H': [game.widgetpia, 0, 3, 'hyperspace'],
    '2': [game.widgetpia, 0, 4, '2 players'],
    '1': [game.widgetpia, 0, 5, '1 player'],
    'D': [game.widgetpia, 0, 6, 'reverse'],
    'S': [game.widgetpia, 0, 7, 'down'],
    'W': [game.widgetpia, 1, 0, 'up'],
  };
} else {
  // Newer, robotron/joust hardware configuration
  game.video_counter_address = 0xCB00;
  game.watchdog = 0xCBFF;
  game.widgetpia = new emu.Pia(game.main, 0xC804);
  game.rompia = new emu.Pia(game.main, 0xC80C);
  game.main.hookWrite(0xCA00, function (startCode) {
    williams.blit(game.main,
                  startCode,
                  game.main.fetch8(0xCA01),
                  game.main.fetch16(0xCA02),
                  game.main.fetch16(0xCA04),
                  game.main.fetch8(0xCA06),
                  game.main.fetch8(0xCA07));
  });
  if (game.rom !== 'blittest')
    game.main.defineIO(0xC010, 0x1000-0x10);
  game.main.keymap = {
    // This is for Robotron, but just using it for everything for now.
    'E': [game.widgetpia, 0, 0, 'move up'],
    'D': [game.widgetpia, 0, 1, 'move down'],
    'S': [game.widgetpia, 0, 2, 'move left'],
    'F': [game.widgetpia, 0, 3, 'move right'],
    '1': [game.widgetpia, 0, 4, '1 player start'],
    '2': [game.widgetpia, 0, 5, '2 player start'],
    'I': [game.widgetpia, 0, 6, 'fire up'],
    'K': [game.widgetpia, 0, 7, 'fire down'],
    'J': [game.widgetpia, 1, 0, 'fire left'],
    'L': [game.widgetpia, 1, 1, 'fire right'],
    '7': [game.rompia,    0, 0, 'auto up'],
    '8': [game.rompia,    0, 1, 'advance'],
    '6': [game.rompia,    0, 2, 'right coin'],
    '9': [game.rompia,    0, 3, 'high score reset'],
    '5': [game.rompia,    0, 4, 'left coin'],
    'T': [game.rompia,    0, 5, 'slam door tilt']
  };
}

if (game.rom !== 'blittest')
  game.main.createRam(0, 0xC010);

if (game.bank0000) {
  game.main.hookWrite(0xC900, function (b) {
    var rom = (b & 1);
    game.main.loadRom(0x0000, rom ? game.bank0000 : null);
    if (typeof $ !== 'undefined')
      $('#main .bank').innerHTML = rom ? 'ROM' : 'DRAM';
  });
}

if (game.bankC000) {
  game.main.hookWrite(0xD000, function (b) {
    // Select ROM bank at C000-CFFF
    if (typeof $ !== 'undefined')
      $('#main .bank').innerHTML = b || 'I/O';
    game.main.loadRom(0xC000, game.bankC000[b]);
    return false;
  });
}

if (game.rom !== 'blittest')
  game.sound.createRam(0, 128);

game.soundpia = new emu.Pia(game.sound, 0x0400);

game.audio = new williams.AudioThing();

game.rompia.connectPeripheralOutput(1, function (value) {
  game.soundpia.peripheralInput(1, value);
});

game.soundpia.connectPeripheralOutput(0, function (value) {
  game.audio.addSample(game.sound.clock(), value);
});

exports.game = game

