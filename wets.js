var game = 'WETS';
var fs = require('fs');
var Mpu = require('./emu680X').Mpu;
var williams = require('./williams');

var main = new Mpu(6809);

main.createRam(0, 0x10000);
main.loadSrec('../wets/willyvidctrs.s19');

main.hookWrite(0xCA00, function (b) {
  williams.blit(main, b, 
                main.fetch8(0xCA01), 
                main.fetch16(0xCA02), 
                main.fetch16(0xCA04),
                main.fetch8(0xCA06), 
                main.fetch8(0xCA07));
  });

main.store16(0xFFFE, 0xF000);
main.reset();

var sound = new Mpu(6800);
sound.createRam(0, 0x10000);
sound.store8(0xF000, 0x7E);
sound.store16(0xF001, 0xF000);
sound.store8(0xF003, 0x7E);
sound.store16(0xF004, 0xF000);
sound.store16(0xFFFE, 0xF000);
sound.reset();