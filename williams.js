

function color(bbgggrrr) {
  var r = 0xE0 & (bbgggrrr << 5);
  var g = 0xE0 & (bbgggrrr << 2);
  var b = 0xC0 & bbgggrrr;
  r |= (r >> 3) | (r >> 6);
  g |= (g >> 3) | (g >> 6);
  b |= (b >> 2) | (b >> 4) | (b >> 6);
  return [r, g, b, 255];
}

function render(ram, pixels) {
  var pal16 = Array(16);
  for (var i = 0; i < 16; ++i)
    pal16[i] = color(ram[0xC000 + i]);
  var palette = Array(256);
  for (var i = 0; i < 256; ++i)
    palette[i] = pal16[i >> 4].concat(pal16[i & 0xF]);
  function put(x, y, c) {
    var d = 4 * (x + y * 304);
    pixels[d++] = c[0];
    pixels[d++] = c[1];
    pixels[d++] = c[2];
    pixels[d] = c[3];
  }
  var npixels = 256 * 304 * 4;
  for (var xy = 0; xy < 256 * 304 / 2; ++xy) {
    var y = xy & 0xFF;
    var x0 = 2 * (xy >> 8);
    put(x0,   y, pal16[ram[xy] >> 4]);
    put(x0+1, y, pal16[ram[xy] & 0xF]);
  }
}


// There's a bug in the blitter that xors bit 2 of the height and
// width. It's fixed in later hardware, but not in any that I
// currently care about.
var m_blitter_xor = 0x4;

function blit(mpu, start, mask, source, dest, width, height) {
  width ^= m_blitter_xor;
  height ^= m_blitter_xor;
  var sxstride = (start & 1) ? 256 : 1;
  var systride = (start & 1) ? 1 : width;
  var dxstride = (start & 2) ? 256 : 1;
  var dystride = (start & 2) ? 1 : width;
  var transparency = !!(start & 0x8);
  var solid = !!(start & 0x10);
  var ror = !!(start & 0x20);
  var even = !(start & 0x40);
  var odd = !(start & 0x80);

  for (var y = 0; y < height; ++y) {
    var s = source & 0xFFFF;
    var d = dest & 0xFFFF;
    var oldc = ror ? mpu.fetch8((s + sxstride * (width - 1)) & 0xFFFF) : 0;
    for (var x = 0; x < width; ++x) {
      var c = mpu.fetch8(s);
      if (ror) {
        var newc = 0xFF & ((oldc << 4) | (c >> 4));
        oldc = c;
        c = newc;
      }
      if (solid) {
        if (transparency)
          c = (((c & 0xF0) ? mask & 0xF0 : 0) |
               ((c & 0x0F) ? mask & 0x0F : 0));
        else
          c = mask;
      }
      if (!even || !odd || transparency) {
        var oc = mpu.fetchRam(d);
        if (!even || (transparency && !(c & 0x0F)))
          c = (c & 0xF0) | (oc & 0x0F);
        if (!odd || (transparency && !(c & 0xF0)))
          c = (c & 0x0F) | (oc & 0xF0);
      }
      mpu.store8(d, c);
      s = (s + sxstride) & 0xffff;
      d = (d + dxstride) & 0xffff;
    }

    if (start & 1)
      source = (source & 0xff00) | ((source + 1) & 0xff);
    else
      source += width;

    if (start & 2)
      dest = (dest & 0xff00) | ((dest + 1) & 0xff);
    else
      dest += width;
  }
}

function readRoms(/*...*/) {
  var fs = require('fs');
  var result = fs.readFileSync('roms/' + arguments[0]);
  for (var i = 1; arguments[i]; ++i) {
    var f1 = result;
    var f2 = fs.readFileSync('roms/' + arguments[i]);
    result = new Uint8Array(f1.length + f2.length);
    result.set(f1);
    result.set(f2, f1.length);
  }
  return result;
}


function AudioThing() {
  var samples = [];
  this.addSample = function (time, value) {
    samples.push([time, value]);
    // function x8(n) {
    //   return ('0' + (0xFF&n).toString(16).toUpperCase()).slice(-2);
    // }
    // console.log(time.toFixed(6), x8(value))
  }
  this.samples = function () { return samples }

  this.play = function () {
    var ctx = new webkitAudioContext();
    var RATE = ctx.sampleRate;
    var b = [];
    var i = 0, v = 0, n = 0;
    while (n < samples.length) {
      while (i / RATE < samples[n][0]) {
        b.push(v);
        i = b.length;
      }
      v = samples[n++][1];
    }
    var buf = ctx.createBuffer(1, b.length, RATE);
    for (var i = 0; i < b.length; ++i)
      buf.getChannelData(0)[i] = b[i] / 128 - 1;
    var source = ctx.createBufferSource();
    source.buffer = buf;
    source.connect(ctx.destination);
    source.start(0);
  }
}

function randVideo(mpu) {
  for (var i = 0; i < 304 * 256 / 2; ++i) mpu.store8(i, i);
}

exports.render = render;
exports.blit = blit;
exports.readRoms = readRoms;
exports.AudioThing = AudioThing;
