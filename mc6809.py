class CPU(object):
  def __init__(self):
    self._CC = 0
    self._A = 0
    self._B = 0
    self._DP = 0
    self._X = 0
    self._Y = 0
    self._S = 0
    self._U = 0
    self._PC = 0

  #CC= property(lambda self: self._CC,lambda self, value: self._CC=uint8(value))

  @property
  def A(self):
    return self._A
  @A.setter
  def A(self, value):
    self._A = int8(value)
    
  #B = property(lambda self: self._B, lambda self, value: self._B = int8(value))
  #DP= property(lambda self: self._DP,lambda self, value: self._DP=uint8(value))

  def __getitem__(self, index):
    return something
  def __setitem__(self, index, value):
    return something

  # -1 for signed byte
  # 2 for unsigned short etc
  def NZVC(self, value, bytes=-1):
    N(value, bytes)
    Z(value, bytes)
    V(value, bytes)
    return C(value, bytes)

  def N(self, value, bytes):
    self._CC = setbit(self._CC, 3, value & [0x80, 0x8000][abs(bytes-1)])
    return crop(value, bytes)

  def addr(cpu, mode, bytes=1):
    if mode == EXTENDED:
      return nextWord()

    elif mode == DIRECT:
      return DP * 256 + nextByte()

    elif mode == INDEXED:
      postbyte = nextByte()
      register = 'XYUS'[(postbyte>>5) & 0x3]
      indirect = (postbyte & 0x10 != 0)
      mode = postbyte & 0xF
      if postbyte == 0x9F:
        # Extended indirect
        return input16(nextWord())
      elif (postbyte & 0x80) == 0:
        # 5-bit offset from register contained in postbyte
        return reg(register) + (-mode if indirect else mode)

      offset = reg(register)
      if mode == 0x4:
        # Zero offset from register
        pass
      elif mode == 0x8:
        # 8-bit offset from register
        offset += nextSbyte()
      elif mode == 0x9:
        offset += nextShort()
      elif mode == 0x6:
        offset += A
      elif mode == 0x5:
        offset += B
      elif mode == 0xA:
        offset += D
      elif mode == 0:
        increg(register, 1)
      elif mode == 1:
        increg(register, 2)
      elif mode == 2:
        increg(register, -1)
        offset -= 1
      elif mode == 3:
        increg(register, -2)
        offset -= 2
      elif mode == 0xC:
        offset = nextSbyte()
      elif mode == 0xD:
        offset = nextShort()
      else:
        error("Invalid indexing mode %02X in postbyte %02X" % (mode, postbyte))

      if indirect:
        offset = input(offset, bytes)

      return offset


  def emulate(cpu, mnemonic):
    if mnemonic == 'ABX':
      cpu.X += cpu.Bu

    elif mnemonic == 'ADCA':
      cpu.H(cpu.A, cpu.fetch(), cpu.C)
      cpu.A = cpu.NZVC(cpu.A + cpu.fetch() + cpu.C)
    elif mnemonic == 'ADCB':
      cpu.H(cpu.B, cpu.fetch(), cpu.C)
      cpu.B = cpu.NZVC(cpu.B + cpu.fetch() + cpu.C)

    elif mnemonic == 'ADDA':
      cpu.H(cpu.A, cpu.fetch())
      cpu.A = cpu.NZVC(cpu.A + cpu.fetch())
    elif mnemonic == 'ADDB':
      cpu.H(cpu.B, cpu.fetch())
      cpu.B = cpu.NZVC(cpu.B + cpu.fetch())
    elif mnemonic == 'ADDD':
      cpu.D = cpu.NZVC(cpu.D + cpu.fetch(2), 2)

    elif mnemonic == 'ANDA':
      cpu.A = cpu.NZV(cpu.A & cpu.fetch())
    elif mnemonic == 'ANDB':
      cpu.A = cpu.NZV(cpu.A & cpu.fetch())
    elif mnemonic == 'ANDCC':
      cpu.CC = cpu.CC & cpu.fetch()

    elif mnemonic == 'ASLA':
      cpu.A = cpu.NZC(cpu.A << 1)
      cpu.V = cpu.N ^ cpu.C
    elif mnemonic == 'ASLB':
      cpu.B = cpu.NZC(cpu.B << 1)
      cpu.V = cpu.N ^ cpu.C
    elif mnemonic == 'ASL':
      a = cpu.addr()
      cpu[a] = cpu.NZC(cpu[a] << 1)
      cpu.V = cpu.N ^ cpu.C

    elif mnemonic == 'ASRA':
      cpu.C = cpu.A & 1
      cpu.A = cpu.NZ(cpu.A >> 1)
    elif mnemonic == 'ASRB':
      cpu.C = cpu.B & 1
      cpu.B = cpu.NZ(cpu.B >> 1)
    elif mnemonic == 'ASR':
      a = cpu.addr()
      cpu.C = cpu[a] & 1
      cpu[a] = cpu.NZ(cpu[a] >> 1)

    elif mnemonic[1:] == 'BCC':
      branch(cpu.fetch(), not cpu.C)
    elif mnemonic[1:] == 'BCS':
      branch(cpu.fetch(), cpu.C)
    elif mnemonic[1:] == 'BEQ':
      branch(cpu.fetch(), cpu.Z)
    elif mnemonic[1:] == 'BGE':
      branch(cpu.fetch(), cpu.N == cpu.V)
    elif mnemonic[1:] == 'BGT':
      branch(cpu.fetch(), cpu.N == cpu.V and not cpu.Z)
    elif mnemonic[1:] == 'BHI':
      branch(cpu.fetch(), not cpu.Z and not cpu.C)
    elif mnemonic[1:] == 'BLE':
      branch(cpu.fetch(), cpu.N != cpu.V or cpu.Z)
    elif mnemonic[1:] == 'BLS':
      branch(cpu.fetch(), cpu.Z or cpu.C)
    elif mnemonic[1:] == 'BLT':
      branch(cpu.fetch(), cpu.N != cpu.V)
    elif mnemonic[1:] == 'BMI':
      branch(cpu.fetch(), cpu.N)
    elif mnemonic[1:] == 'BNE':
      branch(cpu.fetch(), not cpu.Z)
    elif mnemonic[1:] == 'BPL':
      branch(cpu.fetch(), not cpu.N)
    elif mnemonic[1:] == 'BRA':
      branch(cpu.fetch())
    elif mnemonic[1:] == 'BRN':
      cpu.fetch()
    elif mnemonic[1:] == 'BSR':
      offset = cpu.fetch()
      cpu.push(cpu.PC, 2)
      cpu.branch(offset)
    elif mnemonic[1:] == 'BVC':
      branch(cpu.fetch(), not cpu.V)
    elif mnemonic[1:] == 'BVS':
      branch(cpu.fetch(), cpu.V)

    elif mnemonic == 'BITA':
      cpu.NZV(cpu.A & cpu.fetch())
    elif mnemonic == 'BITB':
      cpu.NZV(cpu.B & cpu.fetch())

    elif mnemonic == 'CLRA':
      cpu.A = cpu.NZVC(0)
    elif mnemonic == 'CLRB':
      cpu.A = cpu.NZVC(0)
    elif mnemonic == 'CLR':
      cpu[cpu.addr()] = cpu.NZVC(0)

    elif mnemonic == 'CMPA':
      cpu.NZVC(cpu.A - cpu.fetch())
    elif mnemonic == 'CMPB':
      cpu.NZVC(cpu.B - cpu.fetch())
    elif mnemonic == 'CMPD':
      cpu.NZVC(cpu.D - cpu.fetch(2), 2)
    elif mnemonic == 'CMPS':
      cpu.NZVC(cpu.S - cpu.fetch(2), 2)
    elif mnemonic == 'CMPU':
      cpu.NZVC(cpu.U - cpu.fetch(2), 2)
    elif mnemonic == 'CMPX':
      cpu.NZVC(cpu.X - cpu.fetch(2), 2)
    elif mnemonic == 'CMPY':
      cpu.NZVC(cpu.Y - cpu.fetch(2), 2)

    elif mnemomic == 'COMA':
      cpu.A = cpu.NZV(~cpu.A)
      cpu.C = 1
    elif mneomic == 'COMB':
      cpu.B = cpu.NZV(~cpu.B)
      cpu.C = 1
    elif mneomic == 'COM':
      a = cpu.addr()
      cpu[a] = cpu.NZV(~cpu[a])
      cpu.C = 1

    elif mnemonic == 'CWAI':
      cpu.CC = cpu.CC & cpu.fetch()
      cpu.E = 1
      cpu.push(cpu.PC, 2)
      cpu.push(cpu.U, 2)
      cpu.push(cpu.Y, 2)
      cpu.push(cpu.X, 2)
      cpu.push(cpu.DP)
      cpu.push(cpu.D, 2)
      cpu.push(cpu.CC)
      cpu.wait()

    elif mnemonic == 'DAA':
      ahi = (cpu.A >> 4) & 0xF
      alo = cpu.A & 0xF
      if cpu.C or ahi > 9 or (ahi > 8 and alo > 9):
        cpu.A = (ahi + 6) << 4 + alo
      if cpu.H or alo > 9:
        cpu.A += 6
      cpu.NZ(cpu.A)
      # value of C is kind of a mystery, but fuck DAA, presumably
      error('BCD? really?')

    elif mnemonic == 'DECA':
      cpu.A = cpu.NZV(cpu.A - 1)
    elif mnemonic == 'DECB':
      cpu.B = cpu.NZV(cpu.B - 1)
    elif mnemonic == 'DEC':
      a = cpu.addr()
      cpu[a] = cpu.NZV(cpu[a] - 1)

    elif mnemonic == 'EORA':
      cpu.A = cpu.NZV(cpu.A ^ cpu.fetch())
    elif mnemonic == 'EORB':
      cpu.B = cpu.NZV(cpu.B ^ cpu.fetch())

    elif mnemonic == 'EXG':
      r0,r1 = tfrRegisters(nextByte())
      tmp = cpu[r0] if r0 else -1
      if r0:
        cpu[r0] = cpu[r1] if r1 else -1
      if r1:
        cpu[r1] = tmp

    elif mnemonic == 'INCA':
      cpu.A = cpu.NZV(cpu.A + 1)
    elif mnemonic == 'INCB':
      cpu.B = cpu.NZV(cpu.B + 1)
    elif mnemonic == 'INC':
      a = cpu.addr()
      cpu[a] = cpu.NZV(cpu[a] + 1)

    elif mnemonic == 'JMP':
      cpu.PC = cpu.addr()

    elif mnemonic == 'JSR':
      cpu.push(cpu.PC, 2)
      cpu.PC = cpu.addr()

    elif mnemonic == 'LDA':
      cpu.A = cpu.NZV(cpu.fetch())
    elif mnemonic == 'LDB':
      cpu.B = cpu.NZV(cpu.fetch())
    elif mnemonic == 'LDD':
      cpu.D = cpu.NZV(cpu.fetch(), 2)
    elif mnemonic == 'LDS':
      cpu.S = cpu.NZV(cpu.fetch(), 2)
    elif mnemonic == 'LDU':
      cpu.U = cpu.NZV(cpu.fetch(), 2)
    elif mnemonic == 'LDX':
      cpu.X = cpu.NZV(cpu.fetch(), 2)
    elif mnemonic == 'LDY':
      cpu.Y = cpu.NZV(cpu.fetch(), 2)

    elif mnemonic == 'LEAS':
      cpu.S = cpu.addr()
    elif mnemonic == 'LEAU':
      cpu.U = cpu.addr()
    elif mnemonic == 'LEAX':
      cpu.X = cpu.Z(cpu.addr(), 2)
    elif mnemonic == 'LEAY':
      cpu.Y = cpu.Z(cpu.addr(), 2)

    elif mnemonic == 'LSRA':
      cpu.C = cpu.A & 1
      cpu.A = cpu.NZ(0x7F & (cpu.A >> 1))
    elif mnemonic == 'LSRB':
      cpu.C = cpu.B & 1
      cpu.B = cpu.NZ(0x7F & (cpu.B >> 1))
    elif mnemonic == 'LSR':
      a = cpu.addr()
      cpu.C = cpu[a] & 1
      cpu[a] = cpu.NZ(0x7F & (cpu[a] >> 1))

    elif mnemonic == 'MUL':
      cpu.D = cpu.Z(cpu.A * cpu.B, 2)
      cpu.C = 1 & (cpu.B >> 7)

    elif mnemonic == 'NEGA':
      cpu.C = (cpu.A != 0)
      cpu.V = (cpu.A == 0x80)
      cpu.A = cpu.NZ(-cpu.A)
    elif mnemonic == 'NEGB':
      cpu.C = (cpu.B != 0)
      cpu.V = (cpu.B == 0x80)
      cpu.B = cpu.NZ(-cpu.B)
    elif mnemonic == 'NEG':
      a = cpu.addr()
      cpu.C = (cpu[a] != 0)
      cpu.V = (cpu[a] == 0x80)
      cpu[a] = cpu.NZ(-cpu[a])

    elif mnemonic == 'NOP':
      pass

    elif mnemonic == 'HCF':
      sys.exit(1)

    elif mnemonic == 'ORA':
      cpu.A = cpu.NZV(cpu.A | cpu.fetch())
    elif mnemonic == 'ORB':
      cpu.B = cpu.NZV(cpu.B | cpu.fetch())
    elif mnemonic == 'ORCC':
      cpu.CC = cpu.CC | cpu.fetch()

    elif mnemonic in ('PSHS', 'PSHU'):
      stack = mnemonic[-1]
      mask = cpu.fetch()
      if mask & 0x80: cpu.push(cpu.PC, stack, 2)
      if mask & 0x40: cpu.push(cpu.U if stack == 'S' else cpu.S, stack, 2)
      if mask & 0x20: cpu.push(cpu.Y,  stack, 2)
      if mask & 0x10: cpu.push(cpu.X,  stack, 2)
      if mask & 0x08: cpu.push(cpu.DP, stack)
      if mask & 0x04: cpu.push(cpu.B,  stack)
      if mask & 0x02: cpu.push(cpu.A,  stack)
      if mask & 0x01: cpu.push(cpu.CC, stack)

    elif mnemonic in ('PULS', 'PULU'):
      stack = mnemonic[-1]
      mask = cpu.fetch()
      if mask & 0x01: cpu.CC = cpu.pull(stack)
      if mask & 0x02: cpu.A  = cpu.pull(stack)
      if mask & 0x04: cpu.B  = cpu.pull(stack)
      if mask & 0x08: cpu.DP = cpu.pull(stack)
      if mask & 0x10: cpu.X  = cpu.pull(stack, 2)
      if mask & 0x20: cpu.Y  = cpu.pull(stack, 2)
      if mask & 0x40: cpu['U' if stack == 'S' else 'S'] = cpu.pull(stack, 2)
      if mask & 0x80: cpu.PC = cpu.pull(stack, 2)

    elif mnemonic == 'ROLA':
      cpu.A = cpu.NZC(cpu.A << 1 | cpu.C)
      cup.V = cpu.N ^ cpu.C
    elif mnemonic == 'ROLB':
      cpu.B = cpu.NZC(cpu.B << 1 | cpu.C)
      cup.V = cpu.N ^ cpu.C
    elif mnemonic == 'ROL':
      a = cpu.addr()
      cpu[a] = cpu.NZC(cpu[a] << 1 | cpu.C)
      cup.V = cpu.N ^ cpu.C

    elif mnemonic == 'RORA':
      msb = (cpu.C << 7) & 0x80
      cpu.C = cpu.A & 1
      cpu.A = cpu.NZ(msb | (0x7F & cpu.A) >> 1)
    elif mnemonic == 'RORB':
      msb = (cpu.C << 7) & 0x80
      cpu.C = cpu.B & 1
      cpu.B = cpu.NZ(msb | (0x7F & cpu.B) >> 1)
    elif mnemonic == 'ROR':
      a = cup.addr()
      msb = (cpu.C << 7) & 0x80
      cpu.C = cpu[a] & 1
      cpu[a] = cpu.NZ(msb | (0x7F & cpu[a]) >> 1)

    elif mnemonic == 'RTI':
      cpu.CC = cpu.pull('S')
      if cpu.E:
        cpu.A = cpu.pull('S')
        cpu.B = cpu.pull('S')
        cpu.DP = cpu.pull('S')
        cpu.X = cpu.pull('S', 2)
        cpu.Y = cpu.pull('S', 2)
        cpu.U = cpu.pull('S', 2)
      cpu.PC = cpu.pull('S', 2)

    elif mnemonic == 'RTS':
      cpu.PC = cpu.pull('S', 2)

    elif mnemonic == 'SBCA':
      cpu.A = cpu.NZVC(cpu.A - cpu.fetch() - cpu.C)
    elif mnemonic == 'SBCB':
      cpu.B = cpu.NZVC(cpu.B - cpu.fetch() - cpu.C)

    elif mnemonic == 'SEX':
      cpu.D = cpu.NZ(cpu.B, 2)

    elif mnemonic == 'STA':
      cpu[cpu.addr()] = cpu.NZV(cpu.A)
    elif mnemonic == 'STB':
      cpu[cpu.addr()] = cpu.NZV(cpu.B)
    elif mnemonic == 'STD':
      cpu[cpu.addr()] = cpu.NZV(cpu.D, 2)
    elif mnemonic == 'STS':
      cpu[cpu.addr()] = cpu.NZV(cpu.S, 2)
    elif mnemonic == 'STU':
      cpu[cpu.addr()] = cpu.NZV(cpu.U, 2)
    elif mnemonic == 'STX':
      cpu[cpu.addr()] = cpu.NZV(cpu.X, 2)
    elif mnemonic == 'STY':
      cpu[cpu.addr()] = cpu.NZV(cpu.Y, 2)

    elif mnemonic == 'SBCA':
      cpu.A = cpu.NZVC(cpu.A - cpu.fetch())
    elif mnemonic == 'SBCB':
      cpu.B = cpu.NZVC(cpu.B - cpu.fetch())
    elif mnemonic == 'SBCD':
      cpu.D = cpu.NZVC(cpu.D - cpu.fetch(2), 2)

    elif mnemonic[:3] in ('SWI', 'RES'):
      cpu.E = 1
      pushS(cpu.PC, 2)
      pushS(cpu.U, 2)
      pushS(cpu.Y, 2)
      pushS(cpu.X, 2)
      pushS(cpu.DP)
      pushS(cpu.B)
      pushS(cpu.A)
      pushS(cpu.CC)
      if mnemonic == 'SWI':
        cpu.I = 1
        cpu.F = 1
      cpu.PC = cpu[{'SWI': 0xFFFA,
                    'SWI2': 0xFFF4,
                    'SWI3': 0xFFF2,
                    'RESET': 0xFFFE}[mnemonic]]

    elif mnemonic == 'SYNC':
      cpu.wait()

    elif mnemonic == 'TFR':
      d,s = tfrRegisters(cpu.fetch())
      if d:
        cpu[d] = cpu[s] if s else -1

    elif mnemonic == 'TSTA':
      cpu.NZV(cpu.A)
    elif mnemonic == 'TSTB':
      cpu.NZV(cpu.B)
    elif mnemonic == 'TSTA':
      cpu.NZV(cpu[cpu.fetch()])

    else:
      error('Unimplemented opcode 0x%X / %s' % (opcode, mnemonic))


